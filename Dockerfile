FROM openjdk:11
EXPOSE 8080
ADD build/libs/*.jar app.jar
ADD newrelic/newrelic.jar newrelic.jar
ADD newrelic/newrelic.yml newrelic.yml
CMD ["java","-jar","/app.jar"]
