------------------------------------------------------------------------------------------------------------------------
--- EXTENSIONS
------------------------------------------------------------------------------------------------------------------------
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

------------------------------------------------------------------------------------------------------------------------
--- TYPES
------------------------------------------------------------------------------------------------------------------------
CREATE TYPE role AS ENUM ('ROLE_USER', 'ROLE_ADMIN' );
CREATE TYPE status AS ENUM ('ACCEPTED', 'PENDING', 'REJECTED' );

------------------------------------------------------------------------------------------------------------------------
--- TABLES
------------------------------------------------------------------------------------------------------------------------

CREATE TABLE "user" (
id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
first_name VARCHAR (255) NOT NULL,
last_name VARCHAR (255),
nickname VARCHAR (255),
login VARCHAR (255) UNIQUE NOT NULL,
password VARCHAR (255) UNIQUE NOT NULL,
address VARCHAR (255),
created_at timestamp NOT NULL,
updated_at timestamp,
deleted_at timestamp,
role VARCHAR(30) NOT NULL,
enabled boolean NOT NULL DEFAULT FALSE,
confirm_registration_date timestamp,
confirm_registration_token VARCHAR(10) UNIQUE NOT NULL,
reset_password_token VARCHAR(10) UNIQUE,
reset_password_date timestamp
);

CREATE TABLE comment (
id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
content VARCHAR(255) NOT NULL,
user_id UUID NOT NULL,
image_id UUID NOT NULL,
created_at timestamp NOT NULL,
updated_at timestamp,
deleted_at timestamp
);

CREATE TABLE image (
id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
image_url VARCHAR(255) NOT NULL,
user_id UUID NOT NULL,
created_at timestamp NOT NULL,
updated_at timestamp,
deleted_at timestamp
);

CREATE TABLE image_history (
id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
status VARCHAR(32) NOT NULL,
image_id UUID UNIQUE NOT NULL,
reason VARCHAR(255) NOT NULL,
created_at timestamp NOT NULL,
updated_at timestamp,
deleted_at timestamp
);

CREATE TABLE rank (
id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
rank int NOT NULL,
user_id UUID NOT NULL,
image_id UUID NOT NULL,
created_at timestamp NOT NULL,
updated_at timestamp,
deleted_at timestamp
);
