openapi: 3.0.0
info:
  version: "unknown-oas3"
  title: Api Documentation
servers:
  - url: /api/v1
paths:

  # ========================================
  # ===== Auth Api
  # ========================================

  /auth/confirm-registration/finish:
    post:
      tags:
        - auth
      operationId: finishConfirmRegistration
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ConfirmRegistrationRequest'
        required: true
      responses:
        201:
          description: Token is valid and account is enabled
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        404:
          description: Token is not found
        422:
          description: Token expired (living 1 hour)
      deprecated: false
  /auth/confirm-registration/init:
    post:
      tags:
        - auth
      operationId: initConfirmRegistration
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/InitSendingTokenRequest'
      responses:
        200:
          description: Email was successfully sent
        400:
          description: Email is wrong
        404:
          description: Email doesn't exist
        422:
          description: Account has been already enabled
      deprecated: false
  /auth/logout:
    post:
      description: Require sign in
      tags:
        - auth
      operationId: logout
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/TokenRequest'
        required: true
      responses:
        200:
          description: Logout success
        400:
          description: Expired or invalid JWT token in request body
      security:
        - JWT: []
      deprecated: false
  /auth/logoutAll:
    post:
      description: Require sign in
      tags:
        - auth
      operationId: logoutAll
      responses:
        200:
          description: Logout from all devices success
      security:
        - JWT: []
      deprecated: false
  /auth/refresh:
    post:
      tags:
        - auth
      operationId: refresh
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/TokenRequest'
        required: true
      responses:
        201:
          description: Token is refreshed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TokenResponse'
        401:
          description: Expired or invalid JWT token
      deprecated: false
  /auth/reset-password/finish:
    put:
      tags:
        - auth
      operationId: finishPasswordReset
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/FinishPasswordResetRequest'
        description: finishPasswordResetRequest
        required: true
      responses:
        200:
          description: Password was successfully reseted
        400:
          description: Password should contain letters and numbers
        404:
          description: Token doesn't exist
        422:
          description: Token expired (living 1 hour)
      deprecated: false
  /auth/reset-password/init:
    post:
      tags:
        - auth
      operationId: initPasswordReset
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/InitSendingTokenRequest'
      responses:
        200:
          description: Email was sent successfully
        400:
          description: Invalid email
        404:
          description: User with such email doesn't exist
      deprecated: false
  /auth/signin:
    post:
      tags:
        - auth
      operationId: signIn
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/SignInRequest'
        description: signInRequest
        required: true
      responses:
        201:
          description: Login and password are valid
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TokenResponse'
        400:
          description: Invalid email or password (should contain both letters and numbers)
        401:
          description: Wrong login and/or password
      deprecated: false
  /auth/signup:
    post:
      tags:
        - auth
      operationId: signUp
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/SignUpRequest'
        description: signUpRequest
        required: true
      responses:
        201:
          description: >-
            User was created and email was successfully sent, but account isn't
            enabled, user chould confirm registration
        400:
          description: >-
            Invalid email, password (should contain both letters and numbers) or
            first or last name (shouldn't contain numbers)
        422:
          description: User already exists or\and didn't logout
      deprecated: false

  # ========================================
  # ===== Comments Api
  # ========================================

  /comments:
    get:
      tags:
        - comments
      operationId: getComments
      parameters:
        - $ref: '#/components/parameters/Page'
        - $ref: '#/components/parameters/Size'
        - $ref: '#/components/parameters/SortParams'
        - $ref: '#/components/parameters/SearchCommentParam'
        - $ref: '#/components/parameters/IsDeleted'
      responses:
        200:
          description: Page of comments
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Comment'
        400:
          description: Wrong column name when sort
        403:
          description: User trying to access admin's functionality
      deprecated: false
    post:
      description: For users only
      tags:
        - comments
      operationId: createComment
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateCommentRequest'
        description: comment
        required: true
      responses:
        201:
          description: Created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Comment'
        400:
          description: Invalid UUID
        404:
          description: Image to create comment is not found
      security:
        - JWT: []
      deprecated: false
  /comments/all:
    get:
      tags:
        - comments
      operationId: getAllComments
      parameters:
        - $ref: '#/components/parameters/SortParams'
        - $ref: '#/components/parameters/SearchCommentParam'
        - $ref: '#/components/parameters/IsDeleted'
      responses:
        200:
          description: Page of comments
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Comment'
        400:
          description: Wrong column name when sort
        403:
          description: User trying to access admin's functionality
  /comments/{id}:
    get:
      description: Requires log in
      tags:
        - comments
      operationId: getComment
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Comment'
        400:
          description: Invalid UUID
        404:
          description: Comment doesn't exist
      security:
        - JWT: []
      deprecated: false
    put:
      description: For users only and user can put only comments passed by himself
      tags:
        - comments
      operationId: updateComment
      parameters:
        - $ref: '#/components/parameters/Id'
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdateCommentRequest'
        required: true
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Comment'
        400:
          description: Invalid UUID
        403:
          description: User trying to update another user comment
        404:
          description: Comment doesn't exist
      security:
        - JWT: []
      deprecated: false
    delete:
      description: >-
        User can delete only his own comments, admin can delete all comments,
        requires log in
      tags:
        - comments
      operationId: deleteComment
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        204:
          description: Successfully deleted
        400:
          description: Invalid UUID
        403:
          description: User trying to delete another user comments
        404:
          description: Comment doesn't exist
      security:
        - JWT: []
      deprecated: false

  # ========================================
  # ===== Images Api
  # ========================================

  /images:
    get:
      tags:
        - images
      operationId: getImages
      parameters:
        - $ref: '#/components/parameters/Page'
        - $ref: '#/components/parameters/Size'
        - $ref: '#/components/parameters/SortParams'
        - $ref: '#/components/parameters/SearchImageParam'
        - $ref: '#/components/parameters/Status'
        - $ref: '#/components/parameters/IsDeleted'
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Image'
        400:
          description: Wrong status or column name when sort
        403:
          description: User trying to access admin's functionality
      deprecated: false
    post:
      description: >-
        For users only, PENDING after creating, must be ACCEPTED by admin to
        became available
      tags:
        - images
      operationId: uploadImage
      requestBody:
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                file:
                  description: file
                  type: string
                  format: binary
              required:
                - file
      responses:
        201:
          description: Only admins have access after creating
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Image'
      security:
        - JWT: []
      deprecated: false
  /images/all:
    get:
      tags:
        - images
      operationId: getAllImages
      parameters:
        - $ref: '#/components/parameters/SortParams'
        - $ref: '#/components/parameters/SearchImageParam'
        - $ref: '#/components/parameters/Status'
        - $ref: '#/components/parameters/IsDeleted'
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Image'
        400:
          description: Wrong status or column name when sort
        403:
          description: User trying to access admin's functionality
      deprecated: false
  /images/{id}:
    delete:
      description: 'User can delete only his own images, admin can delete all images'
      tags:
        - images
      operationId: deleteImage
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        204:
          description: No Content
        403:
          description: Low access level
        404:
          description: Image doesn't exist or pending/rejected
      security:
        - JWT: []
      deprecated: false
  /images/{id}/comments:
    get:
      description: 'Return all comments given to image, requires log in'
      tags:
        - images
      operationId: getImageComments
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Comment'
        400:
          description: Invalid UUID
        404:
          description: Image doesn't exist or pending/rejected
      security:
        - JWT: []
      deprecated: false
  /images/{id}/history:
    put:
      description: For admins only, requires image id
      tags:
        - images
      operationId: updateImageHistory
      parameters:
        - $ref: '#/components/parameters/Id'
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdateImageHistoryRequest'
        description: imageHistory
        required: true
      responses:
        200:
          description: OK
        400:
          description: Invalid UUID or status of image history
        403:
          description: Low access level
        404:
          description: Image doesn't exist
      security:
        - JWT: []
      deprecated: false
  /images/{id}/ranks:
    get:
      description: 'Return all ranks given to image, requires log in'
      tags:
        - images
      operationId: getImageRanks
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Rank'
        400:
          description: Invalid UUID or rank out of range
        404:
          description: Image doesn't exist or pending/rejected
      security:
        - JWT: []
      deprecated: false

  # ========================================
  # ===== Ranks Api
  # ========================================

  /ranks:
    get:
      description: >-
        Return ranks between max and min params, max param must be greater than
        min
      tags:
        - ranks
      operationId: getRanks
      parameters:
        - $ref: '#/components/parameters/Page'
        - $ref: '#/components/parameters/Size'
        - $ref: '#/components/parameters/MinRank'
        - $ref: '#/components/parameters/MaxRank'
        - $ref: '#/components/parameters/SortParams'
        - $ref: '#/components/parameters/IsDeleted'
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Rank'
        400:
          description: >-
            Min and/or max rank out of range or invalid, or invalid column name
            when sort
        403:
          description: User trying to access admin's functionality
      deprecated: false
    post:
      description: For users only
      tags:
        - ranks
      operationId: createRank
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateRankRequest'
        description: Must be between 1 and 10
        required: true
      responses:
        201:
          description: Created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Rank'
        400:
          description: Min and/or max rank out of range
        404:
          description: Image to give a rank doesn't exist
      security:
        - JWT: []
      deprecated: false
  /ranks/all:
    get:
      description: >-
        Return ranks between max and min params or invalid, max param must be
        greater than min
      tags:
        - ranks
      operationId: getAllRanks
      parameters:
        - $ref: '#/components/parameters/MinRank'
        - $ref: '#/components/parameters/MaxRank'
        - $ref: '#/components/parameters/SortParams'
        - $ref: '#/components/parameters/IsDeleted'
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Rank'
        400:
          description: 'Min and/or max rank out of range, or invalid column name when sort'
        403:
          description: User trying to access admin's functionality
      deprecated: false
  /ranks/{id}:
    get:
      description: Requires log in
      tags:
        - ranks
      operationId: getRank
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Rank'
        400:
          description: Invalid UUID
        404:
          description: Rank doesn't exist
      security:
        - JWT: []
      deprecated: false
    put:
      description: For users only and user can put only ranks passed by himself
      tags:
        - ranks
      operationId: updateRank
      parameters:
        - $ref: '#/components/parameters/Id'
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdateRankRequest'
        required: true
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Rank'
        400:
          description: Invalid UUID
        403:
          description: User trying to update another user rank
        404:
          description: Rank doesn't exist
      security:
        - JWT: []
      deprecated: false
    delete:
      description: 'User can delete only his own ranks, admin can delete all ranks'
      tags:
        - ranks
      operationId: deleteRank
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        204:
          description: No Content
        400:
          description: Invalid UUID
        403:
          description: User trying to delete another user ranks
        404:
          description: Rank doesn't exist
      security:
        - JWT: []
      deprecated: false

  # ========================================
  # ===== Users Api
  # ========================================

  /users:
    get:
      tags:
        - users
      operationId: getUsers
      parameters:
        - $ref: '#/components/parameters/Page'
        - $ref: '#/components/parameters/Size'
        - $ref: '#/components/parameters/SortParams'
        - $ref: '#/components/parameters/SearchUserParam'
        - $ref: '#/components/parameters/Roles'
        - $ref: '#/components/parameters/IsDeleted'
        - $ref: '#/components/parameters/IsEnabled'
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/User'
        400:
          description: Wrong column name when sort or wrong role
        403:
          description: User trying to access admin's functionality
      deprecated: false
  /users/all:
    get:
      tags:
        - users
      operationId: getAllUsers
      parameters:
        - $ref: '#/components/parameters/SortParams'
        - $ref: '#/components/parameters/SearchUserParam'
        - $ref: '#/components/parameters/Roles'
        - $ref: '#/components/parameters/IsDeleted'
        - $ref: '#/components/parameters/IsEnabled'
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/User'
        400:
          description: Wrong column name when sort or wrong role
        403:
          description: User trying to access admin's functionality
      deprecated: false
  /users/current:
    get:
      description: Requires log in
      tags:
        - users
      operationId: getCurrentUser
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        404:
          description: User deleted or account wasn't enabled
      security:
        - JWT: []
      deprecated: false
  /users/password:
    put:
      description: 'Requires log in, password must contain both letters and numbers'
      tags:
        - users
      operationId: changePassword
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ChangePasswordRequest'
        description: changePasswordRequest
        required: true
      responses:
        200:
          description: OK
        400:
          description: Invalid new and/or old password
        404:
          description: Old password doesn't exist
      security:
        - JWT: []
      deprecated: false
  /users/{id}:
    get:
      description: Requires log in
      tags:
        - users
      operationId: getUser
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        400:
          description: Invalid UUID
        404:
          description: User deleted or account wasn't enabled
      security:
        - JWT: []
      deprecated: false
    delete:
      description: >-
        User can delete only his account, admin can delete all accounts,
        requires log in
      tags:
        - users
      operationId: deleteUser
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        204:
          description: No Content
        400:
          description: Invalid UUID
        403:
          description: User trying to delete another user
        404:
          description: User already deleted or doesn't exist or account wasn't enabled
      security:
        - JWT: []
      deprecated: false
  /users/{id}/activity-report:
    get:
      description: Requires log in
      tags:
        - users
      operationId: getActivityReport
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        200:
          description: Report was sent to email
        400:
          description: Invalid UUID
        404:
          description: User deleted or doesn't exist or account wasn't enabled
      security:
        - JWT: []
      deprecated: false
  /users/{id}/comments:
    get:
      description: Requires log in
      tags:
        - users
      operationId: getUserComments
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Comment'
        400:
          description: Invalid UUID
        404:
          description: User deleted or doesn't exist or account wasn't enabled
      security:
        - JWT: []
      deprecated: false
  /users/{id}/images:
    get:
      description: Requires log in
      tags:
        - users
      operationId: getUserImages
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Image'
        400:
          description: Invalid UUID
        404:
          description: User deleted or doesn't exist or account wasn't enabled
      security:
        - JWT: []
      deprecated: false
  /users/{id}/profile:
    put:
      description: Requires log in
      tags:
        - users
      operationId: updateUserProfile
      parameters:
        - $ref: '#/components/parameters/Id'
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdateUserRequest'
        required: true
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        400:
          description: Invalid first and\or last name
        404:
          description: User deleted or doesn't exist or account wasn't enabled
      security:
        - JWT: []
      deprecated: false
  /users/{id}/ranks:
    get:
      description: Requires log in
      tags:
        - users
      operationId: getUserRanks
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Rank'
        400:
          description: Invalid UUID
        404:
          description: User deleted or doesn't exist or account wasn't enabled
      security:
        - JWT: []
      deprecated: false

components:

  # ========================================
  # ===== Models
  # ========================================

  schemas:
    ChangePasswordRequest:
      description: Old and new passwords must contain both letters and numbers
      type: object
      properties:
        newPassword:
          type: string
          pattern: '(?=.*\d)[\w]{6,}'
        oldPassword:
          type: string
          pattern: '(?=.*\d)[\w]{6,}'
    Comment:
      type: object
      properties:
        content:
          type: string
        createdAt:
          type: string
          format: date-time
          readOnly: true
        deletedAt:
          type: string
          format: date-time
        id:
          type: string
          format: uuid
          readOnly: true
        imageId:
          type: string
          format: uuid
          readOnly: true
        updatedAt:
          type: string
          format: date-time
        userId:
          type: string
          format: uuid
          readOnly: true
    ConfirmRegistrationRequest:
      type: object
      properties:
        confirmRegistrationToken:
          description: 10 random symbols (numbers and letters)
          example: cSI5mINDu5
          type: string
    CreateCommentRequest:
      description: Create comment can only logged in user
      type: object
      properties:
        content:
          type: string
        imageId:
          type: string
          format: uuid
    CreateRankRequest:
      type: object
      properties:
        imageId:
          type: string
          format: uuid
        rank:
          description: Rank must be between 1 and 10
          type: integer
          format: int32
          minimum: 1
          maximum: 10
    FinishPasswordResetRequest:
      description: Token has been sent previously to email
      type: object
      properties:
        newPassword:
          description: Must contain both letters and numbers and min legth is 6
          type: string
          pattern: '(?=.*\d)[\w]{6,}'
        resetPasswordToken:
          description: 10 random symbols (numbers and letters)
          example: cSI5mINDu5
          type: string
    Image:
      type: object
      properties:
        createdAt:
          type: string
          format: date-time
          readOnly: true
        deletedAt:
          type: string
          format: date-time
        id:
          type: string
          format: uuid
          readOnly: true
        imageUrl:
          description: Url to image stored in AWS S3
          type: string
          readOnly: true
        updatedAt:
          type: string
          format: date-time
        userId:
          type: string
          format: uuid
          readOnly: true
    InitSendingTokenRequest:
      description: User has to be already signed up (but account is still enabled)
      type: object
      properties:
        email:
          type: string
          format: email
          pattern: '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$'
    Rank:
      type: object
      properties:
        createdAt:
          type: string
          format: date-time
          readOnly: true
        deletedAt:
          type: string
          format: date-time
        id:
          type: string
          format: uuid
          readOnly: true
        imageId:
          type: string
          format: uuid
          readOnly: true
        rank:
          description: Rank must be between 1 and 10
          type: integer
          format: int32
        updatedAt:
          type: string
          format: date-time
        userId:
          type: string
          format: uuid
          readOnly: true
    SignInRequest:
      description: Can be successfully done only after sign up and confirm registration
      type: object
      properties:
        email:
          type: string
          format: email
          pattern: '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$'
        password:
          description: Must contain both letters and numbers and min legth is 6
          type: string
          pattern: '(?=.*\d)[\w]{6,}'
    SignUpRequest:
      type: object
      properties:
        address:
          type: string
        email:
          type: string
          format: email
          pattern: '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$'
        firstName:
          description: Mustn't contain numbers
          pattern: \D+
          type: string
        lastName:
          description: Mustn't contain numbers
          pattern: \D+
          type: string
        nickname:
          type: string
        password:
          description: Must contain both letters and numbers and min legth is 6
          type: string
          pattern: '(?=.*\d)[\w]{6,}'
        role:
          $ref: '#/components/schemas/UserRole'
    TokenRequest:
      description: Must be valid JWT token
      type: object
      properties:
        token:
          example: >-
            eyJhbGciOiJIUzI1.eyJyb2xlIjoiUk9MRV9VU0VSIiwic3ViIjoic2FzaGFnYWxheTk5QHlhbmRleC5yd
            SIsImlhdCI6MTU2OTkxNDYwMCwiZXhwIjoxNTY5OTE0OTYwfQ.WVQU0tpr7mnYlV8ZK8HlTwTlPlbesyjpcIqxBtVNz64
          type: string
    TokenResponse:
      description: Must be valid JWT tokens
      type: object
      properties:
        accessToken:
          description: Living 1 hour
          type: string
          example: >-
            eyJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiUk9MRV9VU0VSIiwic3ViIjoic2FzaGFnYWxheTk5QHlhbmRleC5ydS
            IsImlhdCI6MTU2OTkxNDYwMCwiZXhwIjoxNTY5OTE0OTYwfQ.WVQU0tpr7mnYlV8ZK8HlTwTlPlbesyjpcIqxBtVNz64
        refreshToken:
          description: Living 100 hours
          type: string
          example: >-
            eyJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiUk9MRV9VU0VSIiwic3ViIjoic2FzaGFnYWxheTk5QHlhbmRleC5ydS
            IsImlhdCI6MTU2OTkxNDYwMCwiZXhwIjoxNTY5OTE0OTYwfQ.WVQU0tpr7mnYlV8ZK8HlTwTlPlbesyjpcIqxBtVNz64
        accessTokenExpirationDate:
          type: string
          format: date-time
        refreshTokenExpirationDate:
          type: string
          format: date-time
    UpdateCommentRequest:
      description: Update comment can only logged in user
      type: object
      properties:
        newContent:
          type: string
    UpdateImageHistoryRequest:
      description: For admins only
      type: object
      required:
        - newStatus
      properties:
        newReason:
          type: string
        newStatus:
          $ref: '#/components/schemas/ImageHistoryStatus'
      title: UpdateImageHistoryRequest
    UpdateRankRequest:
      type: object
      properties:
        newRank:
          description: Rank must be between 1 and 10
          type: integer
          format: int32
          minimum: 1
          maximum: 10
    UpdateUserRequest:
      type: object
      properties:
        newAddress:
          type: string
        newFirstName:
          description: Mustn't contain numbers
          type: string
          pattern: \D+
        newLastName:
          description: Mustn't contain numbers
          type: string
          pattern: \D+
        newNickname:
          type: string
    User:
      type: object
      properties:
        address:
          type: string
        confirmRegistrationDate:
          type: string
          format: date-time
        confirmRegistrationToken:
          description: 10 random symbols
          example: cSI5mINDu5
          type: string
        createdAt:
          type: string
          format: date-time
          readOnly: true
        deletedAt:
          type: string
          format: date-time
        enabled:
          description: False when sign up until confirm registration with token
          type: boolean
        firstName:
          description: Mustn't contain numbers
          pattern: \D+
          type: string
        id:
          type: string
          format: uuid
          readOnly: true
        lastName:
          description: Mustn't contain numbers
          pattern: \D+
          type: string
        login:
          description: Must be valid email
          type: string
          format: email
          readOnly: true
        nickname:
          type: string
        password:
          description: Must contain both letters and numbers and min legth is 6
          type: string
          pattern: '(?=.*\d)[\w]{6,}'
        resetPasswordDate:
          type: string
          format: date-time
        resetPasswordToken:
          description: 10 random symbols
          example: cSI5mINDu5
          type: string
        role:
          $ref: '#/components/schemas/UserRole'
        updatedAt:
          type: string
          format: date-time
    ImageHistoryStatus:
      type: string
      enum:
        - PENDING
        - ACCEPTED
        - REJECTED
    UserRole:
      type: string
      enum:
        - ROLE_ADMIN
        - ROLE_USER

  # ========================================
  # ===== Security
  # ========================================

  securitySchemes:
    JWT:
      type: apiKey
      name: Authorization
      in: header

  # ========================================
  # ===== Parameters
  # ========================================

  parameters:
    Id:
      name: id
      in: path
      required: true
      schema:
        type: string
        format: uuid
    IsDeleted:
      name: isDeleted
      in: query
      description: For admins only
      required: false
      schema:
        type: boolean
    IsEnabled:
      name: isEnabled
      in: query
      description: For admins only
      required: false
      schema:
        type: boolean
    MaxRank:
      in: query
      name: maxRank
      required: false
      schema:
        type: integer
        minimum: 1
        maximum: 10
        exclusiveMinimum: false
        exclusiveMaximum: false
        default: 10
    MinRank:
      in: query
      name: minRank
      required: false
      schema:
        type: integer
        minimum: 1
        maximum: 10
        exclusiveMinimum: false
        exclusiveMaximum: false
        default: 1
    Page:
      name: page
      in: query
      required: false
      schema:
        type: integer
        minimum: 1
        exclusiveMinimum: false
        default: 1
    Roles:
      name: roles
      in: query
      description: For admins only, can be only ADMIN and\or USER
      required: false
      explode: true
      schema:
        type: array
        items:
          type: string
    SearchCommentParam:
      name: searchParam
      in: query
      description: Search is on content filed
      required: false
      schema:
        type: string
    SearchImageParam:
      name: searchParam
      in: query
      description: Search is on image url field
      required: false
      schema:
        type: string
    SearchUserParam:
      name: searchParam
      in: query
      description: Search is on addrress, first, last and nick names fields
      required: false
      schema:
        type: string
    Size:
      name: size
      in: query
      required: false
      schema:
        type: integer
        minimum: 1
        exclusiveMinimum: false
        default: 100
    SortParams:
      name: sortParams
      in: query
      description: Default sorted by created date
      required: false
      explode: true
      schema:
        type: array
        items:
          type: string
    Status:
      name: status
      in: query
      description: For admins only, can be PENDING, ACCEPTED, REJECTED
      required: false
      explode: true
      schema:
        type: array
        items:
          type: string