package com.itechart.photogallery.auth.service.impl;

import com.itechart.photogallery.auth.service.AuthService;
import com.itechart.photogallery.auth.service.MailService;
import com.itechart.photogallery.common.config.JwtTokenProvider;
import com.itechart.photogallery.common.generated.model.*;
import com.itechart.photogallery.user.repository.UserRepository;
import com.itechart.photogallery.user.service.UserSearchService;
import com.itechart.photogallery.user.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

import static com.itechart.photogallery.common.utils.ExceptionUtils.*;
import static com.itechart.photogallery.common.utils.MailUtils.createContext;
import static com.itechart.photogallery.common.utils.SecurityUtils.getCurrentUser;
import static com.itechart.photogallery.user.mapper.UserMapper.USER_MAPPER;
import static java.time.temporal.ChronoUnit.SECONDS;

@Log4j2
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private static final String TOKEN_KEY = "token";
    private static final String CONFIRM_REGISTRATION_TEMPLATE = "confirm-registration-email-template";
    private static final String RESET_PASSWORD_TEMPLATE = "reset-password-email-template";

    @Value("${token.expiration.time}")
    private long tokenExpirationTime;

    private final MailService mailService;
    private final UserSearchService userSearchService;
    private final UserService userService;

    private final UserRepository userRepository;

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;

    @Override
    public User finishConfirmRegistration(ConfirmRegistrationRequest request) {
        var entity = userRepository.findByConfirmRegistrationToken(request.getConfirmRegistrationToken())
                .orElseThrow(() -> entityNotFoundException(String.format("Token %s doesn't exist",
                        request.getConfirmRegistrationToken())));
        isTokenExpired(entity.getConfirmRegistrationDate());

        entity.setEnabled(true);
        var confirmedUser = userRepository.save(entity);
        userSearchService.confirm(confirmedUser);

        log.info("IN finishConfirmRegistration - auth: {} successfully confirmed", request);
        return USER_MAPPER.toModel(confirmedUser);
    }

    @Override
    public void signUp(SignUpRequest signUpRequest) {
        isSignedIn();
        isAlreadyExists(signUpRequest.getEmail());

        log.info("IN signUp - auth: sign up success");
        userService.create(signUpRequest);
    }

    @Override
    public TokenResponse signIn(SignInRequest signInRequest) {
        var user = userRepository.findEnabledByLogin(signInRequest.getEmail()).orElseThrow(() -> {
            throw entityNotFoundException(String.format("User with email %s doesn't exist or disabled",
                    signInRequest.getEmail()));
        });

        var authenticationToken = new UsernamePasswordAuthenticationToken(user.getId(), signInRequest.getPassword());
        var authentication = authenticationManager.authenticate(authenticationToken);

        var token = jwtTokenProvider.createToken(authentication);
        jwtTokenProvider.addToCache(String.valueOf(user.getId()), token);
        return token;
    }

    @Override
    public TokenResponse refresh(TokenRequest tokenRequest) {
        return jwtTokenProvider.refreshTokens(tokenRequest.getToken());
    }

    @Override
    @Transactional
    public void initPasswordReset(InitSendingTokenRequest initSendingTokenRequest) {
        var entity = userRepository.findEnabledByLogin(initSendingTokenRequest.getEmail()).orElseThrow(() ->
                entityNotFoundException(String.format("There is no user with email %s",
                        initSendingTokenRequest.getEmail())));
        var resetPasswordToken = RandomStringUtils.random(10, true, true);

        entity.setResetPasswordDate(Instant.now());
        entity.setResetPasswordToken(resetPasswordToken);
        userRepository.save(entity);

        var context = createContext(TOKEN_KEY, resetPasswordToken);
        mailService.sendEmail(context, initSendingTokenRequest.getEmail(), RESET_PASSWORD_TEMPLATE);
    }

    @Override
    @Transactional
    public void initConfirmRegistration(InitSendingTokenRequest initSendingTokenRequest) {
        var entity = userRepository.findByLogin(initSendingTokenRequest.getEmail()).orElseThrow(() ->
                entityNotFoundException(String.format("User with email %s doesn't exists",
                        initSendingTokenRequest.getEmail())));
        isAccountEnabled(entity.getEnabled());

        var confirmRegistrationToken = RandomStringUtils.random(10, true, true);

        entity.setConfirmRegistrationDate(Instant.now());
        entity.setConfirmRegistrationToken(confirmRegistrationToken);
        userRepository.save(entity);

        var context = createContext(TOKEN_KEY, confirmRegistrationToken);
        mailService.sendEmail(context, initSendingTokenRequest.getEmail(), CONFIRM_REGISTRATION_TEMPLATE);
    }

    @Override
    public void logout(TokenRequest tokenRequest) {
        var currentUser = userService.getCurrent();
        jwtTokenProvider.logout(String.valueOf(currentUser.getId()), tokenRequest.getToken());
    }


    @Override
    public void logoutAll() {
        var currentUser = userService.getCurrent();
        jwtTokenProvider.logoutAll(String.valueOf(currentUser.getId()));
    }

    @Override
    @Transactional
    public void finishPasswordReset(FinishPasswordResetRequest finishPasswordResetRequest) {
        var entity = userRepository.findByResetPasswordToken(finishPasswordResetRequest.getResetPasswordToken())
                .orElseThrow(() -> entityNotFoundException(String.format("Token %s doesn't exist",
                        finishPasswordResetRequest.getResetPasswordToken())));
        isTokenExpired(entity.getResetPasswordDate());
        var encodedPassword = passwordEncoder.encode(finishPasswordResetRequest.getNewPassword());
        entity.setPassword(encodedPassword);
        userRepository.save(entity);
        log.info("IN finishPasswordReset - auth: password is successfully reseted");
    }

    private void isTokenExpired(Instant confirmRegistrationDate) {
        var currentDate = Instant.now();
        var expirationDate = confirmRegistrationDate.plus(tokenExpirationTime, SECONDS);

        if (currentDate.isAfter(expirationDate)) {
            throw expiredTokenException("Token expired.");
        }
    }

    private void isSignedIn() {
        if (getCurrentUser() != null) {
            throw entityAlreadyProcessedException("You can't sign up until logout");
        }
    }

    private void isAccountEnabled(Boolean enabled) {
        if (enabled) {
            throw entityAlreadyProcessedException("Account has been already enabled");
        }
    }

    private void isAlreadyExists(String login) {
        if (userRepository.existsByLogin(login)) {
            throw entityAlreadyProcessedException(String.format("User with email %s already exists", login));
        }
    }

}
