package com.itechart.photogallery.auth.service;

import com.itechart.photogallery.common.generated.model.*;

public interface AuthService {

    User finishConfirmRegistration(ConfirmRegistrationRequest request);

    TokenResponse signIn(SignInRequest signInRequest);

    TokenResponse refresh(TokenRequest tokenRequest);

    void signUp(SignUpRequest signUpRequest);

    void finishPasswordReset(FinishPasswordResetRequest finishPasswordResetRequest);

    void initPasswordReset(InitSendingTokenRequest initSendingTokenRequest);

    void initConfirmRegistration(InitSendingTokenRequest initSendingTokenRequest);

    void logout(TokenRequest tokenRequest);

    void logoutAll();

}
