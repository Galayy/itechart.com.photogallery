package com.itechart.photogallery.auth.service;

import org.thymeleaf.context.Context;

public interface MailService {

    void sendEmail(Context context, String email, String templateName);

}
