package com.itechart.photogallery.auth.service.impl;

import static java.nio.charset.StandardCharsets.UTF_8;

import static org.springframework.mail.javamail.MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED;

import com.itechart.photogallery.auth.service.MailService;

import lombok.extern.log4j.Log4j2;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@Log4j2
@Service
@RequiredArgsConstructor
public class MailServiceImpl implements MailService {

    private static final String EMAIL_MESSAGE = "Photogallery service";

    private final JavaMailSender mailSender;
    private final TemplateEngine templateEngine;

    @Override
    @SneakyThrows
    public void sendEmail(Context context, String email, String templateName) {
        var html = templateEngine.process(templateName, context);
        var message = mailSender.createMimeMessage();

        var helper = new MimeMessageHelper(message, MULTIPART_MODE_MIXED_RELATED, UTF_8.name());
        helper.setTo(email);
        helper.setText(html, true);
        helper.setSubject(EMAIL_MESSAGE);

        mailSender.send(message);
        log.info("IN sendEmail - mail: email was successfully sent to {}", email);
    }

}
