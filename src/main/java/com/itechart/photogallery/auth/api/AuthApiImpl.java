package com.itechart.photogallery.auth.api;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import javax.validation.Valid;

import com.itechart.photogallery.auth.service.AuthService;
import com.itechart.photogallery.common.generated.api.AuthApi;
import com.itechart.photogallery.common.generated.model.*;

import io.swagger.annotations.Api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@Api(tags = "auth")
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class AuthApiImpl implements AuthApi {

    private final AuthService authService;

    public ResponseEntity<Void> finishPasswordReset(@Valid @RequestBody FinishPasswordResetRequest finishPasswordResetRequest) {
        authService.finishPasswordReset(finishPasswordResetRequest);
        return new ResponseEntity<>(OK);
    }

    public ResponseEntity<Void> initPasswordReset(@Valid @RequestBody InitSendingTokenRequest initSendingTokenRequest) {
        authService.initPasswordReset(initSendingTokenRequest);
        return new ResponseEntity<>(OK);
    }

    public ResponseEntity<User> finishConfirmRegistration(@RequestBody ConfirmRegistrationRequest request) {
        var confirmedUser = authService.finishConfirmRegistration(request);
        return new ResponseEntity<>(confirmedUser, CREATED);
    }


    public ResponseEntity<Void> initConfirmRegistration(@Valid @RequestBody InitSendingTokenRequest initSendingTokenRequest) {
        authService.initConfirmRegistration(initSendingTokenRequest);
        return new ResponseEntity<>(OK);
    }

    public ResponseEntity<Void> signUp(@Valid @RequestBody SignUpRequest signUpRequest) {
        authService.signUp(signUpRequest);
        return new ResponseEntity<>(CREATED);
    }

    public ResponseEntity<TokenResponse> signIn(@Valid @RequestBody SignInRequest signInRequest) {
        var tokenResponse = authService.signIn(signInRequest);
        return new ResponseEntity<>(tokenResponse, CREATED);
    }

    public ResponseEntity<TokenResponse> refresh(@RequestBody TokenRequest tokenRequest) {
        var tokenResponse = authService.refresh(tokenRequest);
        return new ResponseEntity<>(tokenResponse, CREATED);
    }

    public ResponseEntity<Void> logout(@RequestBody TokenRequest tokenRequest) {
        authService.logout(tokenRequest);
        return new ResponseEntity<>(OK);
    }

    public ResponseEntity<Void> logoutAll() {
        authService.logoutAll();
        return new ResponseEntity<>(OK);
    }

}
