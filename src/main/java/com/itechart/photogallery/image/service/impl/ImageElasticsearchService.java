package com.itechart.photogallery.image.service.impl;

import com.itechart.photogallery.image.elastic.ImageDocument;
import com.itechart.photogallery.image.entity.ImageEntity;
import com.itechart.photogallery.image.repository.ImageElasticRepository;
import com.itechart.photogallery.image.service.ImageSearchService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.itechart.photogallery.common.utils.ExceptionUtils.entityNotFoundException;

@Log4j2
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(name = "spring.elasticsearch.enabled", havingValue = "true")
public class ImageElasticsearchService implements ImageSearchService {

    private static final String NAME = "image";

    private final ImageElasticRepository imageElasticRepository;

    @Override
    public void create(ImageEntity entity) {
        var document = new ImageDocument();
        document.setId(entity.getId());
        document.setImageUrl(entity.getImageUrl());
        imageElasticRepository.save(document);
        log.info("IN create - image elasticsearch: success");
    }

    @Override
    public void delete(UUID id) {
        var document = imageElasticRepository.findById(id).orElseThrow(() -> entityNotFoundException(NAME, id));
        document.setDeletedAt(Date.from(Instant.now()));
        imageElasticRepository.save(document);
        log.info("IN delete - image elasticsearch: success");
    }

    @Override
    public List<UUID> getIds(String searchParam, Boolean isDeleted, Pageable pageable) {
        final Page<ImageDocument> documents;
        if (searchParam == null) {
            documents = findWithDeletedFilterParam(isDeleted, pageable);
        } else {
            documents = findWithDeletedFilterParam(searchParam, isDeleted, pageable);
        }

        var imageIds = documents.stream().map(ImageDocument::getId).collect(Collectors.toList());
        log.info("IN getIds - image elasticsearch: {} images were found", imageIds.size());
        return imageIds;
    }

    private Page<ImageDocument> findWithDeletedFilterParam(String searchParam, Boolean isDeleted, Pageable pageable) {
        if (isDeleted == null) {
            return imageElasticRepository.searchAll(searchParam, pageable);
        } else if (isDeleted) {
            return imageElasticRepository.searchDeleted(searchParam, pageable);
        } else {
            return imageElasticRepository.searchExisted(searchParam, pageable);
        }
    }

    private Page<ImageDocument> findWithDeletedFilterParam(Boolean isDeleted, Pageable pageable) {
        if (isDeleted == null) {
            return imageElasticRepository.findAll(pageable);
        } else if (isDeleted) {
            return imageElasticRepository.findDeleted(pageable);
        } else {
            return imageElasticRepository.findExisted(pageable);
        }
    }

}
