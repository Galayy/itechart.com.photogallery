package com.itechart.photogallery.image.service;

import java.util.List;
import java.util.UUID;

import com.itechart.photogallery.common.generated.model.Image;
import com.itechart.photogallery.common.generated.model.UpdateImageHistoryRequest;
import com.itechart.photogallery.common.service.CommonService;

import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

public interface ImageService extends CommonService<Image> {

    Image upload(MultipartFile file);

    List<Image> getByUserId(UUID id);

    List<Image> getAll(List<String> sortParams, String searchParam, List<String> status, Boolean isDeleted);

    Page<Image> getPage(List<String> sortParams, String searchParam, List<String> status, Boolean isDeleted,
                        Integer page, Integer size);

    void updateHistory(UUID imageId, UpdateImageHistoryRequest imageHistory);

}
