package com.itechart.photogallery.image.service.impl;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.itechart.photogallery.common.entity.AbstractEntity;
import com.itechart.photogallery.image.entity.ImageEntity;
import com.itechart.photogallery.image.repository.ImageRepository;
import com.itechart.photogallery.image.service.ImageSearchService;

import lombok.extern.log4j.Log4j2;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Log4j2
@Service
@RequiredArgsConstructor
@ConditionalOnMissingBean(ImageElasticsearchService.class)
public class ImageFulltextSearchService implements ImageSearchService {

    private final ImageRepository imageRepository;

    @Override
    public List<UUID> getIds(String searchParam, Boolean isDeleted, Pageable pageable) {
        var images = imageRepository.search(searchParam, isDeleted, pageable).getContent();
        var imagesIds = images.stream().map(AbstractEntity::getId).collect(Collectors.toList());
        log.info("IN getIds - image fulltext search: {} ids were found", imagesIds.size());
        return imagesIds;
    }

    @Override
    public void create(ImageEntity entity) { }

    @Override
    public void delete(UUID id) { }

}
