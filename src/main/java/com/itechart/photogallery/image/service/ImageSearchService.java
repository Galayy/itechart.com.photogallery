package com.itechart.photogallery.image.service;

import java.util.List;
import java.util.UUID;

import com.itechart.photogallery.common.service.CommonSearchService;
import com.itechart.photogallery.image.entity.ImageEntity;

import org.springframework.data.domain.Pageable;

public interface ImageSearchService extends CommonSearchService<ImageEntity> {

    List<UUID> getIds(String searchParam, Boolean isDeleted, Pageable pageable);

}
