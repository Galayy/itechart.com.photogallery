package com.itechart.photogallery.image.service.impl;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import static com.itechart.photogallery.common.utils.ExceptionUtils.entityNotFoundException;
import static com.itechart.photogallery.common.utils.FilterUtils.checkFilterParams;
import static com.itechart.photogallery.common.utils.FilterUtils.createDeletedFilterParamOrDefault;
import static com.itechart.photogallery.common.utils.PagingUtils.toPageable;
import static com.itechart.photogallery.common.utils.SecurityUtils.hasAdminAccessLevel;
import static com.itechart.photogallery.common.utils.SortingUtils.parseSortOrDefault;
import static com.itechart.photogallery.image.mapper.ImageHistoryStatusMapper.IMAGE_HISTORY_STATUS_MAPPER;

import static org.apache.commons.lang3.StringUtils.trimToEmpty;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import com.itechart.photogallery.common.generated.model.Image;
import com.itechart.photogallery.common.generated.model.ImageHistoryStatus;
import com.itechart.photogallery.common.generated.model.UpdateImageHistoryRequest;
import com.itechart.photogallery.common.service.CachingService;
import com.itechart.photogallery.image.entity.ImageEntity;
import com.itechart.photogallery.image.entity.ImageHistoryEntity;
import com.itechart.photogallery.image.filter.ImageHistoryStatusFilter;
import com.itechart.photogallery.image.model.CachedImage;
import com.itechart.photogallery.image.repository.ImageHistoryRepository;
import com.itechart.photogallery.image.service.ImageService;
import com.itechart.photogallery.user.service.UserService;

import lombok.extern.log4j.Log4j2;

import org.redisson.api.RedissonClient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@Log4j2
@Primary
@Service
@RequiredArgsConstructor
public class CachingImageServiceImpl implements ImageService {

    @Value("${app.redis.image-ttl}")
    private Integer imageTtl;

    private static final String ALL_IMAGES_PREFIX = "all";
    private static final String IMAGE_COMPLETE_KEY = "image:%s:complete";
    private static final String IMAGE_LOCK_KEY = "image:%s:lock";
    private static final String IMAGE_KEY = "image:%s";

    private static final Integer DEFAULT_PAGE = 1;
    private static final Integer DEFAULT_SIZE = 1000;
    private static final Integer WAIT_TIME_MILLIS = 1000;

    private final ImageHistoryRepository imageHistoryRepository;

    private final CachingService cachingService;
    private final ImageService imageService;
    private final UserService userService;

    private final RedissonClient redissonClient;

    private final ImageHistoryStatusFilter imageHistoryStatusFilter;

    @Override
    @SneakyThrows
    public Image upload(MultipartFile file) {
        return imageService.upload(file);
    }

    @Override
    public List<Image> getByUserId(UUID id) {
        var key = String.format(IMAGE_KEY, ALL_IMAGES_PREFIX);
        var completeKey = String.format(IMAGE_COMPLETE_KEY, ALL_IMAGES_PREFIX);

        if (cachingService.absentsEntry(key) || cachingService.absentsEntry(completeKey)) {
            var images = imageService.getByUserId(id);
            populateImagesCache(key, completeKey, images);
        }
        userService.getById(id);

        var cachedImages = redissonClient.getScoredSortedSet(key)
                .stream()
                .map(CachedImage.class::cast)
                .filter(image -> image.getUserId().equals(id))
                .collect(Collectors.toList());

        log.info("IN getByUserId - image cache: {} images were found from cache", cachedImages.size());
        return cachedImages.stream().map(this::toImage).collect(Collectors.toList());
    }

    @Override
    public List<Image> getAll(List<String> sortParams, String searchParam, List<String> status, Boolean isDeleted) {
        var images = getImagesPage(sortParams, searchParam, status, isDeleted, DEFAULT_PAGE, DEFAULT_SIZE).getContent();
        log.info("IN getAll - image cache: {} images were found from cache", images.size());
        return images;
    }

    @Override
    public Page<Image> getPage(List<String> sortParams, String searchParam, List<String> status, Boolean isDeleted,
                               Integer page, Integer size) {
        var images =  getImagesPage(sortParams, searchParam, status, isDeleted, page, size);
        log.info("IN getPage - image cache: {} images were found from cache", images.getContent().size());
        return images;
    }

    @Override
    public void updateHistory(UUID imageId, UpdateImageHistoryRequest imageHistory) {
        imageService.updateHistory(imageId, imageHistory);
    }

    @Override
    public Image getById(UUID id) {
        var key = String.format(IMAGE_KEY, id);
        var completeKey = String.format(IMAGE_COMPLETE_KEY, id);
        if (cachingService.absentsEntry(key) || cachingService.absentsEntry(completeKey)) {
            populateImageCache(id, key, completeKey);
        }

        log.info("IN getById - image cache: image was found from cache");
        return Optional.ofNullable(redissonClient.getBucket(key).get())
                .map(CachedImage.class::cast)
                .map(this::toImage)
                .orElseThrow(() -> entityNotFoundException("image", id));
    }

    @Override
    public void deleteById(UUID id) {
        imageService.deleteById(id);
    }

    private Page<Image> getImagesPage(List<String> sortParams, String searchParam, List<String> status,
                                      Boolean isDeleted, Integer page, Integer size) {
        if (!hasAdminAccessLevel()) {
            checkFilterParams(isDeleted, status);
        }

        var key = String.format(IMAGE_KEY, ALL_IMAGES_PREFIX);
        var completeKey = String.format(IMAGE_COMPLETE_KEY, ALL_IMAGES_PREFIX);

        if (cachingService.absentsEntry(key) || cachingService.absentsEntry(completeKey)) {
            var images = imageService.getAll(sortParams, searchParam, status, isDeleted);
            populateImagesCache(key, completeKey, images);
        }
        var preparedStatus = imageHistoryStatusFilter.createFilterParam(status)
                .stream()
                .map(IMAGE_HISTORY_STATUS_MAPPER::toStatusModel)
                .collect(Collectors.toList());
        var preparedDeletedFilterParam = createDeletedFilterParamOrDefault(isDeleted);

        var cachedImages = getImages(key, searchParam, preparedStatus, preparedDeletedFilterParam);
        var images = cachedImages.stream().map(this::toImage).collect(Collectors.toList());

        var sort = parseSortOrDefault(sortParams, ImageEntity.class);
        var pageable = toPageable(page, size, sort);
        return new PageImpl<>(images, pageable, cachedImages.size());
    }

    @SneakyThrows
    private void populateImageCache(UUID id, String key, String completeKey) {
        var image = imageService.getById(id);
        var imageHistoryEntity = imageHistoryRepository.findByImageId(image.getId())
                .orElseThrow(() -> entityNotFoundException("image", image.getId()));
        var cachedImage = toCachedImage(image, imageHistoryEntity);
        var lock = redissonClient.getLock(String.format(IMAGE_LOCK_KEY, id));

        try {
            lock.tryLock(WAIT_TIME_MILLIS, MILLISECONDS);
            cachingService.save(key, imageTtl, cachedImage);
            cachingService.save(completeKey, imageTtl, String.valueOf(id));
            log.info("IN populateImageCache - image cache: image was successfully cached");
        } finally {
            lock.unlock();
        }
    }

    @SneakyThrows
    private void populateImagesCache(String key, String completeKey, List<Image> images) {
        var cachedImages = createCachedImagesList(images);
        var lock = redissonClient.getLock(String.format(IMAGE_LOCK_KEY, ALL_IMAGES_PREFIX));

        try {
            lock.tryLock(WAIT_TIME_MILLIS, MILLISECONDS);
            cachingService.saveAll(key, imageTtl, cachedImages);
            cachingService.save(completeKey, imageTtl, ALL_IMAGES_PREFIX);
            log.info("IN populateImagesCache - image cache: {} images were successfully cached",
                    cachedImages.size());
        } finally {
            lock.unlock();
        }
    }

    private List<CachedImage> getImages(String key, String searchParam, List<ImageHistoryStatus> preparedStatus,
                                        Boolean preparedDeletedFilterParam) {
        var query = trimToEmpty(searchParam);
        return redissonClient.getScoredSortedSet(key)
                .stream()
                .map(CachedImage.class::cast)
                .filter(cachedImage -> {
                    if (preparedDeletedFilterParam == null) {
                        return cachedImage.getImageUrl().contains(query)
                                && preparedStatus.contains(cachedImage.getStatus());
                    } else if (preparedDeletedFilterParam) {
                        return cachedImage.getImageUrl().contains(query) && cachedImage.getDeletedAt() != null
                                && preparedStatus.contains(cachedImage.getStatus());
                    } else {
                        return cachedImage.getImageUrl().contains(query) && cachedImage.getDeletedAt() == null
                                && preparedStatus.contains(cachedImage.getStatus());
                    }
                })
                .collect(Collectors.toList());
    }

    private CachedImage toCachedImage(Image image, ImageHistoryEntity imageHistoryEntity) {
        var cachedImage = new CachedImage();

        cachedImage.setId(image.getId());
        cachedImage.setImageUrl(image.getImageUrl());
        cachedImage.setUserId(image.getUserId());
        cachedImage.setCreatedAt(image.getCreatedAt());
        cachedImage.setDeletedAt(image.getDeletedAt());
        cachedImage.setUpdatedAt(image.getUpdatedAt());
        cachedImage.setStatus(IMAGE_HISTORY_STATUS_MAPPER.toStatusModel(imageHistoryEntity.getStatus()));

        return cachedImage;
    }

    private Image toImage(CachedImage cachedImage) {
        var image = new Image();

        image.setId(cachedImage.getId());
        image.setImageUrl(cachedImage.getImageUrl());
        image.setUserId(cachedImage.getUserId());
        image.setCreatedAt(cachedImage.getCreatedAt());
        image.setDeletedAt(cachedImage.getDeletedAt());
        image.setUpdatedAt(cachedImage.getUpdatedAt());

        return image;
    }

    private List<CachedImage> createCachedImagesList(List<Image> images) {
        return images.stream()
                .map(image -> {
                    var imageHistoryEntity = imageHistoryRepository.findByImageId(image.getId())
                            .orElseThrow(() -> entityNotFoundException("image", image.getId()));
                    return toCachedImage(image, imageHistoryEntity);
                })
                .collect(Collectors.toList());
    }

}
