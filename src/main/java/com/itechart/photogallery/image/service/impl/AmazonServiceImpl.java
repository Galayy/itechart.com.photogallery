package com.itechart.photogallery.image.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Objects;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;

import com.itechart.photogallery.image.service.AmazonService;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@Log4j2
@Service
@RequiredArgsConstructor
public class AmazonServiceImpl implements AmazonService {

    private final AmazonS3 s3client;

    @Value("${amazonProperties.bucketName}")
    private String bucketName;

    @Value("${amazonProperties.endpointUrl}")
    private String endpointUrl;

    @Override
    @SneakyThrows
    public String uploadImage(MultipartFile file) {
        var fileName = file.getOriginalFilename();
        var image = new File(Objects.requireNonNull(fileName));

        var imageOutputStream = new FileOutputStream(image);
        imageOutputStream.write(file.getBytes());
        imageOutputStream.close();

        var putObjectRequest = new PutObjectRequest(bucketName, fileName, image);
        s3client.putObject(putObjectRequest);
        log.info("IN uploadImage - amazon: image was successfully uploaded");
        return endpointUrl + "/" + bucketName + "/" + fileName;
    }

}
