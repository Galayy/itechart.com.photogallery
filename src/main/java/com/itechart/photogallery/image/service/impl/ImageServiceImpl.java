package com.itechart.photogallery.image.service.impl;

import com.itechart.photogallery.comment.repository.CommentRepository;
import com.itechart.photogallery.common.generated.model.Image;
import com.itechart.photogallery.common.generated.model.UpdateImageHistoryRequest;
import com.itechart.photogallery.image.entity.ImageEntity;
import com.itechart.photogallery.image.entity.ImageHistoryEntity;
import com.itechart.photogallery.image.filter.ImageHistoryStatusFilter;
import com.itechart.photogallery.image.repository.ImageHistoryRepository;
import com.itechart.photogallery.image.repository.ImageRepository;
import com.itechart.photogallery.image.service.AmazonService;
import com.itechart.photogallery.image.service.ImageSearchService;
import com.itechart.photogallery.image.service.ImageService;
import com.itechart.photogallery.rank.repository.RankRepository;
import com.itechart.photogallery.user.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.itechart.photogallery.common.utils.ExceptionUtils.entityNotFoundException;
import static com.itechart.photogallery.common.utils.ExceptionUtils.isEnoughPermissions;
import static com.itechart.photogallery.common.utils.FilterUtils.checkFilterParams;
import static com.itechart.photogallery.common.utils.FilterUtils.createDeletedFilterParamOrDefault;
import static com.itechart.photogallery.common.utils.PagingUtils.toPageable;
import static com.itechart.photogallery.common.utils.SecurityUtils.hasAdminAccessLevel;
import static com.itechart.photogallery.common.utils.SortingUtils.parseSortOrDefault;
import static com.itechart.photogallery.image.entity.enums.ImageHistoryEntityStatus.ACCEPTED;
import static com.itechart.photogallery.image.entity.enums.ImageHistoryEntityStatus.PENDING;
import static com.itechart.photogallery.image.mapper.ImageHistoryStatusMapper.IMAGE_HISTORY_STATUS_MAPPER;
import static com.itechart.photogallery.image.mapper.ImageMapper.IMAGE_MAPPER;
import static java.util.Collections.emptyList;

@Log4j2
@Service
@RequiredArgsConstructor
public class ImageServiceImpl implements ImageService {

    private static final String NAME = "image";
    private static final String PENDING_REASON = "Waiting for approve";

    private final AmazonService amazonService;
    private final ImageSearchService imageSearchService;
    private final UserService userService;

    private final CommentRepository commentRepository;
    private final ImageRepository imageRepository;
    private final ImageHistoryRepository imageHistoryRepository;
    private final RankRepository rankRepository;

    private final ImageHistoryStatusFilter imageHistoryStatusFilter;

    @Override
    @Transactional(readOnly = true)
    public Image getById(UUID id) {
        var entity = imageRepository.findById(id).filter(this::filterImages).orElseThrow(() ->
                entityNotFoundException(NAME, id));
        log.info("IN getById - image: image was successfully found");
        return IMAGE_MAPPER.toModel(entity);
    }

    @Override
    @Transactional
    public Image upload(MultipartFile file) {
        var currentUser = userService.getCurrent();
        var fileUrl = amazonService.uploadImage(file);

        var imageEntity = new ImageEntity();
        imageEntity.setImageUrl(fileUrl);
        imageEntity.setUserId(currentUser.getId());

        var savedImageEntity = imageRepository.save(imageEntity);

        var imageHistoryEntity = new ImageHistoryEntity();
        imageHistoryEntity.setStatus(PENDING);
        imageHistoryEntity.setImageId(savedImageEntity.getId());
        imageHistoryEntity.setReason(PENDING_REASON);
        imageHistoryRepository.save(imageHistoryEntity);

        imageSearchService.create(savedImageEntity);
        log.info("IN upload - image: image and image history were successfully created");
        return IMAGE_MAPPER.toModel(savedImageEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Image> getAll(List<String> sortParams, String searchParam, List<String> status, Boolean isDeleted) {
        if (!hasAdminAccessLevel()) {
            checkFilterParams(isDeleted, status);
        }
        var size = imageRepository.findAll().size();
        var elasticPageable = toPageable(1, size);

        var preparedDeletedFilterParam = createDeletedFilterParamOrDefault(isDeleted);
        var preparedImagesIds = imageSearchService.getIds(searchParam, preparedDeletedFilterParam, elasticPageable);

        if (preparedImagesIds.isEmpty()) {
            log.info("IN getAll - image: 0 images were found");
            return emptyList();
        }
        var preparedStatus = imageHistoryStatusFilter.createFilterParam(status);
        var imagesIdAccordingToStatus = imageHistoryRepository.search(preparedStatus, preparedImagesIds)
                .stream()
                .map(ImageHistoryEntity::getImageId)
                .collect(Collectors.toList());

        if (imagesIdAccordingToStatus.isEmpty()) {
            log.info("IN getAll - image: 0 images were found");
            return emptyList();
        }
        var sort = parseSortOrDefault(sortParams, ImageEntity.class);
        var images = imageRepository.search(imagesIdAccordingToStatus, sort)
                .stream()
                .map(IMAGE_MAPPER::toModel)
                .collect(Collectors.toList());
        log.info("IN getAll - image: {} images were found", images.size());
        return images;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Image> getPage(List<String> sortParams, String searchParam, List<String> status, Boolean isDeleted,
                               Integer page, Integer size) {
        if (!hasAdminAccessLevel()) {
            checkFilterParams(isDeleted, status);
        }

        var elasticPageable = toPageable(page, size);
        var preparedDeletedFilterParam = createDeletedFilterParamOrDefault(isDeleted);
        var preparedImagesIds = imageSearchService.getIds(searchParam, preparedDeletedFilterParam, elasticPageable);

        if (preparedImagesIds.isEmpty()) {
            log.info("IN getPage - image: 0 images were found");
            return Page.empty();
        }
        var preparedStatus = imageHistoryStatusFilter.createFilterParam(status);
        var imagesIdAccordingToStatus = imageHistoryRepository.search(preparedStatus, preparedImagesIds)
                .stream()
                .map(ImageHistoryEntity::getImageId)
                .collect(Collectors.toList());

        if (imagesIdAccordingToStatus.isEmpty()) {
            log.info("IN getPage - image: 0 images were found");
            return Page.empty();
        }
        var sort = parseSortOrDefault(sortParams, ImageEntity.class);
        var pageable = toPageable(page, size, sort);
        var images = imageRepository.search(imagesIdAccordingToStatus, pageable)
                .map(IMAGE_MAPPER::toModel);
        log.info("IN getPage - image: {} images were found", images.getContent().size());
        return images;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Image> getByUserId(UUID userId) {
        userService.getById(userId);
        var images = imageRepository.findAllByUserId(userId)
                .stream()
                .filter(this::filterImages)
                .map(IMAGE_MAPPER::toModel)
                .collect(Collectors.toList());
        log.info("IN getByUserId - image: {} images were found", images.size());
        return images;
    }

    @Override
    @Transactional
    public void deleteById(UUID id) {
        var currentUser = userService.getCurrent();
        var entity = imageRepository.findById(id).orElseThrow(() -> entityNotFoundException(NAME, id));
        isEnoughPermissions(currentUser, entity.getUserId());

        commentRepository.deleteByImageId(entity.getId(), Instant.now());
        rankRepository.deleteByImageId(entity.getId(), Instant.now());

        imageHistoryRepository.deleteByImageId(entity.getId(), Instant.now());
        imageRepository.deleteById(id, Instant.now());
        imageSearchService.delete(id);
        log.info("IN deleteById - image: image was successfully deleted");
    }

    @Override
    @Transactional
    public void updateHistory(UUID imageId, UpdateImageHistoryRequest imageHistory) {
        var entity = imageHistoryRepository.findByImageId(imageId).orElseThrow(() -> entityNotFoundException(NAME,
                imageId));

        entity.setStatus(IMAGE_HISTORY_STATUS_MAPPER.toEntity(imageHistory.getNewStatus()));
        entity.setReason(imageHistory.getNewReason());
        var updatedImageHistoryEntity = imageHistoryRepository.save(entity);

        if (!updatedImageHistoryEntity.getStatus().equals(ACCEPTED)) {
            commentRepository.deleteByImageId(imageId, Instant.now());
            rankRepository.deleteByImageId(imageId, Instant.now());

            imageRepository.deleteById(imageId, Instant.now());
            imageSearchService.delete(imageId);
        }
        log.info("IN updateHistory - image: image history was successfully updated");
    }

    private boolean filterImages(ImageEntity entity) {
        var historyEntity = imageHistoryRepository.findByImageId(entity.getId()).orElseThrow(() ->
                entityNotFoundException(String.format("There is no history with id %s", entity.getId())));
        return historyEntity.getStatus().equals(ACCEPTED);
    }

}
