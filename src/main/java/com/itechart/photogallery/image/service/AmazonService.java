package com.itechart.photogallery.image.service;

import org.springframework.web.multipart.MultipartFile;

public interface AmazonService {

    String uploadImage(MultipartFile file);

}
