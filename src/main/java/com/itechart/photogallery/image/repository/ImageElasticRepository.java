package com.itechart.photogallery.image.repository;

import java.util.UUID;

import com.itechart.photogallery.image.elastic.ImageDocument;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
@ConditionalOnProperty(name = "spring.elasticsearch.enabled", havingValue = "true")
public interface ImageElasticRepository extends ElasticsearchRepository<ImageDocument, UUID> {

    @Query("{\"bool\": {\"must\": {\"match\":{\"imageUrl\":\"?0\"}}}}")
    Page<ImageDocument> searchAll(String imageUrl, Pageable pageable);

    @Query("{\"bool\": {\"must\":[" +
               "{\"match\":{\"imageUrl\":\"?0\"}}," +
               "{\"exists\" : {\"field\": \"deletedAt\"}}" +
            "]}}")
    Page<ImageDocument> searchDeleted(String imageUrl, Pageable pageable);

    @Query("{\"bool\": {\"must\":[" +
               "{\"match\":{\"ImageUrl\":\"?0\"}}," +
               "{\"bool\":{ \"must_not\": {\"exists\": {\"field\": \"deletedAt\"}}}}" +
            "]}}")
    Page<ImageDocument> searchExisted(String imageUrl, Pageable pageable);

    @Query("{\"bool\": {\"must_not\": {\"exists\": {\"field\": \"deletedAt\"}}}}")
    Page<ImageDocument> findExisted(Pageable pageable);

    @Query("{\"bool\": {\"must\": {\"exists\": {\"field\": \"deletedAt\"}}}}")
    Page<ImageDocument> findDeleted(Pageable pageable);

}
