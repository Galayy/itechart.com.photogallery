package com.itechart.photogallery.image.repository;

import com.itechart.photogallery.image.entity.ImageHistoryEntity;
import com.itechart.photogallery.image.entity.enums.ImageHistoryEntityStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ImageHistoryRepository extends JpaRepository<ImageHistoryEntity, UUID> {

    @Query("SELECT imageHistory " +
           "FROM ImageHistoryEntity imageHistory " +
           "WHERE imageHistory.id = :id" +
           " AND imageHistory.deletedAt IS NULL")
    Optional<ImageHistoryEntity> findById(@Param("id") UUID id);

    @Query("SELECT imageHistory " +
           "FROM ImageHistoryEntity imageHistory " +
           "WHERE imageHistory.imageId = :imageId" +
           " AND imageHistory.deletedAt IS NULL")
    Optional<ImageHistoryEntity> findByImageId(@Param("imageId") UUID imageId);

    @Modifying
    @Query("UPDATE ImageHistoryEntity imageHistory " +
           "SET imageHistory.deletedAt = :date " +
           "WHERE imageHistory.imageId = :imageId")
    void deleteByImageId(@Param("imageId") UUID imageId, @Param("date") Instant date);

    @Query("SELECT imageHistory " +
           "FROM ImageHistoryEntity imageHistory " +
           "WHERE imageHistory.status IN :status" +
           " AND imageHistory.deletedAt IS NULL" +
           " AND imageHistory.imageId IN :imagesIds")
    List<ImageHistoryEntity> search(@Param("status") List<ImageHistoryEntityStatus> status,
                                    @Param("imagesIds") List<UUID> imagesIds);

}
