package com.itechart.photogallery.image.repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.itechart.photogallery.image.entity.ImageEntity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends JpaRepository<ImageEntity, UUID> {

    @Modifying
    @Query("UPDATE ImageEntity image " +
           "SET image.deletedAt = :date " +
           "WHERE image.id = :id")
    void deleteById(@Param("id") UUID id, @Param("date") Instant date);

    @Modifying
    @Query("UPDATE ImageEntity image " +
           "SET image.deletedAt = :date " +
           "WHERE image.userId = :userId")
    void deleteByUserId(@Param("userId") UUID userId, @Param("date") Instant date);

    @Query("SELECT image " +
           "FROM ImageEntity image " +
           "WHERE image.userId = :userId" +
           " AND image.deletedAt IS NULL")
    List<ImageEntity> findAllByUserId(@Param("userId") UUID userId);

    @Query("SELECT image " +
           "FROM ImageEntity image " +
           "WHERE image.userId = :userId" +
           " AND image.createdAt >= :afterDate")
    List<ImageEntity> findAllByUserIdAndAfterDate(@Param("userId") UUID userId, @Param("afterDate") Instant afterDate);

    @Query("SELECT image " +
           "FROM ImageEntity image " +
           "WHERE image.id = :id" +
           " AND image.deletedAt IS NULL")
    Optional<ImageEntity> findById(@Param("id") UUID id);

    @Query("SELECT image " +
           "FROM ImageEntity image " +
           "WHERE image.id IN :imageIds")
    List<ImageEntity> search(@Param("imageIds") List<UUID> imageIds, Sort sort);

    @Query("SELECT image " +
           "FROM ImageEntity image " +
           "WHERE image.id IN :imageIds")
    Page<ImageEntity> search(@Param("imageIds") List<UUID> imageIds, Pageable pageable);

    @Query("SELECT image " +
           "FROM ImageEntity image " +
           "WHERE (:searchParam IS NULL OR image.imageUrl LIKE %:searchParam%)" +
           " AND (:isDeleted IS NULL OR :isDeleted = true AND image.deletedAt IS NOT NULL " +
           "      OR :isDeleted = false AND image.deletedAt IS NULL)")
    Page<ImageEntity> search(@Param("searchParam") String searchParam, @Param("isDeleted") Boolean isDeleted,
                             Pageable pageable);

}
