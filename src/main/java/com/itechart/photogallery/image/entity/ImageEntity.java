package com.itechart.photogallery.image.entity;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.itechart.photogallery.common.entity.AbstractEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "image")
@EqualsAndHashCode(callSuper = true)
public class ImageEntity extends AbstractEntity {

    @Column(name = "image_url", nullable = false)
    private String imageUrl;

    @Column(name = "user_id", nullable = false, updatable = false)
    private UUID userId;

}
