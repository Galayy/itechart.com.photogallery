package com.itechart.photogallery.image.entity.enums;

import java.util.AbstractMap;
import java.util.Map;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ImageHistoryEntityStatus {

    PENDING("PENDING"), ACCEPTED("ACCEPTED"), REJECTED("REJECTED");

    private static final Map<String, ImageHistoryEntityStatus> MAP = Map.ofEntries(
            new AbstractMap.SimpleEntry<>("PENDING", PENDING),
            new AbstractMap.SimpleEntry<>("ACCEPTED", ACCEPTED),
            new AbstractMap.SimpleEntry<>("REJECTED", REJECTED));

    private final String authority;

    public static ImageHistoryEntityStatus fromAuthority(String authority) {
        return MAP.get(authority);
    }

}
