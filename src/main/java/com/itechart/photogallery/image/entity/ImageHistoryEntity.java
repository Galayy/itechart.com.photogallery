package com.itechart.photogallery.image.entity;

import java.util.UUID;
import javax.persistence.*;

import com.itechart.photogallery.common.entity.AbstractEntity;
import com.itechart.photogallery.image.entity.enums.ImageHistoryEntityStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "image_history")
@EqualsAndHashCode(callSuper = true)
public class ImageHistoryEntity extends AbstractEntity {

    @Column(name = "image_id", nullable = false, updatable = false)
    private UUID imageId;

    @Column(name = "reason")
    private String reason;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private ImageHistoryEntityStatus status;

}
