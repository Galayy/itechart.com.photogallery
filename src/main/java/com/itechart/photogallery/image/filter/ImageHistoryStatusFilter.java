package com.itechart.photogallery.image.filter;

import com.itechart.photogallery.common.filter.Filter;
import com.itechart.photogallery.image.entity.enums.ImageHistoryEntityStatus;

public interface ImageHistoryStatusFilter extends Filter<ImageHistoryEntityStatus> {

}
