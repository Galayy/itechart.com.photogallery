package com.itechart.photogallery.image.filter.impl;

import static java.util.Collections.singletonList;

import static com.itechart.photogallery.common.utils.ExceptionUtils.invalidInputDataException;
import static com.itechart.photogallery.common.utils.SecurityUtils.hasAdminAccessLevel;
import static com.itechart.photogallery.image.entity.enums.ImageHistoryEntityStatus.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.itechart.photogallery.image.entity.enums.ImageHistoryEntityStatus;
import com.itechart.photogallery.image.filter.ImageHistoryStatusFilter;

import org.springframework.stereotype.Component;

@Component
public class ImageHistoryStatusFilterImpl implements ImageHistoryStatusFilter {

    @Override
    public List<ImageHistoryEntityStatus> createFilterParam(List<String> filterParams) {
        if (hasAdminAccessLevel()) {
            return prepareFilterParams(filterParams);
        }
        return singletonList(ACCEPTED);
    }

    @Override
    public List<ImageHistoryEntityStatus> prepareFilterParams(List<String> filterParams) {
        if (filterParams == null) {
            return List.of(PENDING, ACCEPTED, REJECTED);
        }
        return filterParams.stream().map(this::validateFilterParam).collect(Collectors.toList());
    }

    @Override
    public ImageHistoryEntityStatus validateFilterParam(String filterParam) {
        return Optional.ofNullable(filterParam)
                .map(ImageHistoryEntityStatus::fromAuthority)
                .orElseThrow(() -> invalidInputDataException("Invalid status name"));
    }

}
