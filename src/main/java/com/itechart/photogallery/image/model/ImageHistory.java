package com.itechart.photogallery.image.model;

import java.util.UUID;

import com.itechart.photogallery.common.entity.AbstractEntity;
import com.itechart.photogallery.common.generated.model.ImageHistoryStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ImageHistory extends AbstractEntity {

    private UUID id;
    private UUID imageId;
    private String reason;
    private ImageHistoryStatus status;

}
