package com.itechart.photogallery.image.model;

import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

import com.itechart.photogallery.common.generated.model.ImageHistoryStatus;

import lombok.Data;

@Data
public class CachedImage implements Serializable {

    private UUID id;
    private String imageUrl;
    private ImageHistoryStatus status;
    private UUID userId;

    private Instant createdAt;
    private Instant deletedAt;
    private Instant updatedAt;

}
