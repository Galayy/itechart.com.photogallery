package com.itechart.photogallery.image.api;

import static com.itechart.photogallery.common.utils.HeaderUtils.generatePaginationHeaders;

import static org.springframework.http.HttpStatus.*;

import java.util.List;
import java.util.UUID;

import com.itechart.photogallery.comment.service.CommentService;
import com.itechart.photogallery.common.generated.api.ImagesApi;
import com.itechart.photogallery.common.generated.model.Comment;
import com.itechart.photogallery.common.generated.model.Image;
import com.itechart.photogallery.common.generated.model.Rank;
import com.itechart.photogallery.common.generated.model.UpdateImageHistoryRequest;
import com.itechart.photogallery.image.service.ImageService;
import com.itechart.photogallery.rank.service.RankService;

import io.swagger.annotations.Api;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import lombok.RequiredArgsConstructor;

@RestController
@Api(tags = "images")
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class ImagesApiImpl implements ImagesApi {

    private final CommentService commentService;
    private final ImageService imageService;
    private final RankService rankService;

    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Image> uploadImage(@RequestParam MultipartFile file) {
        var newImage = imageService.upload(file);
        return new ResponseEntity<>(newImage, CREATED);
    }

    public ResponseEntity<List<Image>> getImages(Integer page, Integer size, List<String> sortParams, String searchParam,
                                                 List<String> status, Boolean isDeleted) {
        var images = imageService.getPage(sortParams, searchParam, status, isDeleted, page, size);
        var headers = generatePaginationHeaders(images);
        return new ResponseEntity<>(images.getContent(), headers, OK);
    }

    public ResponseEntity<List<Image>> getAllImages(List<String> sortParams, String searchParam, List<String> status,
                                                    Boolean isDeleted) {
        var images = imageService.getAll(sortParams, searchParam, status, isDeleted);
        return new ResponseEntity<>(images, OK);
    }

    public ResponseEntity<List<Comment>> getImageComments(@PathVariable UUID id) {
        var comments = commentService.getByImageId(id);
        return new ResponseEntity<>(comments, OK);
    }

    public ResponseEntity<List<Rank>> getImageRanks(@PathVariable UUID id) {
        var comments = rankService.getByImageId(id);
        return new ResponseEntity<>(comments, OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Void> updateImageHistory(@PathVariable UUID id,
                                                   @RequestBody UpdateImageHistoryRequest imageHistory) {
        imageService.updateHistory(id, imageHistory);
        return new ResponseEntity<>(OK);
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Void> deleteImage(@PathVariable UUID id) {
        imageService.deleteById(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

}
