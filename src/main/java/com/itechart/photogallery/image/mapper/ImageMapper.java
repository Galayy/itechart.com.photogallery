package com.itechart.photogallery.image.mapper;

import com.itechart.photogallery.common.generated.model.Image;
import com.itechart.photogallery.image.entity.ImageEntity;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ImageMapper {

    ImageMapper IMAGE_MAPPER = Mappers.getMapper(ImageMapper.class);

    Image toModel(ImageEntity imageEntity);

}
