package com.itechart.photogallery.image.mapper;

import com.itechart.photogallery.common.generated.model.ImageHistoryStatus;
import com.itechart.photogallery.image.entity.enums.ImageHistoryEntityStatus;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ImageHistoryStatusMapper {

    ImageHistoryStatusMapper IMAGE_HISTORY_STATUS_MAPPER = Mappers.getMapper(ImageHistoryStatusMapper.class);

    ImageHistoryEntityStatus toEntity(ImageHistoryStatus status);

    ImageHistoryStatus toStatusModel(ImageHistoryEntityStatus status);

}
