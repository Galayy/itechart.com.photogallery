package com.itechart.photogallery.image.elastic;

import java.util.Date;
import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import lombok.Data;

@Data
@Document(indexName = "image")
public class ImageDocument {

    @Id
    private UUID id;

    private Date deletedAt;

    private String imageUrl;

}
