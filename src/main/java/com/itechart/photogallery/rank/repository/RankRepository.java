package com.itechart.photogallery.rank.repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.itechart.photogallery.rank.entity.RankEntity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RankRepository extends JpaRepository<RankEntity, UUID> {

    @Modifying
    @Query("UPDATE RankEntity rank " +
           "SET rank.deletedAt = :date " +
           "WHERE rank.id = :id")
    void deleteById(@Param("id") UUID id, @Param("date") Instant date);

    @Modifying
    @Query("UPDATE RankEntity rank " +
           "SET rank.deletedAt = :date " +
           "WHERE rank.imageId = :imageId")
    void deleteByImageId(@Param("imageId") UUID imageId, @Param("date") Instant date);

    @Modifying
    @Query("UPDATE RankEntity rank " +
           "SET rank.deletedAt = :date " +
           "WHERE rank.userId = :userId")
    void deleteByUserId(@Param("userId") UUID userId, @Param("date") Instant date);

    @Query("SELECT rank " +
           "FROM RankEntity rank " +
           "WHERE rank.id = :id" +
           " AND rank.deletedAt IS NULL")
    Optional<RankEntity> findById(@Param("id") UUID id);

    @Query("SELECT rank " +
           "FROM RankEntity rank " +
           "WHERE (rank.rank BETWEEN :minRank AND :maxRank)" +
           " AND (:isDeleted IS NULL OR :isDeleted = TRUE AND rank.deletedAt IS NOT NULL OR :isDeleted = FALSE " +
           " AND rank.deletedAt IS NULL)")
    Page<RankEntity> search(@Param("minRank") Integer minRank, @Param("maxRank") Integer maxRank,
                            @Param("isDeleted") Boolean isDeleted, Pageable pageable);

    @Query("SELECT rank " +
           "FROM RankEntity rank " +
           "WHERE (rank.rank BETWEEN :minRank AND :maxRank)" +
           " AND (:isDeleted IS NULL OR :isDeleted = TRUE AND rank.deletedAt IS NOT NULL OR :isDeleted = FALSE " +
           " AND rank.deletedAt IS NULL)")
    List<RankEntity> search(@Param("minRank") Integer minRank, @Param("maxRank") Integer maxRank,
                            @Param("isDeleted") Boolean isDeleted, Sort sort);

    @Query("SELECT rank " +
           "FROM RankEntity rank " +
           "WHERE rank.userId = :userId" +
           " AND rank.deletedAt IS NULL")
    List<RankEntity> findAllByUserId(@Param("userId") UUID userId);

    @Query("SELECT image " +
           "FROM RankEntity image " +
           "WHERE image.userId = :userId" +
           " AND image.createdAt >= :afterDate")
    List<RankEntity> findAllByUserIdAndAfterDate(@Param("userId") UUID userId, @Param("afterDate") Instant afterDate);

    @Query("SELECT rank " +
           "FROM RankEntity rank " +
           "WHERE rank.imageId = :imageId" +
           " AND rank.deletedAt IS NULL")
    List<RankEntity> findAllByImageId(@Param("imageId") UUID imageId);

}
