package com.itechart.photogallery.rank.service;

import java.util.List;
import java.util.UUID;

import com.itechart.photogallery.common.generated.model.CreateRankRequest;
import com.itechart.photogallery.common.generated.model.Rank;
import com.itechart.photogallery.common.generated.model.UpdateRankRequest;
import com.itechart.photogallery.common.service.CommonService;

import org.springframework.data.domain.Page;

public interface RankService extends CommonService<Rank> {

    Rank create(CreateRankRequest rank);

    Rank update(UUID id, UpdateRankRequest rankToUpdate);

    Page<Rank> getPage(List<String> sortParams, Integer maxRank, Integer minRank, Boolean isDeleted, Integer page,
                       Integer size);

    List<Rank> getAll(List<String> sortParams, Integer minRank, Integer maxRank, Boolean isDeleted);

    List<Rank> getByUserId(UUID id);

    List<Rank> getByImageId(UUID id);

}
