package com.itechart.photogallery.rank.service.impl;

import static com.itechart.photogallery.common.utils.ExceptionUtils.*;
import static com.itechart.photogallery.common.utils.FilterUtils.checkFilterParams;
import static com.itechart.photogallery.common.utils.FilterUtils.createDeletedFilterParamOrDefault;
import static com.itechart.photogallery.common.utils.PagingUtils.toPageable;
import static com.itechart.photogallery.common.utils.SecurityUtils.hasAdminAccessLevel;
import static com.itechart.photogallery.common.utils.SortingUtils.parseSortOrDefault;
import static com.itechart.photogallery.rank.mapper.RankMapper.RANK_MAPPER;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.itechart.photogallery.common.generated.model.CreateRankRequest;
import com.itechart.photogallery.common.generated.model.Rank;
import com.itechart.photogallery.common.generated.model.UpdateRankRequest;
import com.itechart.photogallery.image.service.ImageService;
import com.itechart.photogallery.rank.entity.RankEntity;
import com.itechart.photogallery.rank.repository.RankRepository;
import com.itechart.photogallery.rank.service.RankService;
import com.itechart.photogallery.user.service.UserService;

import lombok.extern.log4j.Log4j2;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Log4j2
@Service
@RequiredArgsConstructor
public class RankServiceImpl implements RankService {

    private final ImageService imageService;
    private final UserService userService;

    private final RankRepository rankRepository;

    @Override
    @Transactional
    public Rank create(CreateRankRequest rank) {
        imageService.getById(rank.getImageId());

        var rankToSave = new RankEntity();
        rankToSave.setRank(rank.getRank());
        rankToSave.setImageId(rank.getImageId());
        rankToSave.setUserId(userService.getCurrent().getId());

        var savedComment = rankRepository.save(rankToSave);
        log.info("IN create - rank: rank was successfully created");
        return RANK_MAPPER.toModel(savedComment);
    }

    @Override
    @Transactional
    public Rank update(UUID id, UpdateRankRequest rankToUpdate) {
        var currentUser = userService.getCurrent();
        var rankEntity = rankRepository.findById(id).orElseThrow(() ->
                entityNotFoundException(String.format("There is no rank with id %s", id)));
        isEnoughPermissions(currentUser, rankEntity.getUserId());

        rankEntity.setRank(rankToUpdate.getNewRank());
        var updatedRank = rankRepository.save(rankEntity);
        log.info("IN update - rank: rank was successfully updated");
        return RANK_MAPPER.toModel(updatedRank);
    }

    @Override
    @Transactional
    public void deleteById(UUID id) {
        var currentUser = userService.getCurrent();
        var rankEntity = rankRepository.findById(id).orElseThrow(() ->
                entityNotFoundException(String.format("There is no rank with id %s", id)));
        isEnoughPermissions(currentUser, rankEntity.getUserId());
        rankRepository.deleteById(id, Instant.now());
        log.info("IN delete - rank: rank was successfully deleted");

    }

    @Override
    @Transactional(readOnly = true)
    public Rank getById(UUID id) {
        var rankEntity = rankRepository.findById(id).orElseThrow(() ->
                entityNotFoundException(String.format("There is no rank with id %s", id)));
        log.info("IN getById - rank: rank was successfully found");
        return RANK_MAPPER.toModel(rankEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Rank> getPage(List<String> sortParams, Integer minRank, Integer maxRank, Boolean isDeleted,
                              Integer page, Integer size) {
        if (!hasAdminAccessLevel()) {
            checkFilterParams(isDeleted);
        }
        checkRanks(minRank, maxRank);

        var preparedDeletedFilterParam = createDeletedFilterParamOrDefault(isDeleted);
        var sort = parseSortOrDefault(sortParams, RankEntity.class);
        var pageable = toPageable(page, size, sort);

        var ranks = rankRepository.search(minRank, maxRank, preparedDeletedFilterParam, pageable)
                .map(RANK_MAPPER::toModel);
        log.info("IN getPage - rank: {} ranks was found", ranks.getContent().size());
        return ranks;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Rank> getAll(List<String> sortParams, Integer minRank, Integer maxRank, Boolean isDeleted) {
        if (!hasAdminAccessLevel()) {
            checkFilterParams(isDeleted);
        }
        checkRanks(minRank, maxRank);

        var preparedDeletedFilterParam = createDeletedFilterParamOrDefault(isDeleted);
        var sort = parseSortOrDefault(sortParams, RankEntity.class);

        var ranks = rankRepository.search(minRank, maxRank, preparedDeletedFilterParam, sort)
                .stream()
                .map(RANK_MAPPER::toModel)
                .collect(Collectors.toList());
        log.info("IN getAll - rank: {} ranks was found", ranks.size());
        return ranks;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Rank> getByUserId(UUID id) {
        userService.getById(id);
        var ranks = rankRepository.findAllByUserId(id)
                .stream()
                .map(RANK_MAPPER::toModel)
                .collect(Collectors.toList());
        log.info("IN getByUserId - rank: {} ranks were found", ranks.size());
        return ranks;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Rank> getByImageId(UUID id) {
        imageService.getById(id);
        var ranks = rankRepository.findAllByImageId(id)
                .stream()
                .map(RANK_MAPPER::toModel)
                .collect(Collectors.toList());
        log.info("IN getByImageId - rank: {} ranks were found", ranks.size());
        return ranks;
    }

    private void checkRanks(Integer minRank, Integer maxRank) {
        if (minRank > maxRank) {
            throw invalidInputDataException("Maximum rank should be greater than minimum");
        }
    }

}
