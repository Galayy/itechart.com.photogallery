package com.itechart.photogallery.rank.mapper;

import com.itechart.photogallery.common.generated.model.Rank;
import com.itechart.photogallery.rank.entity.RankEntity;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface RankMapper {

    RankMapper RANK_MAPPER = Mappers.getMapper(RankMapper.class);

    Rank toModel(RankEntity rankEntity);

}
