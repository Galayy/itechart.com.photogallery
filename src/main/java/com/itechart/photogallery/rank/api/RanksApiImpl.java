package com.itechart.photogallery.rank.api;

import static com.itechart.photogallery.common.utils.HeaderUtils.generatePaginationHeaders;

import static org.springframework.http.HttpStatus.*;

import java.util.List;
import java.util.UUID;
import javax.validation.Valid;

import com.itechart.photogallery.common.generated.api.RanksApi;
import com.itechart.photogallery.common.generated.model.CreateRankRequest;
import com.itechart.photogallery.common.generated.model.Rank;
import com.itechart.photogallery.common.generated.model.UpdateRankRequest;
import com.itechart.photogallery.rank.service.RankService;

import io.swagger.annotations.Api;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@Api(tags = "ranks")
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class RanksApiImpl implements RanksApi {

    private final RankService rankService;

    public ResponseEntity<Rank> getRank(@PathVariable UUID id) {
        var comment = rankService.getById(id);
        return new ResponseEntity<>(comment, OK);
    }

    public ResponseEntity<List<Rank>> getRanks(Integer page, Integer size, Integer minRank, Integer maxRank,
                                               List<String> sortParams, Boolean isDeleted) {
        var comments = rankService.getPage(sortParams, minRank, maxRank, isDeleted, page, size);
        var headers = generatePaginationHeaders(comments);
        return new ResponseEntity<>(comments.getContent(), headers, OK);
    }

    public ResponseEntity<List<Rank>> getAllRanks(Integer minRank, Integer maxRank, List<String> sortParams,
                                                  Boolean isDeleted) {
        var users = rankService.getAll(sortParams, minRank, maxRank, isDeleted);
        return new ResponseEntity<>(users, OK);
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Rank> createRank(@Valid @RequestBody CreateRankRequest rank) {
        var createdRank = rankService.create(rank);
        return new ResponseEntity<>(createdRank, CREATED);
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Rank> updateRank(@PathVariable UUID id, @Valid @RequestBody UpdateRankRequest rankToUpdate) {
        var updatedComment = rankService.update(id, rankToUpdate);
        return new ResponseEntity<>(updatedComment, OK);
    }

    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<Void> deleteRank(@PathVariable UUID id) {
        rankService.deleteById(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

}
