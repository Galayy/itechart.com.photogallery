package com.itechart.photogallery.rank.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.itechart.photogallery.rank.validation.annotations.Rank;

public class RankValidator implements ConstraintValidator<Rank, Integer> {

    private static final int MAX = 10;
    private static final int MIN = 1;

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        return value >= MIN && value <= MAX;
    }

}
