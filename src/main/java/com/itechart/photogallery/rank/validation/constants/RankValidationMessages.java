package com.itechart.photogallery.rank.validation.constants;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public final class RankValidationMessages {

    public static final String INVALID_RANK_MESSAGE = "Rank must be from 1 to 10.";

}
