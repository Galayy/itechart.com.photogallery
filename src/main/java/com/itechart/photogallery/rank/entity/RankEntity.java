package com.itechart.photogallery.rank.entity;


import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.itechart.photogallery.common.entity.AbstractEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "rank")
@EqualsAndHashCode(callSuper = true)
public class RankEntity extends AbstractEntity {

    @Column(name = "image_id", nullable = false, updatable = false)
    private UUID imageId;

    @Column(name = "rank", nullable = false)
    private Integer rank;

    @Column(name = "user_id", nullable = false, updatable = false)
    private UUID userId;

}
