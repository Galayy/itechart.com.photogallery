package com.itechart.photogallery.user.service.impl;

import com.itechart.photogallery.auth.service.MailService;
import com.itechart.photogallery.comment.repository.CommentRepository;
import com.itechart.photogallery.common.generated.model.ChangePasswordRequest;
import com.itechart.photogallery.common.generated.model.SignUpRequest;
import com.itechart.photogallery.common.generated.model.UpdateUserRequest;
import com.itechart.photogallery.common.generated.model.User;
import com.itechart.photogallery.image.repository.ImageHistoryRepository;
import com.itechart.photogallery.image.repository.ImageRepository;
import com.itechart.photogallery.rank.repository.RankRepository;
import com.itechart.photogallery.user.entity.UserEntity;
import com.itechart.photogallery.user.filter.UserRoleFilter;
import com.itechart.photogallery.user.model.UserDetailsImpl;
import com.itechart.photogallery.user.repository.UserRepository;
import com.itechart.photogallery.user.service.UserSearchService;
import com.itechart.photogallery.user.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static com.itechart.photogallery.common.utils.ExceptionUtils.*;
import static com.itechart.photogallery.common.utils.FilterUtils.*;
import static com.itechart.photogallery.common.utils.MailUtils.createContext;
import static com.itechart.photogallery.common.utils.PagingUtils.toPageable;
import static com.itechart.photogallery.common.utils.SecurityUtils.getCurrentUserOpt;
import static com.itechart.photogallery.common.utils.SecurityUtils.hasAdminAccessLevel;
import static com.itechart.photogallery.common.utils.SortingUtils.parseSortOrDefault;
import static com.itechart.photogallery.user.mapper.UserMapper.USER_MAPPER;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.Collections.emptyList;

@Log4j2
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private static final String ACTIVITY_REPORT_TEMPLATE = "activity-report-email-template";
    private static final String CONFIRM_REGISTRATION_TEMPLATE = "confirm-registration-email-template";
    private static final String NAME = "user";
    private static final String TOKEN_KEY = "token";

    private final MailService mailService;
    private final UserSearchService userSearchService;

    private final CommentRepository commentRepository;
    private final ImageHistoryRepository imageHistoryRepository;
    private final ImageRepository imageRepository;
    private final RankRepository rankRepository;
    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;
    private final UserRoleFilter userRoleFilter;

    @Override
    @Transactional
    public void create(SignUpRequest signUpRequest) {
        var encodedPassword = passwordEncoder.encode(signUpRequest.getPassword());
        var confirmRegistrationToken = RandomStringUtils.random(10, true, true);

        var entity = new UserEntity();
        entity.setAddress(signUpRequest.getAddress());
        entity.setPassword(encodedPassword);
        entity.setLogin(signUpRequest.getEmail());
        entity.setFirstName(signUpRequest.getFirstName());
        entity.setNickname(signUpRequest.getNickname());
        entity.setLastName(signUpRequest.getLastName());
        entity.setRole(USER_MAPPER.toEntity(signUpRequest.getRole()));
        entity.setConfirmRegistrationDate(Instant.now());
        entity.setConfirmRegistrationToken(confirmRegistrationToken);
        entity.setEnabled(false);
        var savedUser = userRepository.save(entity);

        log.info("IN create - user: user was added to database");
        var context = createContext(TOKEN_KEY, confirmRegistrationToken);
        mailService.sendEmail(context, entity.getLogin(), CONFIRM_REGISTRATION_TEMPLATE);
        userSearchService.create(savedUser);
    }

    @Override
    @Transactional
    public User update(UUID id, UpdateUserRequest userToUpdate) {
        var entityToUpdate = userRepository.findById(id).orElseThrow(() -> entityNotFoundException(NAME, id));
        var currentUser = getCurrent();
        isEnoughPermissions(currentUser, id);

        entityToUpdate.setAddress(userToUpdate.getNewAddress());
        entityToUpdate.setFirstName(userToUpdate.getNewFirstName());
        entityToUpdate.setLastName(userToUpdate.getNewLastName());
        entityToUpdate.setNickname(userToUpdate.getNewNickname());

        var updatedEntity = userRepository.save(entityToUpdate);
        userSearchService.update(id, updatedEntity);
        log.info("IN update - user: user was successfully updated");
        return USER_MAPPER.toModel(updatedEntity);
    }

    @Override
    @Transactional
    public void updatePassword(ChangePasswordRequest changePasswordRequest) {
        var currentUser = getCurrent();
        isPasswordCorrect(changePasswordRequest.getOldPassword(), currentUser.getPassword());

        var encodedPassword = passwordEncoder.encode(changePasswordRequest.getNewPassword());
        currentUser.setPassword(encodedPassword);
        var userEntity = USER_MAPPER.toEntity(currentUser);
        userRepository.save(userEntity);
        log.info("IN updatePassword - user: password was successfully updated");
    }

    @Override
    @Transactional(readOnly = true)
    public void getActivityReport(UUID id) {
        var currentUser = getCurrent();
        var user = userRepository.findById(id).orElseThrow(() -> entityNotFoundException(NAME, id));
        isEnoughPermissions(currentUser, id);
        var afterDate = Instant.now().minus(30, DAYS);

        var variables = new HashMap<String, Object>();
        variables.put("date", afterDate);
        variables.put("login", user.getLogin());
        variables.putAll(prepareComments(id, afterDate));
        variables.putAll(prepareImages(id, afterDate));
        variables.putAll(prepareRanks(id, afterDate));

        var context = createContext(variables);
        log.info("IN getActivityReport - user: report was successfully made");
        mailService.sendEmail(context, user.getLogin(), ACTIVITY_REPORT_TEMPLATE);
    }

    @Override
    @Transactional(readOnly = true)
    public User getById(UUID id) {
        var user = USER_MAPPER.toModel(userRepository.findById(id)
                .orElseThrow(() -> entityNotFoundException(String.format("User with id %s doesn't exist", id))));
        log.info("IN getById - user: user was successfully found");
        return user;
    }

    @Override
    @Transactional(readOnly = true)
    public User getCurrent() {
        var userDetails = getCurrentUserOpt().orElseThrow(() -> entityNotFoundException("There is no user logged in."));
        var user = USER_MAPPER.toModel(userRepository.findById(userDetails.getId()).orElseThrow(() ->
                entityNotFoundException("User not found.")));
        log.info("IN getCurrent - user: user was successfully found");
        return user;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<User> getPage(List<String> sortParams, String searchParam, List<String> roles, Boolean isDeleted,
                              Boolean isEnabled, Integer page, Integer size) {
        if (!hasAdminAccessLevel()) {
            checkFilterParams(isDeleted, isEnabled, roles);
        }

        var searchPageable = toPageable(page, size);
        var preparedDeletedFilterParam = createDeletedFilterParamOrDefault(isDeleted);
        var preparedEnabledFilterParam = createEnabledFilterParamOrDefault(isEnabled);
        var preparedRoles = userRoleFilter.createFilterParam(roles);
        var preparedUserIds = userSearchService.getIds(searchParam, preparedDeletedFilterParam,
                preparedEnabledFilterParam, searchPageable);

        if (preparedUserIds.isEmpty()) {
            log.info("IN getPage - user: 0 users were found");
            return Page.empty();
        }
        var sort = parseSortOrDefault(sortParams, UserEntity.class);
        var pageable = toPageable(page, size, sort);
        var users = userRepository.search(preparedUserIds, preparedRoles, pageable).map(USER_MAPPER::toModel);
        log.info("IN getPage - user: {} users were found", users.getContent().size());
        return users;
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> getAll(List<String> sortParams, String searchParam, List<String> roles, Boolean isDeleted,
                             Boolean isEnabled) {
        if (!hasAdminAccessLevel()) {
            checkFilterParams(isDeleted, isEnabled, roles);
        }
        var size = userRepository.findAll().size();
        var searchPageable = toPageable(1, size);

        var preparedDeletedFilterParam = createDeletedFilterParamOrDefault(isDeleted);
        var preparedEnabledFilterParam = createEnabledFilterParamOrDefault(isEnabled);
        var preparedRoles = userRoleFilter.createFilterParam(roles);
        var preparedUserIds = userSearchService.getIds(searchParam, preparedDeletedFilterParam,
                preparedEnabledFilterParam, searchPageable);

        if (preparedUserIds.isEmpty()) {
            log.info("IN getAll - user: 0 users were found");
            return emptyList();
        }
        var sort = parseSortOrDefault(sortParams, UserEntity.class);
        var users = userRepository.search(preparedUserIds, preparedRoles, sort)
                .stream()
                .map(USER_MAPPER::toModel)
                .collect(Collectors.toList());
        log.info("IN getAll - user: {} users were found", users.size());
        return users;
    }

    @Override
    @Transactional
    public void deleteById(UUID id) {
        var entity = userRepository.findById(id).orElseThrow(() -> entityNotFoundException(NAME, id));
        var currentUser = getCurrent();
        isEnoughPermissions(currentUser, entity);

        commentRepository.deleteByUserId(entity.getId(), Instant.now());
        rankRepository.deleteByUserId(entity.getId(), Instant.now());

        imageRepository.findAllByUserId(id).forEach(image ->
                imageHistoryRepository.deleteByImageId(image.getId(), Instant.now()));
        imageRepository.deleteByUserId(id, Instant.now());

        userRepository.deleteById(id, Instant.now());
        userSearchService.delete(id);
        log.info("IN deleteById - user: user was successfully deleted");
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetailsImpl loadUserByUsername(String id) {
        var entity = userRepository.findById(UUID.fromString(id)).orElseThrow(() ->
                usernameNotFoundException(String.format("User with id %s not found", id)));
        isUserDeleted(entity.getDeletedAt());

        var userDetails = new UserDetailsImpl();
        userDetails.setId(entity.getId());
        userDetails.setPassword(entity.getPassword());
        userDetails.setRole(entity.getRole());
        return userDetails;
    }

    private Map<String, Object> prepareComments(UUID id, Instant afterDate) {
        var userComments = commentRepository.findAllByUserIdAndAfterDate(id, afterDate);
        var createdComments = filterEntities(userComments, false);
        var deletedComments = filterEntities(userComments, true);

        return Map.ofEntries(new AbstractMap.SimpleEntry<>("comments", createdComments),
                new AbstractMap.SimpleEntry<>("deletedComments", deletedComments));
    }

    private Map<String, Object> prepareImages(UUID id, Instant afterDate) {
        var userImages = imageRepository.findAllByUserIdAndAfterDate(id, afterDate);
        var createdImages = filterEntities(userImages, false);
        var deletedImages = filterEntities(userImages, true);

        return Map.ofEntries(new AbstractMap.SimpleEntry<>("images", createdImages),
                new AbstractMap.SimpleEntry<>("deletedImages", deletedImages));
    }

    private Map<String, Object> prepareRanks(UUID id, Instant afterDate) {
        var userRanks = rankRepository.findAllByUserIdAndAfterDate(id, afterDate);
        var createdRanks = filterEntities(userRanks, false);
        var deletedRanks = filterEntities(userRanks, true);

        return Map.ofEntries(new AbstractMap.SimpleEntry<>("ranks", createdRanks),
                new AbstractMap.SimpleEntry<>("deletedRanks", deletedRanks));
    }

    private void isPasswordCorrect(String requestedPassword, String existedPassword) {
        if (!passwordEncoder.matches(requestedPassword, existedPassword)) {
            throw invalidInputDataException("Old password is incorrect.");
        }
    }

    private void isUserDeleted(Instant deletedAt) {
        if (deletedAt != null) {
            throw forbiddenAccessException(String.format("User was deleted at %s", deletedAt));
        }
    }

}
