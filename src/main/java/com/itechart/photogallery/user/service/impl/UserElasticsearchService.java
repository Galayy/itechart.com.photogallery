package com.itechart.photogallery.user.service.impl;

import com.itechart.photogallery.user.elastic.UserDocument;
import com.itechart.photogallery.user.entity.UserEntity;
import com.itechart.photogallery.user.repository.UserElasticRepository;
import com.itechart.photogallery.user.service.UserSearchService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.itechart.photogallery.common.utils.ExceptionUtils.entityNotFoundException;

@Log4j2
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(name = "spring.elasticsearch.enabled", havingValue = "true")
public class UserElasticsearchService implements UserSearchService {

    private static final String NAME = "user";

    private final UserElasticRepository userElasticRepository;

    @Override
    public void create(UserEntity entity) {
        var document = new UserDocument();
        document.setId(entity.getId());
        document.setAddress(entity.getAddress());
        document.setFirstName(entity.getFirstName());
        document.setNickname(entity.getNickname());
        document.setLastName(entity.getLastName());
        document.setEnabled(entity.getEnabled());
        document.setRole(entity.getRole().getAuthority());
        userElasticRepository.save(document);
        log.info("IN create - user elasticsearch: success");
    }

    @Override
    public void confirm(UserEntity entity) {
        var document = userElasticRepository.findById(entity.getId()).orElseThrow(() ->
                entityNotFoundException(NAME, entity.getId()));
        document.setEnabled(entity.getEnabled());
        userElasticRepository.save(document);
        log.info("IN confirm - user elasticsearch: success");
    }

    @Override
    public void delete(UUID id) {
        var document = userElasticRepository.findById(id).orElseThrow(() -> entityNotFoundException(NAME, id));
        document.setDeletedAt(Date.from(Instant.now()));
        userElasticRepository.save(document);
        log.info("IN delete - user elasticsearch: success");
    }

    @Override
    public void update(UUID id, UserEntity entity) {
        var documentToUpdate = userElasticRepository.findById(id).orElseThrow(() ->
                entityNotFoundException(NAME, id));
        documentToUpdate.setAddress(entity.getAddress());
        documentToUpdate.setFirstName(entity.getFirstName());
        documentToUpdate.setLastName(entity.getLastName());
        documentToUpdate.setNickname(entity.getNickname());
        userElasticRepository.save(documentToUpdate);
        log.info("IN update - user elasticsearch: success");
    }

    @Override
    public List<UUID> getIds(String searchParam, Boolean isDeleted, Boolean isEnabled, Pageable pageable) {
        final Page<UserDocument> documentsAccordingToDeleted;
        final Page<UserDocument> documentsAccordingToEnabled;

        if (searchParam == null) {
            documentsAccordingToDeleted = findWithDeletedFilterParam(isDeleted, pageable);
            documentsAccordingToEnabled = findWithEnabledFilterParam(isEnabled, pageable);
        } else {
            documentsAccordingToDeleted = findWithDeletedFilterParam(searchParam, isDeleted, pageable);
            documentsAccordingToEnabled = findWithEnabledFilterParam(searchParam, isEnabled, pageable);
        }

        var documentsAccordingToDeletedIds = documentsAccordingToDeleted.stream().map(UserDocument::getId)
                .collect(Collectors.toList());
        var documentsAccordingToEnabledIds = documentsAccordingToEnabled.stream().map(UserDocument::getId)
                .collect(Collectors.toList());
        var documentsIds = documentsAccordingToDeletedIds.stream().distinct()
                .filter(documentsAccordingToEnabledIds::contains).collect(Collectors.toList());

        log.info("IN getIds - user elasticsearch: {} users were found", documentsIds.size());
        return documentsIds;
    }

    private Page<UserDocument> findWithDeletedFilterParam(String query, Boolean isDeleted,
                                                          Pageable pageable) {
        if (isDeleted == null) {
            return userElasticRepository.searchAll(query, pageable);
        } else if (isDeleted) {
            return userElasticRepository.searchDeleted(query, pageable);
        } else {
            return userElasticRepository.searchExisted(query, pageable);
        }
    }

    private Page<UserDocument> findWithEnabledFilterParam(String query, Boolean isEnabled,
                                                          Pageable pageable) {
        if (isEnabled == null) {
            return userElasticRepository.searchAll(query, pageable);
        } else {
            return userElasticRepository.searchAllByEnabled(query, isEnabled, pageable);
        }
    }

    private Page<UserDocument> findWithDeletedFilterParam(Boolean isDeleted, Pageable pageable) {
        if (isDeleted == null) {
            return userElasticRepository.findAll(pageable);
        } else if (isDeleted) {
            return userElasticRepository.findDeleted(pageable);
        } else {
            return userElasticRepository.findExisted(pageable);
        }
    }

    private Page<UserDocument> findWithEnabledFilterParam(Boolean isEnabled, Pageable pageable) {
        if (isEnabled == null) {
            return userElasticRepository.findAll(pageable);
        } else {
            return userElasticRepository.findAllByEnabled(isEnabled, pageable);
        }
    }

}
