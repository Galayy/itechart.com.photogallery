package com.itechart.photogallery.user.service.impl;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.itechart.photogallery.common.entity.AbstractEntity;
import com.itechart.photogallery.user.entity.UserEntity;
import com.itechart.photogallery.user.repository.UserRepository;
import com.itechart.photogallery.user.service.UserSearchService;

import lombok.extern.log4j.Log4j2;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Log4j2
@Service
@RequiredArgsConstructor
@ConditionalOnMissingBean(UserElasticsearchService.class)
public class UserFulltextSearchService implements UserSearchService {

    private final UserRepository userRepository;

    @Override
    public void confirm(UserEntity entity) {}

    @Override
    public void update(UUID id, UserEntity entity) {}

    @Override
    public List<UUID> getIds(String searchParam, Boolean isDeleted, Boolean isEnabled, Pageable pageable) {
        var users = userRepository.search(searchParam, isDeleted, isEnabled, pageable).getContent();
        var userIds =  users.stream().map(AbstractEntity::getId).collect(Collectors.toList());
        log.info("IN getIds - user fulltext search: {} ids were found", userIds.size());
        return userIds;
    }

    @Override
    public void create(UserEntity entity) {}

    @Override
    public void delete(UUID id) {}

}
