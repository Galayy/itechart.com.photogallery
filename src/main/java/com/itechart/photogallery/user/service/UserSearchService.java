package com.itechart.photogallery.user.service;

import java.util.List;
import java.util.UUID;

import com.itechart.photogallery.common.service.CommonSearchService;
import com.itechart.photogallery.user.entity.UserEntity;

import org.springframework.data.domain.Pageable;

public interface UserSearchService extends CommonSearchService<UserEntity> {

    void confirm(UserEntity entity);

    void update(UUID id, UserEntity entity);

    List<UUID> getIds(String searchParam, Boolean isDeleted, Boolean isEnabled, Pageable pageable);

}
