package com.itechart.photogallery.user.service;

import java.util.List;
import java.util.UUID;

import com.itechart.photogallery.common.generated.model.ChangePasswordRequest;
import com.itechart.photogallery.common.generated.model.SignUpRequest;
import com.itechart.photogallery.common.generated.model.UpdateUserRequest;
import com.itechart.photogallery.common.generated.model.User;
import com.itechart.photogallery.common.service.CommonService;

import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService, CommonService<User> {

    User update(UUID id, UpdateUserRequest userToUpdate);

    User getCurrent();

    Page<User> getPage(List<String> sortParams, String searchParam, List<String> roles, Boolean isDeleted,
                       Boolean isEnabled, Integer page, Integer size);

    List<User> getAll(List<String> sortParams, String searchParam, List<String> roles, Boolean isDeleted,
                      Boolean isEnabled);

    void create(SignUpRequest signUpRequest);

    void updatePassword(ChangePasswordRequest changePasswordRequest);

    void getActivityReport(UUID id);

}
