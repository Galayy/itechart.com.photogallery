package com.itechart.photogallery.user.mapper;

import com.itechart.photogallery.common.generated.model.User;
import com.itechart.photogallery.common.generated.model.UserRole;
import com.itechart.photogallery.user.entity.UserEntity;
import com.itechart.photogallery.user.entity.enums.UserEntityRole;

import org.mapstruct.Mapper;
import org.mapstruct.ValueMapping;
import org.mapstruct.ValueMappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper USER_MAPPER = Mappers.getMapper(UserMapper.class);

    UserEntity toEntity(User user);

    User toModel(UserEntity userEntity);

    @ValueMappings({
            @ValueMapping(source = "ADMIN", target = "ROLE_ADMIN"),
            @ValueMapping(source = "USER", target = "ROLE_USER")
    })
    UserEntityRole toEntity(UserRole role);

    @ValueMappings({
            @ValueMapping(source = "ROLE_ADMIN", target = "ADMIN"),
            @ValueMapping(source = "ROLE_USER", target = "USER")
    })
    UserRole toModel(UserEntityRole role);

}
