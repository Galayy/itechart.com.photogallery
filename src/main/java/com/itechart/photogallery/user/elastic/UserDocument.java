package com.itechart.photogallery.user.elastic;

import java.util.Date;
import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import lombok.Data;

@Data
@Document(indexName = "user")
public class UserDocument {

    @Id
    private UUID id;

    private String address;
    private Date deletedAt;
    private Boolean enabled;
    private String firstName;
    private String lastName;
    private String nickname;
    private String role;

}
