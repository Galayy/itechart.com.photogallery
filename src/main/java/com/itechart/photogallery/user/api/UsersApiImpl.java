package com.itechart.photogallery.user.api;

import static com.itechart.photogallery.common.utils.HeaderUtils.generatePaginationHeaders;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

import java.util.List;
import java.util.UUID;
import javax.validation.Valid;

import com.itechart.photogallery.comment.service.CommentService;
import com.itechart.photogallery.common.generated.api.UsersApi;
import com.itechart.photogallery.common.generated.model.*;
import com.itechart.photogallery.image.service.ImageService;
import com.itechart.photogallery.rank.service.RankService;
import com.itechart.photogallery.user.service.UserService;

import io.swagger.annotations.Api;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@Api(tags = "users")
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class UsersApiImpl implements UsersApi {

    private final CommentService commentService;
    private final ImageService imageService;
    private final RankService rankService;
    private final UserService userService;

    public ResponseEntity<List<User>> getUsers(Integer page, Integer size, List<String> sortParams, String searchParam,
                                               List<String> roles, Boolean isDeleted, Boolean isEnabled) {
        var users = userService.getPage(sortParams, searchParam, roles, isDeleted, isEnabled, page, size);
        var headers = generatePaginationHeaders(users);
        return new ResponseEntity<>(users.getContent(), headers, OK);
    }

    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<User> getCurrentUser() {
        var user = userService.getCurrent();
        return new ResponseEntity<>(user, OK);
    }

    public ResponseEntity<List<User>> getAllUsers(List<String> sortParams, String searchParam, List<String> roles,
                                                  Boolean isDeleted, Boolean isEnabled) {
        var users = userService.getAll(sortParams, searchParam, roles, isDeleted, isEnabled);
        return new ResponseEntity<>(users, OK);
    }

    public ResponseEntity<User> getUser(@PathVariable UUID id) {
        var user = userService.getById(id);
        return new ResponseEntity<>(user, OK);
    }

    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<Void> getActivityReport(@PathVariable UUID id) {
        userService.getActivityReport(id);
        return new ResponseEntity<>(OK);
    }

    public ResponseEntity<List<Image>> getUserImages(@PathVariable UUID id) {
        var images = imageService.getByUserId(id);
        return new ResponseEntity<>(images, OK);
    }

    public ResponseEntity<List<Rank>> getUserRanks(@PathVariable UUID id) {
        var comments = rankService.getByUserId(id);
        return new ResponseEntity<>(comments, OK);
    }

    public ResponseEntity<List<Comment>> getUserComments(@PathVariable UUID id) {
        var comments = commentService.getByUserId(id);
        return new ResponseEntity<>(comments, OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public ResponseEntity<User> updateUserProfile(@PathVariable UUID id, @Valid @RequestBody UpdateUserRequest newUser) {
        var updatedUser = userService.update(id, newUser);
        return new ResponseEntity<>(updatedUser, OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public ResponseEntity<Void> deleteUser(@PathVariable UUID id) {
        userService.deleteById(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Void> changePassword(@Valid @RequestBody ChangePasswordRequest changePasswordRequest) {
        userService.updatePassword(changePasswordRequest);
        return new ResponseEntity<>(OK);
    }

}
