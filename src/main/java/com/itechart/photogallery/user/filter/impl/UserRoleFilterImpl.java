package com.itechart.photogallery.user.filter.impl;

import static com.itechart.photogallery.common.utils.ExceptionUtils.invalidInputDataException;
import static com.itechart.photogallery.common.utils.SecurityUtils.hasAdminAccessLevel;
import static com.itechart.photogallery.user.entity.enums.UserEntityRole.ROLE_USER;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.itechart.photogallery.user.entity.enums.UserEntityRole;
import com.itechart.photogallery.user.filter.UserRoleFilter;

import org.springframework.stereotype.Component;

@Component
public class UserRoleFilterImpl implements UserRoleFilter {

    @Override
    public List<UserEntityRole> createFilterParam(List<String> filterParams) {
        if (hasAdminAccessLevel()) {
            return prepareFilterParams(filterParams);
        }
        return Collections.singletonList(ROLE_USER);
    }

    @Override
    public List<UserEntityRole> prepareFilterParams(List<String> filterParams) {
        if (filterParams == null) {
            return null;
        }
        return filterParams.stream().map(this::validateFilterParam).collect(Collectors.toList());
    }

    @Override
    public UserEntityRole validateFilterParam(String filterParam) {
        return Optional.ofNullable(filterParam)
                .map(UserEntityRole::fromAuthority)
                .orElseThrow(() -> invalidInputDataException("Invalid role name"));
    }

}
