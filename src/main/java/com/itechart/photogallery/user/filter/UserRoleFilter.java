package com.itechart.photogallery.user.filter;

import com.itechart.photogallery.common.filter.Filter;
import com.itechart.photogallery.user.entity.enums.UserEntityRole;

public interface UserRoleFilter extends Filter<UserEntityRole> {

}
