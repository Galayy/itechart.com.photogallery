package com.itechart.photogallery.user.repository;

import java.util.UUID;

import com.itechart.photogallery.user.elastic.UserDocument;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
@ConditionalOnProperty(name = "spring.elasticsearch.enabled", havingValue = "true")
public interface UserElasticRepository extends ElasticsearchRepository<UserDocument, UUID> {

    Page<UserDocument> findAllByEnabled(Boolean isEnabled, Pageable pageable);

    @Query("{\"bool\": {\"must_not\": {\"exists\": {\"field\": \"deletedAt\"}}}}")
    Page<UserDocument> findExisted(Pageable pageable);

    @Query("{\"exists\": {\"field\": \"deletedAt\"}}")
    Page<UserDocument> findDeleted(Pageable pageable);

    @Query("{\"bool\": {\"must\":" +
               "{\"multi_match\": {" +
                   "\"query\": \"?0\", " +
                   "\"fields\": [ \"address\", \"firstName\", \"lastName\", \"nickname\"]" +
               "}}" +
            "}}")
    Page<UserDocument> searchAll(String searchParam, Pageable pageable);

    @Query("{\"bool\": {\"must\":[" +
               "{\"multi_match\": {" +
                   "\"query\": \"?0\", " +
                   "\"fields\": [ \"address\", \"firstName\", \"lastName\", \"nickname\"]" +
               "}}," +
               "{\"match\": {\"enabled\": \"?1\"}}" +
            "]}}")
    Page<UserDocument> searchAllByEnabled(String searchParam, Boolean isEnabled, Pageable pageable);

    @Query("{\"bool\": {\"must\":[" +
               "{\"multi_match\": {" +
                   "\"query\": \"?0\", " +
                   "\"fields\": [ \"address\", \"firstName\", \"lastName\", \"nickname\"]" +
               "}}," +
               "{\"bool\":{ \"must_not\": {\"exists\": {\"field\": \"deletedAt\"}}}}" +
            "]}}")
    Page<UserDocument> searchExisted(String searchParam, Pageable pageable);

    @Query("{\"bool\": {\"must\":[" +
               "{\"multi_match\": {" +
                   "\"query\": \"?0\", " +
                   "\"fields\": [ \"address\", \"firstName\", \"lastName\", \"nickname\"]" +
               "}}," +
               "{\"exists\" : {\"field\": \"deletedAt\"}}" +
            "]}}")
    Page<UserDocument> searchDeleted(String searchParam, Pageable pageable);

}
