package com.itechart.photogallery.user.repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.itechart.photogallery.user.entity.UserEntity;
import com.itechart.photogallery.user.entity.enums.UserEntityRole;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    boolean existsByLogin(String login);

    @Modifying
    @Query("UPDATE UserEntity user " +
           "SET user.deletedAt = :date " +
           "WHERE user.id = :id AND user.enabled = TRUE")
    void deleteById(@Param("id") UUID userId, @Param("date") Instant date);

    @Query("SELECT user " +
           "FROM UserEntity user " +
           "WHERE user.id = :id AND user.deletedAt IS NULL AND user.enabled = TRUE")
    Optional<UserEntity> findById(@Param("id") UUID id);

    @Query("SELECT user " +
           "FROM UserEntity user " +
           "WHERE user.login = :login AND user.deletedAt IS NULL")
    Optional<UserEntity> findByLogin(@Param("login") String login);

    @Query("SELECT user " +
           "FROM UserEntity user " +
           "WHERE user.login = :login AND user.deletedAt IS NULL AND user.enabled = TRUE")
    Optional<UserEntity> findEnabledByLogin(@Param("login") String login);

    @Query("SELECT user " +
           "FROM UserEntity user " +
           "WHERE user.resetPasswordToken = :resetPasswordToken" +
           " AND user.deletedAt IS NULL AND user.enabled = TRUE")
    Optional<UserEntity> findByResetPasswordToken(@Param("resetPasswordToken") String id);

    @Query("SELECT user " +
           "FROM UserEntity user " +
           "WHERE user.confirmRegistrationToken = :confirmRegistrationToken AND user.deletedAt IS NULL")
    Optional<UserEntity> findByConfirmRegistrationToken(
            @Param("confirmRegistrationToken") String confirmRegistrationToken);

    @Query("SELECT user " +
           "FROM UserEntity AS user " +
           "WHERE user.id IN :userIds" +
           " AND (COALESCE(:roles) IS NULL OR user.role IN :roles)")
    List<UserEntity> search(@Param("userIds") List<UUID> userIds, @Param("roles") List<UserEntityRole> roles, Sort sort);

    @Query("SELECT user " +
           "FROM UserEntity AS user " +
           "WHERE (:searchParam IS NULL OR user.address LIKE %:searchParam% OR user.firstName LIKE %:searchParam%  " +
           "    OR user.lastName LIKE %:searchParam% OR user.nickname LIKE %:searchParam%)" +
           " AND (:isDeleted IS NULL OR :isDeleted = true AND user.deletedAt IS NOT NULL " +
           "     OR :isDeleted = false AND user.deletedAt IS NULL)" +
           " AND (:isEnabled IS NULL OR user.enabled = :isEnabled)")
    Page<UserEntity> search(@Param("searchParam") String searchParam, @Param("isDeleted") Boolean isDeleted,
                            @Param("isEnabled") Boolean isEnabled, Pageable pageable);

    @Query("SELECT user " +
           "FROM UserEntity AS user " +
           "WHERE user.id IN :userIds" +
           " AND (COALESCE(:roles) IS NULL OR user.role IN :roles)")
    Page<UserEntity> search(@Param("userIds") List<UUID> userIds, @Param("roles") List<UserEntityRole> roles,
                            Pageable pageable);

}
