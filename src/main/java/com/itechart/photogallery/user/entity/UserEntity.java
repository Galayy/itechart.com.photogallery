package com.itechart.photogallery.user.entity;

import java.time.Instant;
import javax.persistence.*;

import com.itechart.photogallery.common.entity.AbstractEntity;
import com.itechart.photogallery.user.entity.enums.UserEntityRole;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "\"user\"")
@EqualsAndHashCode(callSuper = true)
public class UserEntity extends AbstractEntity {

    @Column(name = "address")
    private String address;

    @Column(name = "confirm_registration_date")
    private Instant confirmRegistrationDate;

    @Column(name = "confirm_registration_token", nullable = false, unique = true)
    private String confirmRegistrationToken;

    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "login", nullable = false, updatable = false, unique = true)
    private String login;

    @Column(name = "nickname")
    private String nickname;

    @Column(name = "password", nullable = false, unique = true)
    private String password;

    @Column(name = "reset_password_date")
    private Instant resetPasswordDate;

    @Column(name = "reset_password_token", unique = true)
    private String resetPasswordToken;

    @Enumerated(EnumType.STRING)
    @Column(name = "role", nullable = false, updatable = false)
    private UserEntityRole role;

}
