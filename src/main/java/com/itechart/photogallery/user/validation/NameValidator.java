package com.itechart.photogallery.user.validation;

import static java.util.regex.Pattern.compile;

import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.itechart.photogallery.user.validation.annotations.Name;

public class NameValidator implements ConstraintValidator<Name, String> {

    private static final Pattern NAME_PATTERN = compile("\\D+");

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        var matcher = NAME_PATTERN.matcher(value);
        return matcher.matches();
    }

}
