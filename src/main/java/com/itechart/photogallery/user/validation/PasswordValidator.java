package com.itechart.photogallery.user.validation;

import static java.util.regex.Pattern.compile;

import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.itechart.photogallery.user.validation.annotations.Password;

public class PasswordValidator implements ConstraintValidator<Password, String> {

    private static final Pattern PASSWORD_PATTERN = compile("(?=.*\\d)[\\w]{6,}");

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        var matcher = PASSWORD_PATTERN.matcher(value);
        return matcher.matches();
    }

}
