package com.itechart.photogallery.user.validation.constants;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public final class UserValidationMessages {

    public static final String INVALID_EMAIL_MESSAGE = "Invalid email.";
    public static final String INVALID_NAME_MESSAGE = "Can't contain numbers.";
    public static final String INVALID_PASSWORD_MESSAGE = "Password should contain both numbers and letters.";

}
