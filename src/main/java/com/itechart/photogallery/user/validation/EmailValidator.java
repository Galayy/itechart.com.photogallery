package com.itechart.photogallery.user.validation;

import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;

import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.itechart.photogallery.user.validation.annotations.Email;

public class EmailValidator implements ConstraintValidator<Email,String> {

    private static final Pattern EMAIL_PATTERN = compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
            CASE_INSENSITIVE);

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        var matcher = EMAIL_PATTERN.matcher(value);
        return matcher.matches();
    }

}
