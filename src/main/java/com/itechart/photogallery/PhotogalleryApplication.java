package com.itechart.photogallery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableCaching
@EnableSwagger2
@EnableJpaAuditing
@SpringBootApplication
@EnableJpaRepositories
@EnableElasticsearchRepositories
public class PhotogalleryApplication {

    public static void main(String[] args) {
        SpringApplication.run(PhotogalleryApplication.class, args);
    }

}
