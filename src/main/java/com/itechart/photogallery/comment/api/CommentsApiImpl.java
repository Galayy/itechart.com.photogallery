package com.itechart.photogallery.comment.api;

import static com.itechart.photogallery.common.utils.HeaderUtils.generatePaginationHeaders;

import static org.springframework.http.HttpStatus.*;

import java.util.List;
import java.util.UUID;

import com.itechart.photogallery.comment.service.CommentService;
import com.itechart.photogallery.common.generated.api.CommentsApi;
import com.itechart.photogallery.common.generated.model.Comment;
import com.itechart.photogallery.common.generated.model.CreateCommentRequest;
import com.itechart.photogallery.common.generated.model.UpdateCommentRequest;

import io.swagger.annotations.Api;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@Api(tags = "comments")
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class CommentsApiImpl implements CommentsApi {

    private final CommentService commentService;

    public ResponseEntity<List<Comment>> getComments(Integer page, Integer size, List<String> sortParams,
                                                     String searchParam, Boolean isDeleted) {
        var comments = commentService.getPage(sortParams, searchParam, isDeleted, page, size);
        var headers = generatePaginationHeaders(comments);
        return new ResponseEntity<>(comments.getContent(), headers, OK);
    }

    public ResponseEntity<List<Comment>> getAllComments(List<String> sortParams, String searchParam, Boolean isDeleted) {
        var users = commentService.getAll(sortParams, searchParam, isDeleted);
        return new ResponseEntity<>(users, OK);
    }

    public ResponseEntity<Comment> getComment(@PathVariable UUID id) {
        var comment = commentService.getById(id);
        return new ResponseEntity<>(comment, OK);
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Comment> updateComment(@PathVariable UUID id, @RequestBody UpdateCommentRequest
            commentToUpdate) {
        var updatedComment = commentService.update(id, commentToUpdate);
        return new ResponseEntity<>(updatedComment, OK);
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Comment> createComment(@RequestBody CreateCommentRequest comment) {
        var createdComment = commentService.create(comment);
        return new ResponseEntity<>(createdComment, CREATED);
    }

    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<Void> deleteComment(@PathVariable UUID id) {
        commentService.deleteById(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

}
