package com.itechart.photogallery.comment.repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.itechart.photogallery.comment.entity.CommentEntity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<CommentEntity, UUID> {

    @Modifying
    @Query("UPDATE CommentEntity comment " +
           "SET comment.deletedAt = :date " +
           "WHERE comment.id = :id")
    void deleteById(@Param("id") UUID id, @Param("date") Instant date);

    @Modifying
    @Query("UPDATE CommentEntity comment " +
           "SET comment.deletedAt = :date " +
           "WHERE comment.imageId = :imageId")
    void deleteByImageId(@Param("imageId") UUID imageId, @Param("date") Instant date);

    @Modifying
    @Query("UPDATE CommentEntity comment " +
           "SET comment.deletedAt = :date " +
           "WHERE comment.userId = :userId")
    void deleteByUserId(@Param("userId") UUID userId, @Param("date") Instant date);

    @Query("SELECT comment " +
           "FROM CommentEntity comment " +
           "WHERE comment.id = :id" +
           " AND comment.deletedAt IS NULL")
    Optional<CommentEntity> findById(@Param("id") UUID id);

    @Query("SELECT comment " +
           "FROM CommentEntity comment " +
           "WHERE comment.userId = :userId" +
           " AND comment.deletedAt IS NULL")
    List<CommentEntity> findAllByUserId(@Param("userId") UUID userId);

    @Query("SELECT comment " +
           "FROM CommentEntity comment " +
           "WHERE comment.userId = :userId" +
           " AND comment.createdAt >= :afterDate")
    List<CommentEntity> findAllByUserIdAndAfterDate(@Param("userId") UUID userId,
                                                    @Param("afterDate") Instant afterDate);

    @Query("SELECT comment " +
           "FROM CommentEntity comment " +
           "WHERE comment.imageId = :imageId" +
           " AND comment.deletedAt IS NULL")
    List<CommentEntity> findAllByImageId(@Param("imageId") UUID imageId);

    @Query("SELECT comment " +
           "FROM CommentEntity comment " +
           "WHERE comment.id IN :ids")
    List<CommentEntity> search(@Param("ids") List<UUID> ids, Sort sort);

    @Query("SELECT comment " +
           "FROM CommentEntity comment " +
           "WHERE comment.id IN :ids")
    Page<CommentEntity> search(@Param("ids") List<UUID> ids, Pageable pageable);

    @Query("SELECT comment " +
           "FROM CommentEntity comment " +
           "WHERE :searchParam IS NULL OR comment.content LIKE %:searchParam%" +
           " AND (:isDeleted IS NULL OR :isDeleted = true AND comment.deletedAt IS NOT NULL " +
           "      OR :isDeleted = false AND comment.deletedAt IS NULL)")
    Page<CommentEntity> search(@Param("searchParam") String searchParam, @Param("isDeleted") Boolean isDeleted,
                               Pageable pageable);

}
