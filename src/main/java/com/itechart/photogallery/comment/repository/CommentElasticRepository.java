package com.itechart.photogallery.comment.repository;

import java.util.UUID;

import com.itechart.photogallery.comment.elastic.CommentDocument;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
@ConditionalOnProperty(name = "spring.elasticsearch.enabled", havingValue = "true")
public interface CommentElasticRepository extends ElasticsearchRepository<CommentDocument, UUID> {

    @Query("{\"bool\": {\"must\": {\"match\":{\"content\":\"?0\"}}}}")
    Page<CommentDocument> searchAll(String content, Pageable pageable);

    @Query("{\"bool\": {\"must\":[" +
               "{\"match\":{\"content\":\"?0\"}}," +
               "{\"exists\" : {\"field\": \"deletedAt\"}}" +
            "]}}")
    Page<CommentDocument> searchDeleted(String content, Pageable pageable);

    @Query("{\"bool\": {\"must\":[" +
               "{\"match\":{\"content\":\"?0\"}}," +
               "{\"bool\": {\"must_not\": {\"exists\": {\"field\": \"deletedAt\"}}}}" +
            "]}}")
    Page<CommentDocument> searchExisted(String content, Pageable pageable);

    @Query("{\"bool\": {\"must_not\": {\"exists\": {\"field\": \"deletedAt\"}}}}")
    Page<CommentDocument> findExisted(Pageable pageable);

    @Query("{\"bool\": {\"must\": {\"exists\": {\"field\": \"deletedAt\"}}}}")
    Page<CommentDocument> findDeleted(Pageable pageable);

}
