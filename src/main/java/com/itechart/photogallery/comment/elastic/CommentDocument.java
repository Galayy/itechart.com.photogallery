package com.itechart.photogallery.comment.elastic;

import java.util.Date;
import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import lombok.Data;

@Data
@Document(indexName = "comment")
public class CommentDocument {

    @Id
    private UUID id;

    private String content;

    private Date deletedAt;

}
