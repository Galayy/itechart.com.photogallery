package com.itechart.photogallery.comment.service.impl;

import com.itechart.photogallery.comment.entity.CommentEntity;
import com.itechart.photogallery.comment.repository.CommentRepository;
import com.itechart.photogallery.comment.service.CommentSearchService;
import com.itechart.photogallery.comment.service.CommentService;
import com.itechart.photogallery.common.generated.model.Comment;
import com.itechart.photogallery.common.generated.model.CreateCommentRequest;
import com.itechart.photogallery.common.generated.model.UpdateCommentRequest;
import com.itechart.photogallery.image.service.ImageService;
import com.itechart.photogallery.user.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.itechart.photogallery.comment.mapper.CommentMapper.COMMENT_MAPPER;
import static com.itechart.photogallery.common.utils.ExceptionUtils.entityNotFoundException;
import static com.itechart.photogallery.common.utils.ExceptionUtils.isEnoughPermissions;
import static com.itechart.photogallery.common.utils.FilterUtils.checkFilterParams;
import static com.itechart.photogallery.common.utils.FilterUtils.createDeletedFilterParamOrDefault;
import static com.itechart.photogallery.common.utils.PagingUtils.toPageable;
import static com.itechart.photogallery.common.utils.SecurityUtils.hasAdminAccessLevel;
import static com.itechart.photogallery.common.utils.SortingUtils.parseSortOrDefault;
import static java.util.Collections.emptyList;

@Log4j2
@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    private static final String NAME = "comment";

    private final CommentSearchService commentSearchService;
    private final ImageService imageService;
    private final UserService userService;

    private final CommentRepository commentRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Comment> getAll(List<String> sortParams, String searchParam, Boolean isDeleted) {
        if (!hasAdminAccessLevel()) {
            checkFilterParams(isDeleted);
        }
        var size = commentRepository.findAll().size();

        var elasticPageable = toPageable(1, size);
        var preparedDeletedFilterParam = createDeletedFilterParamOrDefault(isDeleted);
        var preparedCommentIds = commentSearchService.getIds(searchParam, preparedDeletedFilterParam, elasticPageable);

        if (preparedCommentIds.isEmpty()) {
            log.info("IN getAll - comment: 0 comments were found");
            return emptyList();
        }
        var sort = parseSortOrDefault(sortParams, CommentEntity.class);
        var comments = commentRepository.search(preparedCommentIds, sort)
                .stream()
                .map(COMMENT_MAPPER::toModel)
                .collect(Collectors.toList());
        log.info("IN getAll - comment: {} comments were found", comments.size());
        return comments;
    }

    @Override
    @Transactional
    public Comment update(UUID id, UpdateCommentRequest commentToUpdate) {
        var currentUser = userService.getCurrent();
        var entityToUpdate = commentRepository.findById(id).orElseThrow(() -> entityNotFoundException(NAME, id));
        isEnoughPermissions(currentUser, entityToUpdate.getUserId());

        entityToUpdate.setContent(commentToUpdate.getNewContent());
        var updatedEntity = commentRepository.save(entityToUpdate);
        commentSearchService.update(id, updatedEntity);
        log.info("IN update - comment: comment was successfully updated");
        return COMMENT_MAPPER.toModel(updatedEntity);
    }

    @Override
    @Transactional
    public void deleteById(UUID id) {
        var currentUser = userService.getCurrent();
        var entity = commentRepository.findById(id).orElseThrow(() -> entityNotFoundException(NAME, id));

        isEnoughPermissions(currentUser, entity.getUserId());
        commentRepository.deleteById(id, Instant.now());
        commentSearchService.delete(id);
        log.info("IN delete - comment: comment was successfully deleted");
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Comment> getPage(List<String> sortParams, String searchParam, Boolean isDeleted,
                                 Integer page, Integer size) {
        if (!hasAdminAccessLevel()) {
            checkFilterParams(isDeleted);
        }

        var elasticPageable = toPageable(page, size);
        var preparedDeletedFilterParam = createDeletedFilterParamOrDefault(isDeleted);
        var preparedCommentIds = commentSearchService.getIds(searchParam, preparedDeletedFilterParam, elasticPageable);

        if (preparedCommentIds.isEmpty()) {
            log.info("IN getPage - comment: 0 comments were found");
            return Page.empty();
        }
        var sort = parseSortOrDefault(sortParams, CommentEntity.class);
        var pageable = toPageable(page, size, sort);

        var comments = commentRepository.search(preparedCommentIds, pageable).map(COMMENT_MAPPER::toModel);
        log.info("IN getAll - comment: {} comments were found", comments.getContent().size());
        return comments;
    }

    @Override
    @Transactional
    public Comment create(CreateCommentRequest comment) {
        imageService.getById(comment.getImageId());

        var commentToSave = new CommentEntity();
        commentToSave.setContent(comment.getContent());
        commentToSave.setImageId(comment.getImageId());
        commentToSave.setUserId(userService.getCurrent().getId());

        var savedComment = commentRepository.save(commentToSave);
        commentSearchService.create(savedComment);
        log.info("IN create - comment: comment was successfully created");
        return COMMENT_MAPPER.toModel(savedComment);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Comment> getByUserId(UUID id) {
        userService.getById(id);
        var comments = commentRepository.findAllByUserId(id)
                .stream()
                .map(COMMENT_MAPPER::toModel)
                .collect(Collectors.toList());
        log.info("IN getByUserId - comment: {} comments were found", comments.size());
        return comments;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Comment> getByImageId(UUID id) {
        imageService.getById(id);
        var comments = commentRepository.findAllByImageId(id)
                .stream()
                .map(COMMENT_MAPPER::toModel)
                .collect(Collectors.toList());
        log.info("IN getByImageId - comment: {} comments were found", comments.size());
        return comments;
    }

    @Override
    @Transactional(readOnly = true)
    public Comment getById(UUID id) {
        var entity = commentRepository.findById(id).orElseThrow(() -> entityNotFoundException(NAME, id));
        log.info("IN getById - comment: comment was successfully found");
        return COMMENT_MAPPER.toModel(entity);
    }

}
