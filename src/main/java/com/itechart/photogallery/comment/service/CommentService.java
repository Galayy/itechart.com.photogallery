package com.itechart.photogallery.comment.service;

import java.util.List;
import java.util.UUID;

import com.itechart.photogallery.common.generated.model.Comment;
import com.itechart.photogallery.common.generated.model.CreateCommentRequest;
import com.itechart.photogallery.common.generated.model.UpdateCommentRequest;
import com.itechart.photogallery.common.service.CommonService;

import org.springframework.data.domain.Page;

public interface CommentService extends CommonService<Comment> {

    Comment create(CreateCommentRequest comment);

    Comment update(UUID id, UpdateCommentRequest commentToUpdate);

    Page<Comment> getPage(List<String> sortParams, String searchParam, Boolean isDeleted, Integer page,
                          Integer size);

    List<Comment> getByUserId(UUID id);

    List<Comment> getByImageId(UUID id);

    List<Comment> getAll(List<String> sortParams, String searchParam, Boolean isDeleted);

}
