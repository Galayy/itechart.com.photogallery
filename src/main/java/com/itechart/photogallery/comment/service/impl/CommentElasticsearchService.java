package com.itechart.photogallery.comment.service.impl;

import com.itechart.photogallery.comment.elastic.CommentDocument;
import com.itechart.photogallery.comment.entity.CommentEntity;
import com.itechart.photogallery.comment.repository.CommentElasticRepository;
import com.itechart.photogallery.comment.service.CommentSearchService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.itechart.photogallery.common.utils.ExceptionUtils.entityNotFoundException;

@Log4j2
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(name = "spring.elasticsearch.enabled", havingValue = "true")
public class CommentElasticsearchService implements CommentSearchService {

    private static final String NAME = "comment";

    private final CommentElasticRepository commentElasticRepository;

    @Override
    public void create(CommentEntity entity) {
        var document = new CommentDocument();
        document.setContent(entity.getContent());
        document.setId(entity.getId());
        commentElasticRepository.save(document);
        log.info("IN create - comment elasticsearch: success");
    }

    @Override
    public void delete(UUID id) {
        var document = commentElasticRepository.findById(id).orElseThrow(() -> entityNotFoundException(NAME, id));
        document.setDeletedAt(Date.from(Instant.now()));
        commentElasticRepository.save(document);
        log.info("IN delete - comment elasticsearch: success");
    }

    @Override
    public List<UUID> getIds(String searchParam, Boolean isDeleted, Pageable pageable) {
        final Page<CommentDocument> documents;

        if (searchParam == null) {
            documents = findWithDeletedFilterParam(isDeleted, pageable);
        } else {
            documents = findWithDeletedFilterParam(searchParam, isDeleted, pageable);
        }

        var commentIds = documents.stream().map(CommentDocument::getId).collect(Collectors.toList());
        log.info("IN getIds - comment elasticsearch: {} comments were found", commentIds.size());
        return commentIds;
    }

    @Override
    public void update(UUID id, CommentEntity entity) {
        var documentToUpdate = commentElasticRepository.findById(id).orElseThrow(() ->
                entityNotFoundException(NAME, id));
        documentToUpdate.setContent(entity.getContent());
        commentElasticRepository.save(documentToUpdate);
        log.info("IN update - comment elasticsearch: success");
    }

    private Page<CommentDocument> findWithDeletedFilterParam(String searchParam, Boolean isDeleted, Pageable pageable) {
        if (isDeleted == null) {
            return commentElasticRepository.searchAll(searchParam, pageable);
        } else if (isDeleted) {
            return commentElasticRepository.searchDeleted(searchParam, pageable);
        } else {
            return commentElasticRepository.searchExisted(searchParam, pageable);
        }
    }

    private Page<CommentDocument> findWithDeletedFilterParam(Boolean isDeleted, Pageable pageable) {
        if (isDeleted == null) {
            return commentElasticRepository.findAll(pageable);
        } else if (isDeleted) {
            return commentElasticRepository.findDeleted(pageable);
        } else {
            return commentElasticRepository.findExisted(pageable);
        }
    }

}
