package com.itechart.photogallery.comment.service.impl;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.itechart.photogallery.comment.entity.CommentEntity;
import com.itechart.photogallery.comment.repository.CommentRepository;
import com.itechart.photogallery.comment.service.CommentSearchService;
import com.itechart.photogallery.common.entity.AbstractEntity;

import lombok.extern.log4j.Log4j2;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Log4j2
@Service
@RequiredArgsConstructor
@ConditionalOnMissingBean(CommentElasticsearchService.class)
public class CommentFulltextSearchService implements CommentSearchService {

    private final CommentRepository commentRepository;

    @Override
    public void update(UUID id, CommentEntity entity) { }

    @Override
    public List<UUID> getIds(String searchParam, Boolean isDeleted, Pageable pageable) {
        var comments = commentRepository.search(searchParam, isDeleted, pageable).getContent();
        var commentIds = comments.stream().map(AbstractEntity::getId).collect(Collectors.toList());
        log.info("IN getIds - comment fulltext search: {} ids were found", commentIds.size());
        return commentIds;
    }

    @Override
    public void create(CommentEntity entity) { }

    @Override
    public void delete(UUID id) { }

}
