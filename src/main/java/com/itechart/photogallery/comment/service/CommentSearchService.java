package com.itechart.photogallery.comment.service;

import java.util.List;
import java.util.UUID;

import com.itechart.photogallery.comment.entity.CommentEntity;
import com.itechart.photogallery.common.service.CommonSearchService;

import org.springframework.data.domain.Pageable;

public interface CommentSearchService extends CommonSearchService<CommentEntity> {

    void update(UUID id, CommentEntity entity);

    List<UUID> getIds(String searchParam, Boolean isDeleted, Pageable pageable);

}
