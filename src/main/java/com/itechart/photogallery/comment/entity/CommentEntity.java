package com.itechart.photogallery.comment.entity;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.itechart.photogallery.common.entity.AbstractEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "comment")
@EqualsAndHashCode(callSuper = true)
public class CommentEntity extends AbstractEntity {

    @Column(name = "content", nullable = false)
    private String content;

    @Column(name = "image_id", nullable = false, updatable = false)
    private UUID imageId;

    @Column(name = "user_id", nullable = false, updatable = false)
    private UUID userId;

}
