package com.itechart.photogallery.comment.mapper;

import com.itechart.photogallery.comment.entity.CommentEntity;
import com.itechart.photogallery.common.generated.model.Comment;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CommentMapper {

    CommentMapper COMMENT_MAPPER = Mappers.getMapper(CommentMapper.class);

    Comment toModel(CommentEntity commentEntity);

}
