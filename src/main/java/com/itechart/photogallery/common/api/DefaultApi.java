package com.itechart.photogallery.common.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import lombok.AllArgsConstructor;

@Controller
@AllArgsConstructor
public class DefaultApi {

    @GetMapping("/")
    public String defaultRedirect() {
        return "redirect:swagger-ui.html";
    }

}

