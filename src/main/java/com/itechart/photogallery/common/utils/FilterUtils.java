package com.itechart.photogallery.common.utils;

import static com.itechart.photogallery.common.utils.ExceptionUtils.forbiddenAccessException;
import static com.itechart.photogallery.common.utils.SecurityUtils.hasAdminAccessLevel;

import java.util.List;
import java.util.stream.Collectors;

import com.itechart.photogallery.common.entity.AbstractEntity;

import lombok.experimental.UtilityClass;

@UtilityClass
public class FilterUtils {

    public static Boolean createDeletedFilterParamOrDefault(Boolean isDeleted) {
        if (hasAdminAccessLevel()) {
            return isDeleted;
        }
        return false;
    }

    public static Boolean createEnabledFilterParamOrDefault(Boolean isEnabled) {
        if (hasAdminAccessLevel()) {
            return isEnabled;
        }
        return true;
    }

    public static void checkFilterParams(Boolean isDeleted, Boolean isEnabled, List<String> filterParams) {
        if (isDeleted != null || isEnabled != null || filterParams != null) {
            throw forbiddenAccessException("Access for admins only");
        }
    }

    public static void checkFilterParams(Boolean isDeleted, List<String> filterParams) {
        if (isDeleted != null || filterParams != null) {
            throw forbiddenAccessException("Access for admins only");
        }
    }

    public static void checkFilterParams(Boolean isDeleted) {
        if (isDeleted != null) {
            throw forbiddenAccessException("Access for admins only");
        }
    }

    public static List<? extends AbstractEntity> filterEntities(List<? extends AbstractEntity> entities, Boolean isDeleted) {
        if (isDeleted) {
            return entities.stream().filter(entity ->
                    entity.getDeletedAt() != null).collect(Collectors.toList());
        }
        return entities.stream().filter(entity -> entity.getDeletedAt() == null).collect(Collectors.toList());
    }

}
