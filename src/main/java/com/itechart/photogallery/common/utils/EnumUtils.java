package com.itechart.photogallery.common.utils;

import static com.itechart.photogallery.common.generated.model.UserRole.ADMIN;
import static com.itechart.photogallery.common.generated.model.UserRole.USER;

import java.util.AbstractMap;
import java.util.Map;

import com.itechart.photogallery.common.generated.model.UserRole;

import lombok.experimental.UtilityClass;

@UtilityClass
public class EnumUtils {

    private static final Map<String, UserRole> MAP = Map.ofEntries(
            new AbstractMap.SimpleEntry<>("ROLE_USER", USER),
            new AbstractMap.SimpleEntry<>("ROLE_ADMIN", ADMIN));

    public static UserRole fromAuthority(String authority) {
        return MAP.get(authority);
    }

}
