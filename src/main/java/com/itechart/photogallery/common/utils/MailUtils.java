package com.itechart.photogallery.common.utils;

import java.util.Map;

import lombok.experimental.UtilityClass;

import org.thymeleaf.context.Context;

@UtilityClass
public class MailUtils {

    public static Context createContext(Map<String, Object> variables){
        var context = new Context();
        context.setVariables(variables);
        return context;
    }

    public static Context createContext(String key, Object object){
        var context = new Context();
        context.setVariable(key, object);
        return context;
    }

}
