package com.itechart.photogallery.common.utils;

import static com.itechart.photogallery.user.entity.enums.UserEntityRole.ROLE_ADMIN;

import static org.springframework.security.core.context.SecurityContextHolder.getContext;

import java.util.Optional;

import com.itechart.photogallery.user.model.UserDetailsImpl;

import lombok.experimental.UtilityClass;

import org.springframework.security.core.userdetails.UserDetails;

@UtilityClass
public class SecurityUtils {

    public static Optional<UserDetailsImpl> getCurrentUserOpt() {
        var principal = getContext().getAuthentication().getPrincipal();
        return Optional.ofNullable((UserDetailsImpl) principal);
    }

    public static UserDetailsImpl getCurrentUser() {
        var principal = getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return (UserDetailsImpl) principal;
        }
        return null;
    }

    public static boolean hasAdminAccessLevel() {
        return getCurrentUser() != null && getCurrentUser().getRole().equals(ROLE_ADMIN);
    }

}
