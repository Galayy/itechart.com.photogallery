package com.itechart.photogallery.common.utils;

import java.util.List;
import java.util.UUID;

import lombok.experimental.UtilityClass;

import lombok.extern.log4j.Log4j2;

@Log4j2
@UtilityClass
public class SearchUtils {

    public static List<UUID> prepareEntityIds(List<UUID> ids) {
        log.info("IN prepareEntityIds - document: {} ids were found", ids.size());
        if (ids.isEmpty()) {
            return null;
        } else {
            return ids;
        }
    }

}
