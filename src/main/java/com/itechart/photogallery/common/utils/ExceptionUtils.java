package com.itechart.photogallery.common.utils;

import com.itechart.photogallery.common.exception.*;
import com.itechart.photogallery.common.generated.model.User;
import com.itechart.photogallery.user.entity.UserEntity;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.UUID;

import static com.itechart.photogallery.common.generated.model.UserRole.ADMIN;
import static com.itechart.photogallery.user.entity.enums.UserEntityRole.ROLE_USER;

@UtilityClass
public class ExceptionUtils {

    public static EntityAlreadyProcessedException entityAlreadyProcessedException(String string) {
        return new EntityAlreadyProcessedException(string);
    }

    public static ExpiredTokenException expiredTokenException(String string) {
        return new ExpiredTokenException(string);
    }

    public static EntityNotFoundException entityNotFoundException(String string) {
        return new EntityNotFoundException(string);
    }

    public static EntityNotFoundException entityNotFoundException(String entityName, UUID id) {
        return new EntityNotFoundException(String.format("There is no %s with id %s", entityName, id));
    }

    public static InvalidInputDataException invalidInputDataException(String string) {
        return new InvalidInputDataException(string);
    }

    public static UsernameNotFoundException usernameNotFoundException(String string) {
        return new UsernameNotFoundException(string);
    }

    public static ForbiddenAccessException forbiddenAccessException(String string) {
        return new ForbiddenAccessException(string);
    }

    public static void isEnoughPermissions(User currentUser, UUID requestedId) {
        if (!(currentUser.getId().equals(requestedId) || currentUser.getRole().equals(ADMIN))) {
            throw forbiddenAccessException("Low access level");
        }
    }

    public static void isEnoughPermissions(User currentUser, UserEntity requestedUser) {
        if (!(currentUser.getId().equals(requestedUser.getId()) || requestedUser.getRole().equals(ROLE_USER) &&
                currentUser.getRole().equals(ADMIN))) {
            throw forbiddenAccessException(String.format("You cannot delete user with id %s", requestedUser.getId()));
        }
    }

}
