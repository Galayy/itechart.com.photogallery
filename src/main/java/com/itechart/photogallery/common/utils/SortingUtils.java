package com.itechart.photogallery.common.utils;

import static com.itechart.photogallery.common.utils.ExceptionUtils.invalidInputDataException;

import static org.springframework.data.domain.Sort.Direction.DESC;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import lombok.experimental.UtilityClass;

import org.springframework.data.domain.Sort;

@UtilityClass
public class SortingUtils {

    public static Sort parseSortOrDefault(List<String> sortParams, Class<?> model) {
        if (sortParams == null) {
            return Sort.by(DESC, "createdAt");
        }
        return Sort.by(sortParams.stream().map(param -> parseSort(param, model)).collect(Collectors.toList()));
    }

    private static Sort.Order parseSort(String param, Class<?> model) {
        var fieldDirection = param.substring(0, 1);
        var field = param.substring(1);
        if (fieldDirection.equals("+") || fieldDirection.equals("-")) {
            validateSort(field, model);
            return fieldDirection.equals("+") ? Sort.Order.asc(field) : Sort.Order.desc((field));
        }
        throw invalidInputDataException("First param must be + or -");
    }

    private static void validateSort(String sortParam, Class<?> model) {
        try {
            Optional.ofNullable(model.getDeclaredField(sortParam)).orElseThrow(NoSuchFieldException::new);
        } catch (NoSuchFieldException ignored) {
            throw invalidInputDataException("Invalid column name");
        }
    }

}
