package com.itechart.photogallery.common.service.impl;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.List;

import com.itechart.photogallery.common.service.CachingService;

import org.redisson.api.RScoredSortedSet;
import org.redisson.api.RedissonClient;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class CachingServiceImpl implements CachingService {

    private final RedissonClient redissonClient;

    @Override
    public boolean absentsEntry(String key) {
        return redissonClient.getScoredSortedSet(key).remainTimeToLive() <= 0;
    }

    @Override
    public <T> void save(String key, Integer ttl, T object) {
        redissonClient.getBucket(key).set(object, ttl, SECONDS);
    }

    @Override
    public <T> void saveAll(String key, Integer ttl, List<T> objects) {
        var scoredSortedSet = redissonClient.getScoredSortedSet(key);
        scoredSortedSet.clear();
        objects.forEach(object -> {
            scoredSortedSet.add(objects.indexOf(object), object);
            setExpire(scoredSortedSet, ttl);
        });
    }

    @Override
    public <T> void add(String key, Integer ttl, T object) {
        var scoredSortedSet = redissonClient.getScoredSortedSet(key);
        scoredSortedSet.add(scoredSortedSet.size(), object);
        setExpire(scoredSortedSet, ttl);
    }

    private void setExpire(RScoredSortedSet set, int ttl) {
        if (ttl != 0) {
            set.expire(ttl, SECONDS);
        }
    }

}
