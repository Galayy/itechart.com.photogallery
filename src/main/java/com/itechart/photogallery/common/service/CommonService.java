package com.itechart.photogallery.common.service;

import java.util.UUID;

public interface CommonService<T> {

    T getById(UUID id);

    void deleteById(UUID id);

}
