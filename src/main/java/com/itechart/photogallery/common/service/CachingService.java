package com.itechart.photogallery.common.service;

import java.util.List;

public interface CachingService {

    boolean absentsEntry(String key);

    <T> void save(String key, Integer ttl, T object);

    <T> void saveAll(String key, Integer ttl, List<T> objects);

    <T> void add(String key, Integer ttl, T object);

}
