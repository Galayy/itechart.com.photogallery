package com.itechart.photogallery.common.service;

import java.util.UUID;

public interface CommonSearchService<T> {

    void create(T entity);

    void delete(UUID id);

}
