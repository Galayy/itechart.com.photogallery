package com.itechart.photogallery.common.exception;

import lombok.Data;

@Data
public class UnexpectedError {

    private String message;

}
