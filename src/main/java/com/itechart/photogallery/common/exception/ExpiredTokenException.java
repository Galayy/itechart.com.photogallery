package com.itechart.photogallery.common.exception;

import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

import org.springframework.http.HttpStatus;

import lombok.Getter;

public class ExpiredTokenException extends RuntimeException {

    @Getter
    private HttpStatus status;

    public ExpiredTokenException(String message) {
        super(message);
        this.status = UNPROCESSABLE_ENTITY;
    }

}
