package com.itechart.photogallery.common.exception;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

import org.springframework.http.HttpStatus;

import lombok.Getter;

public class InvalidInputDataException extends RuntimeException {

    @Getter
    private HttpStatus status;

    public InvalidInputDataException(String message) {
        super(message);
        this.status = BAD_REQUEST;
    }

}
