package com.itechart.photogallery.common.config;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itechart.photogallery.common.exception.*;

import io.sentry.Sentry;
import io.sentry.spring.SentryExceptionResolver;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class SentryConfig {

    private static final List<Class<? extends Exception>> IGNORE_EXCEPTIONS_LIST = List.of(
            BadCredentialsException.class, EntityAlreadyProcessedException.class, EntityNotFoundException.class,
            ExpiredTokenException.class, ForbiddenAccessException.class, IncorrectJwtAuthenticationException.class,
            InvalidInputDataException.class, MethodArgumentNotValidException.class
    );

    @Value("${dsn}")
    private String dsn;

    @Bean
    public void init() {
        Sentry.init(dsn);
    }

    @Bean
    public HandlerExceptionResolver sentryExceptionResolver() {
        return new SentryExceptionResolver() {
            @Override
            public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response,
                                                 Object handler, Exception ex) {
                if (!IGNORE_EXCEPTIONS_LIST.contains(ex.getClass())) {
                    super.resolveException(request, response, handler, ex);
                }
                return null;
            }
        };
    }

}
