package com.itechart.photogallery.common.config;

import java.net.URI;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedisConfig {

    @Bean
    public RedissonClient redissonClient(@Value("${spring.redis.url}") String url) {
        var redisUri = URI.create(url);
        var config = new Config();
        var serverConfig = config.useSingleServer()
                .setAddress(url)
                .setConnectionPoolSize(10)
                .setConnectionMinimumIdleSize(10)
                .setTimeout(5000);

        if (redisUri.getUserInfo() != null) {
            serverConfig.setPassword(redisUri.getUserInfo().substring(redisUri.getUserInfo().indexOf(":")+1));
        }
        return Redisson.create(config);
    }

}
