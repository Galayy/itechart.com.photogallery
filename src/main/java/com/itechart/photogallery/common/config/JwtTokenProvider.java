package com.itechart.photogallery.common.config;

import static java.util.Base64.getEncoder;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

import static com.itechart.photogallery.common.utils.EnumUtils.fromAuthority;
import static com.itechart.photogallery.common.utils.ExceptionUtils.invalidInputDataException;
import static com.itechart.photogallery.user.mapper.UserMapper.USER_MAPPER;

import static io.jsonwebtoken.SignatureAlgorithm.HS256;

import static org.apache.http.HttpHeaders.AUTHORIZATION;

import static org.springframework.security.core.context.SecurityContextHolder.clearContext;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import com.itechart.photogallery.common.exception.IncorrectJwtAuthenticationException;
import com.itechart.photogallery.common.generated.model.TokenResponse;
import com.itechart.photogallery.common.service.CachingService;
import com.itechart.photogallery.user.model.UserDetailsImpl;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;

import lombok.extern.log4j.Log4j2;

import org.redisson.api.RedissonClient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@Data
@Log4j2
@Component
@RequiredArgsConstructor
public class JwtTokenProvider {

    @Value("${app.jwt.secret.key}")
    private String secretKey;

    private static final String ROLE_KEY = "role";
    private static final String TOKEN_LOCK_KEY = "token:%s:lock";
    private static final String TOKEN_KEY = "token:%s";
    private static final String TOKEN_PREFIX = "Bearer ";
    private static final String EXPIRED_OR_INVALID_JWT_TOKEN = "Expired or invalid JWT token";

    private static final int TOKEN_EXPIRATION_TIME = 3_600_000;
    private static final int REFRESH_TOKEN_EXPIRATION_TIME = 360_000_000;
    private static final int WAIT_TIME_MILLIS = 1000;

    private final CachingService cachingService;

    private final RedissonClient redissonClient;

    @PostConstruct
    protected void init() {
        secretKey = getEncoder().encodeToString(secretKey.getBytes());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public TokenResponse refreshTokens(String refreshToken) {
        var authentication = getAuthentication(refreshToken);
        if (authentication == null) {
            throw new IllegalStateException();
        }

        var id = ((UserDetailsImpl) authentication.getPrincipal()).getUsername();
        var key = String.format(TOKEN_KEY, id);
        var token = findTokens(key, refreshToken);
        redissonClient.getScoredSortedSet(key).removeAll(token);

        var newToken = createToken(authentication);
        addToCache(id, newToken);
        return newToken;
    }

    public TokenResponse createToken(Authentication authentication) {
        var now = new Date();
        var tokenValidity = new Date(now.getTime() + TOKEN_EXPIRATION_TIME);
        var refreshTokenValidity = new Date(now.getTime() + REFRESH_TOKEN_EXPIRATION_TIME);

        var accessToken = genToken(authentication, now, tokenValidity);
        var refreshToken = genToken(authentication, now, refreshTokenValidity);

        var tokenResponse = new TokenResponse();
        tokenResponse.setAccessToken(accessToken);
        tokenResponse.setRefreshToken(refreshToken);
        tokenResponse.setAccessTokenExpirationDate(tokenValidity.toInstant());
        tokenResponse.setRefreshTokenExpirationDate(refreshTokenValidity.toInstant());

        log.info("IN createToken - JWT: JWT token was successfully created");
        return tokenResponse;
    }

    @SneakyThrows
    public void addToCache(String id, TokenResponse response) {
        var key = String.format(TOKEN_KEY, id);
        var lock = redissonClient.getLock(String.format(TOKEN_LOCK_KEY, id));

        try {
            lock.tryLock(WAIT_TIME_MILLIS, MILLISECONDS);
            cachingService.add(key, REFRESH_TOKEN_EXPIRATION_TIME, response);
        } finally {
            lock.unlock();
        }
    }

    public void logout(String id, String token) {
        var key = String.format(TOKEN_KEY, id);
        var tokenResponses = findTokens(key, token);
        isEmptyTokenResponseList(tokenResponses);
        redissonClient.getScoredSortedSet(key).removeAll(tokenResponses);
        clearContext();
        log.info("IN logout - JWT: logout was successfully performed");
    }

    public void logoutAll(String login) {
        var key = String.format(TOKEN_KEY, login);
        redissonClient.getScoredSortedSet(key).clear();
        clearContext();
        log.info("IN logoutAll - JWT: logout from all devices was successfully performed");
    }

    UsernamePasswordAuthenticationToken getAuthentication(String token) {
        var claims = getClaims(token);
        var role = fromAuthority(claims.get(ROLE_KEY).toString());
        var username = getUsername(token);

        var userDetails = new UserDetailsImpl();
        userDetails.setRole(USER_MAPPER.toEntity(role));
        userDetails.setId(UUID.fromString(username));
        return new UsernamePasswordAuthenticationToken(userDetails, token, userDetails.getAuthorities());
    }

    String resolveToken(HttpServletRequest request) {
        var bearerToken = request.getHeader(AUTHORIZATION);
        if (bearerToken != null && bearerToken.startsWith(TOKEN_PREFIX)) {
            return bearerToken.substring(7);
        }
        return null;
    }

    boolean validateToken(String token) {
        var claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
        var id = claims.getBody().getSubject();
        var key = String.format(TOKEN_KEY, id);

        var tokens = findTokens(key, token);
        return !claims.getBody().getExpiration().before(new Date()) && !tokens.isEmpty();
    }

    private String genToken(Authentication authentication, Date now, Date validity) {
        var userDetails = (UserDetailsImpl) authentication.getPrincipal();
        var claims = Jwts.claims();
        claims.put(ROLE_KEY, ((UserDetailsImpl) authentication.getPrincipal()).getRole());
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(HS256, secretKey)
                .compact();
    }

    private List<TokenResponse> findTokens(String key, String token) {
        return redissonClient.getScoredSortedSet(key)
                .stream()
                .map(TokenResponse.class::cast)
                .filter(tokenResponse -> tokenResponse.getAccessToken().equals(token)
                        || tokenResponse.getRefreshToken().equals(token))
                .collect(Collectors.toList());
    }

    private String getUsername(String token) {
        try {
            return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
        } catch (JwtException | IllegalArgumentException | ArrayIndexOutOfBoundsException e) {
            throw new IncorrectJwtAuthenticationException(EXPIRED_OR_INVALID_JWT_TOKEN);
        }
    }

    private Claims getClaims(String token) {
        try {
            return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
        } catch (JwtException e) {
            throw new IncorrectJwtAuthenticationException(EXPIRED_OR_INVALID_JWT_TOKEN);
        }
    }

    private void isEmptyTokenResponseList(List<TokenResponse> tokenResponses) {
        if (tokenResponses.isEmpty()) {
            throw invalidInputDataException(EXPIRED_OR_INVALID_JWT_TOKEN);
        }
    }

}
