package com.itechart.photogallery.common.filter;

import java.util.List;

public interface Filter<T> {

    List<T> createFilterParam(List<String> filterParams);

    List<T> prepareFilterParams(List<String> filterParams);

    T validateFilterParam(String filterParam);

}
