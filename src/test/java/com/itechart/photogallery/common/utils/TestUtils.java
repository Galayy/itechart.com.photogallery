package com.itechart.photogallery.common.utils;

import static com.itechart.photogallery.common.generated.model.UserRole.USER;

import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import com.github.javafaker.Faker;

import com.itechart.photogallery.common.generated.model.*;

public abstract class TestUtils {

    private static final Faker FAKE = Faker.instance(Locale.US, ThreadLocalRandom.current());

    public static SignUpRequest createSignUpRequest(UserRole role) {
        var signUpRequest = new SignUpRequest();
        signUpRequest.setAddress(FAKE.address().fullAddress());
        signUpRequest.setNickname(FAKE.name().name());
        signUpRequest.setFirstName(FAKE.name().firstName());
        signUpRequest.setLastName(FAKE.name().lastName());
        signUpRequest.setEmail(FAKE.bothify("???##@gmail.com"));
        signUpRequest.setPassword(FAKE.bothify("????####"));
        signUpRequest.setRole(role);
        return signUpRequest;
    }

    public static SignUpRequest createSignUpRequest(String firstName, String lastName, String email, String password) {
        var signUpRequest = new SignUpRequest();
        signUpRequest.setAddress(FAKE.address().fullAddress());
        signUpRequest.setNickname(FAKE.name().name());
        signUpRequest.setFirstName(firstName);
        signUpRequest.setLastName(lastName);
        signUpRequest.setEmail(email);
        signUpRequest.setPassword(password);
        signUpRequest.setRole(USER);
        return signUpRequest;
    }

    public static InitSendingTokenRequest createInitSendingTokenRequest(String email) {
        var initPasswordResetRequest = new InitSendingTokenRequest();
        initPasswordResetRequest.setEmail(email);
        return initPasswordResetRequest;
    }

    public static FinishPasswordResetRequest createFinishPasswordResetRequest(String resetPasswordToken) {
        var finishPasswordResetRequest = new FinishPasswordResetRequest();
        finishPasswordResetRequest.setResetPasswordToken(resetPasswordToken);
        finishPasswordResetRequest.setNewPassword(FAKE.bothify("????####"));
        return finishPasswordResetRequest;
    }

    public static FinishPasswordResetRequest createFinishPasswordResetRequest(String resetPasswordToken,
                                                                              String newPassword) {
        var finishPasswordResetRequest = new FinishPasswordResetRequest();
        finishPasswordResetRequest.setResetPasswordToken(resetPasswordToken);
        finishPasswordResetRequest.setNewPassword(newPassword);
        return finishPasswordResetRequest;
    }

    public static ConfirmRegistrationRequest createConfirmRegistrationRequest(String confirmRegistrationToken) {
        var confirmRegistrationRequest = new ConfirmRegistrationRequest();
        confirmRegistrationRequest.setConfirmRegistrationToken(confirmRegistrationToken);
        return confirmRegistrationRequest;
    }

    public static SignInRequest createSignInRequest(String login, String password) {
        var signInRequest = new SignInRequest();
        signInRequest.setEmail(login);
        signInRequest.setPassword(password);
        return signInRequest;
    }

    public static TokenRequest createTokenRequest(String token) {
        var tokenRequest = new TokenRequest();
        tokenRequest.setToken(token);
        return tokenRequest;
    }

    public static CreateCommentRequest createCommentRequest(UUID imageId) {
        var createCommentRequest = new CreateCommentRequest();
        createCommentRequest.setImageId(imageId);
        createCommentRequest.setContent(FAKE.book().title());
        return createCommentRequest;
    }

    public static CreateRankRequest createRankRequest(UUID imageId) {
        var createRankRequest = new CreateRankRequest();
        createRankRequest.setImageId(imageId);
        createRankRequest.setRank(FAKE.number().numberBetween(1, 9));
        return createRankRequest;
    }

    public static CreateRankRequest createRankRequest(Integer rank) {
        var createRankRequest = new CreateRankRequest();
        createRankRequest.setImageId(UUID.fromString(FAKE.internet().uuid()));
        createRankRequest.setRank(rank);
        return createRankRequest;
    }

    public static UpdateImageHistoryRequest createUpdateImageHistoryRequest(ImageHistoryStatus newStatus) {
        var updateImageHistoryRequest = new UpdateImageHistoryRequest();
        updateImageHistoryRequest.setNewStatus(newStatus);
        updateImageHistoryRequest.setNewReason(FAKE.book().title());
        return updateImageHistoryRequest;
    }

    public static UpdateUserRequest createUpdateUserRequest() {
        var userToUpdate = new UpdateUserRequest();
        userToUpdate.setNewAddress(FAKE.address().fullAddress());
        userToUpdate.setNewFirstName(FAKE.name().firstName());
        userToUpdate.setNewLastName(FAKE.name().lastName());
        userToUpdate.setNewNickname(FAKE.name().name());
        return userToUpdate;
    }

    public static UpdateUserRequest createUpdateUserRequest(String firstName, String lastName) {
        var userToUpdate = new UpdateUserRequest();
        userToUpdate.setNewAddress(FAKE.address().fullAddress());
        userToUpdate.setNewFirstName(firstName);
        userToUpdate.setNewLastName(lastName);
        userToUpdate.setNewNickname(FAKE.name().name());
        return userToUpdate;
    }

    public static ChangePasswordRequest createChangePasswordRequest(String oldPassword) {
        var changePasswordRequest = new ChangePasswordRequest();
        changePasswordRequest.setOldPassword(oldPassword);
        changePasswordRequest.setNewPassword(FAKE.bothify("????####"));
        return changePasswordRequest;
    }

    public static ChangePasswordRequest createChangePasswordRequest(String oldPassword, String newPassword) {
        var changePasswordRequest = new ChangePasswordRequest();
        changePasswordRequest.setOldPassword(oldPassword);
        changePasswordRequest.setNewPassword(newPassword);
        return changePasswordRequest;
    }

    public static UpdateCommentRequest createUpdateCommentRequest() {
        var commentToUpdate = new UpdateCommentRequest();
        commentToUpdate.setNewContent(FAKE.book().title());
        return commentToUpdate;
    }

    public static UpdateRankRequest createUpdateRankRequest() {
        var rankToUpdate = new UpdateRankRequest();
        rankToUpdate.setNewRank(FAKE.number().numberBetween(1, 9));
        return rankToUpdate;
    }

    public static UpdateRankRequest createUpdateRankRequest(Integer newRank) {
        var rankToUpdate = new UpdateRankRequest();
        rankToUpdate.setNewRank(newRank);
        return rankToUpdate;
    }

}
