package com.itechart.photogallery.integration.image.api;

import static com.itechart.photogallery.common.generated.model.ImageHistoryStatus.REJECTED;
import static com.itechart.photogallery.common.generated.model.UserRole.USER;
import static com.itechart.photogallery.common.utils.TestUtils.createSignUpRequest;
import static com.itechart.photogallery.common.utils.TestUtils.createUpdateImageHistoryRequest;
import static com.itechart.photogallery.image.mapper.ImageHistoryStatusMapper.IMAGE_HISTORY_STATUS_MAPPER;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.IMAGE_PNG_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.photogallery.common.generated.model.Comment;
import com.itechart.photogallery.common.generated.model.Image;
import com.itechart.photogallery.common.generated.model.Rank;
import com.itechart.photogallery.integration.AbstractIntegrationTest;

import org.junit.Test;

import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;

public class ImagesApiTest extends AbstractIntegrationTest {

    @Test
    public void getImages_HappyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        uploadTestImage(signedUpUser);

        //WHEN
        var mvcResult = mockMvc.perform(get(IMAGE_URL)).andExpect(status().isOk()).andReturn();
        final List<Image> images = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(),
                new TypeReference<>() {
                });

        //THEN
        assertThat(images.size()).isEqualTo(1);
    }

    @Test
    public void getImagesWhenForbidden() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        uploadTestImage(signedUpUser);

        //WHEN
        var resultActions = mockMvc.perform(get(IMAGE_URL).param("status", "ACCEPTED"));

        //THEN
        resultActions.andExpect(status().isForbidden());
    }

    @Test
    public void uploadImage_HappyPath() {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);

        //WHEN
        uploadTestImage(signedUpUser);

        //THEN
        assertThat(imageRepository.findAll().size()).isEqualTo(1);
    }

    @Test
    public void uploadImageWhenUnauthorized() throws Exception {
        //GIVEN
        var file = FAKE.internet().url();
        var newImage = new MockMultipartFile("file", file, IMAGE_PNG_VALUE, file.getBytes());

        //WHEN
        var resultActions = mockMvc.perform(multipart(IMAGE_URL).file(newImage)
                .contentType(MediaType.MULTIPART_FORM_DATA).accept(MediaType.APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isUnauthorized());
    }

    @Test
    public void deleteImageWhenUnauthorized() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var image = uploadTestImage(signedUpUser);

        //WHEN
        var resultActions = mockMvc.perform(delete(IMAGE_URL + "/{userId}", String.valueOf(image.getId())));

        //THEN
        resultActions.andExpect(status().isUnauthorized());
    }

    @Test
    public void deleteImageWithWrongId() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        uploadTestImage(signedUpUser);

        //WHEN
        var resultActions = mockMvc.perform(delete(IMAGE_URL + "/" + FAKE.internet().uuid())
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void deleteImage_HappyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);
        var imageHistory = imageHistoryRepository.findAll().get(0);
        var comment = createTestComment(tokenResponse, image);
        var rank = createTestRank(tokenResponse, image);

        //WHEN
        mockMvc.perform(delete(IMAGE_URL + "/{id}", String.valueOf(image.getId())).header(AUTHORIZATION,
                TOKEN_PREFIX + tokenResponse.getAccessToken())).andExpect(status().isNoContent());

        //THEN
        var deletedImage = imageRepository.getOne(image.getId());
        var deletedImageHistory = imageHistoryRepository.getOne(imageHistory.getId());
        var deletedComment = commentRepository.getOne(comment.getId());
        var deletedRank = rankRepository.getOne(rank.getId());

        assertSoftly(softly -> {
            softly.assertThat(deletedImage.getDeletedAt()).isNotNull();
            softly.assertThat(deletedImageHistory.getDeletedAt()).isNotNull();
            softly.assertThat(deletedComment.getDeletedAt()).isNotNull();
            softly.assertThat(deletedRank.getDeletedAt()).isNotNull();
        });
    }

    @Test
    public void updateImageHistory_HappyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);

        var image = uploadTestImage(signedUpUser);
        var imageHistory = imageHistoryRepository.findAll().get(0);
        var comment = createTestComment(tokenResponse, image);
        var rank = createTestRank(tokenResponse, image);
        var adminTokenResponse = createSignedInAdmin();

        var imageHistoryToUpdate = createUpdateImageHistoryRequest(REJECTED);

        //WHEN
        mockMvc.perform(put(IMAGE_URL + "/{imageId}/history", String.valueOf(imageHistory.getImageId()))
                .content(objectMapper.writeValueAsString(imageHistoryToUpdate)).contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, TOKEN_PREFIX + adminTokenResponse.getAccessToken()))
                .andExpect(status().isOk());

        //THEN
        var updatedImageHistory = imageHistoryRepository.getOne(imageHistory.getId());
        var deletedImage = imageRepository.getOne(image.getId());
        var deletedComment = commentRepository.getOne(comment.getId());
        var deletedRank = rankRepository.getOne(rank.getId());

        assertSoftly(softly -> {
            softly.assertThat(deletedImage.getDeletedAt()).isNotNull();
            softly.assertThat(deletedComment.getDeletedAt()).isNotNull();
            softly.assertThat(deletedRank.getDeletedAt()).isNotNull();
            softly.assertThat(updatedImageHistory.getReason()).isEqualTo(imageHistoryToUpdate.getNewReason());
            softly.assertThat(updatedImageHistory.getStatus()).isEqualTo(IMAGE_HISTORY_STATUS_MAPPER
                    .toEntity(imageHistoryToUpdate.getNewStatus()));
        });
    }

    @Test
    public void updateImageHistoryWithWrongId() throws Exception {
        //GIVEN
        var adminTokenResponse = createSignedInAdmin();
        var imageHistoryToUpdate = createUpdateImageHistoryRequest(REJECTED);

        //WHEN
        var resultActions = mockMvc.perform(put(IMAGE_URL + "/" + FAKE.internet().uuid() + "/history")
                .content(objectMapper.writeValueAsString(imageHistoryToUpdate)).contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, TOKEN_PREFIX + adminTokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void getImageComments_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);
        createTestComment(tokenResponse, image);

        //WHEN
        var mvcResult = mockMvc.perform(get(IMAGE_URL + "/{id}/comments", String.valueOf(image.getId()))
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken())).andExpect(status().isOk())
                .andReturn();
        final List<Comment> comments = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(),
                new TypeReference<>() {
                });

        //THEN
        assertThat(comments.size()).isEqualTo(1);
    }

    @Test
    public void getImageCommentsWithWrongId() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);

        //WHEN
        var resultActions = mockMvc.perform(get(IMAGE_URL + "/" + FAKE.internet().uuid() + "/comments")
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void getImageRanks_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);
        createTestRank(tokenResponse, image);

        //WHEN
        var mvcResult = mockMvc.perform(get(IMAGE_URL + "/{id}/ranks", String.valueOf(image.getId()))
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken())).andExpect(status().isOk())
                .andReturn();
        final List<Rank> ranks = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(),
                new TypeReference<>() {
                });

        //THEN
        assertThat(ranks.size()).isEqualTo(1);
    }

    @Test
    public void getImageRanksWithWrongId() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);

        //WHEN
        var resultActions = mockMvc.perform(get(IMAGE_URL + "/" + FAKE.internet().uuid() + "/ranks")
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

}
