package com.itechart.photogallery.integration.auth.api;

import static java.time.temporal.ChronoUnit.SECONDS;

import static com.itechart.photogallery.common.generated.model.UserRole.USER;
import static com.itechart.photogallery.common.utils.TestUtils.*;
import static com.itechart.photogallery.user.mapper.UserMapper.USER_MAPPER;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.itechart.photogallery.common.generated.model.TokenResponse;
import com.itechart.photogallery.common.generated.model.User;
import com.itechart.photogallery.integration.AbstractIntegrationTest;

import org.junit.Test;

import org.springframework.security.core.context.SecurityContextHolder;

public class AuthApiTest extends AbstractIntegrationTest {

    @Test
    public void initPasswordReset_happyPath() throws Exception {
        //GIVEN
        var signUpRequest = createSignUpRequest(USER);
        var user = signUpAndConfirmTestUser(signUpRequest);
        var initPasswordResetRequest = createInitSendingTokenRequest(user.getLogin());

        //WHEN
        createTestInitPasswordResetRequest(initPasswordResetRequest);
        var updatedUser = userRepository.getOne(user.getId());

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(updatedUser.getResetPasswordToken()).isNotNull();
            softly.assertThat(updatedUser.getResetPasswordDate()).isNotNull();
        });
    }

    @Test
    public void initPasswordResetWhenNotFound() throws Exception {
        //GIVEN
        var initPasswordResetRequest = createInitSendingTokenRequest(TEST_EMAIL);

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/reset-password/init")
                .content(objectMapper.writeValueAsString(initPasswordResetRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void finishConfirmRegistration_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);

        mockMvc.perform(post(AUTH_URL + "/signup").content(objectMapper.writeValueAsString(user))
                .contentType(APPLICATION_JSON)).andExpect(status().isCreated());

        var createdUser = userRepository.findAll().get(0);
        var confirmRegistrationRequest = createConfirmRegistrationRequest(createdUser.getConfirmRegistrationToken());

        //WHEN
        var mvcResult = mockMvc.perform(post(AUTH_URL + "/confirm-registration/finish")
                .content(objectMapper.writeValueAsString(confirmRegistrationRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        var confirmedUser = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), User.class);

        //THEN
        assertThat(confirmedUser.getEnabled()).isEqualTo(true);
    }

    @Test
    public void finishConfirmRegistrationWhenVerificationTokenNotFound() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        signedUpUser.setEnabled(false);
        userRepository.save(USER_MAPPER.toEntity(signedUpUser));
        var confirmRegistrationRequest = createConfirmRegistrationRequest(FAKE.random().hex(10));

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/confirm-registration/finish")
                .content(objectMapper.writeValueAsString(confirmRegistrationRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void signUp_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);

        //WHEN
        mockMvc.perform(post(AUTH_URL + "/signup").content(objectMapper.writeValueAsString(user))
                .contentType(APPLICATION_JSON)).andExpect(status().isCreated());

        //THEN
        var users = userRepository.findAll();

        assertThat(users.size()).isEqualTo(1);
    }

    @Test
    public void signUpWithExistingEmail() throws Exception {
        //GIVEN
        var signUpUserRequest = createSignUpRequest(USER);
        signUpAndConfirmTestUser(signUpUserRequest);

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/signup").
                content(objectMapper.writeValueAsString(signUpUserRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void signUpWhenNotLogout() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var anotherUser = createSignUpRequest(USER);

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(anotherUser)).contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isUnprocessableEntity());
    }


    @Test
    public void signIn_happyPath() throws Exception {
        //GIVEN
        var signUpRequest = createSignUpRequest(USER);
        var user = signUpAndConfirmTestUser(signUpRequest);

        //WHEN
        var token = signInTestUser(user, signUpRequest);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(token.getRefreshToken()).isNotNull();
            softly.assertThat(token.getAccessToken()).isNotNull();
        });
    }

    @Test
    public void signInWithWrongPassword() throws Exception {
        //GIVEN
        var signUpRequest = createSignUpRequest(USER);
        var user = signUpAndConfirmTestUser(signUpRequest);
        var signInRequest = createSignInRequest(user.getLogin(), TEST_PASSWORD);

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/signin").
                content(objectMapper.writeValueAsString(signInRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isUnauthorized());
    }

    @Test
    public void signInWithNotExistingEmail() throws Exception {
        //GIVEN
        var signInRequest = createSignInRequest(TEST_EMAIL, TEST_PASSWORD);

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/signin").
                content(objectMapper.writeValueAsString(signInRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void refresh_happyPath() throws Exception {
        //GIVEN
        var signUpRequest = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(signUpRequest);
        var tokenResponse = signInTestUser(signedUpUser, signUpRequest);

        var refreshToken = createTokenRequest(tokenResponse.getRefreshToken());

        //WHEN
        var mvcResult = mockMvc.perform(post(AUTH_URL + "/refresh")
                .content(objectMapper.writeValueAsString(refreshToken)).contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()))
                .andExpect(status().isCreated()).andReturn();
        var newToken = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), TokenResponse.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(newToken.getRefreshToken()).isNotNull();
            softly.assertThat(newToken.getAccessToken()).isNotNull();
        });
    }

    @Test
    public void finishPasswordReset_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var initPasswordResetRequest = createInitSendingTokenRequest(signedUpUser.getLogin());
        createTestInitPasswordResetRequest(initPasswordResetRequest);
        var updatedUser = userRepository.getOne(signedUpUser.getId());

        var finishPasswordResetRequest = createFinishPasswordResetRequest(updatedUser.getResetPasswordToken());

        //WHEN
        mockMvc.perform(put(AUTH_URL + "/reset-password/finish")
                .content(objectMapper.writeValueAsString(finishPasswordResetRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isOk());

        //THEN
        assertThat(userRepository.getOne(signedUpUser.getId())).isNotEqualTo(signedUpUser.getPassword());
    }

    @Test
    public void finishPasswordResetWhenConfirmationTokenNotFound() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        signUpAndConfirmTestUser(user);

        var finishPasswordResetRequest = createFinishPasswordResetRequest(FAKE.random().hex(10));

        //WHEN
        var resultActions = mockMvc.perform(put(AUTH_URL + "/reset-password/finish")
                .content(objectMapper.writeValueAsString(finishPasswordResetRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void finishPasswordResetWhenTokenExpired() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var initPasswordResetRequest = createInitSendingTokenRequest(signedUpUser.getLogin());
        var expiredDate = signedUpUser.getCreatedAt().minus(7200, SECONDS);

        createTestInitPasswordResetRequest(initPasswordResetRequest);
        var updatedUser = userRepository.getOne(signedUpUser.getId());
        updatedUser.setResetPasswordDate(expiredDate);
        userRepository.save(updatedUser);

        var finishPasswordResetRequest = createFinishPasswordResetRequest(updatedUser.getResetPasswordToken());

        //WHEN
        var resultActions = mockMvc.perform(put(AUTH_URL + "/reset-password/finish")
                .content(objectMapper.writeValueAsString(finishPasswordResetRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void initConfirmRegistration_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var initSendingTokenRequest = createInitSendingTokenRequest(signedUpUser.getLogin());

        signedUpUser.setEnabled(false);
        userRepository.save(USER_MAPPER.toEntity(signedUpUser));

        //WHEN
        mockMvc.perform(post(AUTH_URL + "/confirm-registration/init")
                .content(objectMapper.writeValueAsString(initSendingTokenRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isOk());

        //THEN
        var updatedUser = userRepository.getOne(signedUpUser.getId());

        assertThat(signedUpUser.getConfirmRegistrationToken()).isNotEqualTo(updatedUser.getConfirmRegistrationToken());
    }

    @Test
    public void initConfirmRegistrationWhenAlreadyEnabled() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        signUpAndConfirmTestUser(user);
        var initSendingTokenRequest = createInitSendingTokenRequest(user.getEmail());

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/confirm-registration/init")
                .content(objectMapper.writeValueAsString(initSendingTokenRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void initConfirmRegistrationWhenNotFound() throws Exception {
        //GIVEN
        var initSendingTokenRequest = createInitSendingTokenRequest(TEST_EMAIL);

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/confirm-registration/init")
                .content(objectMapper.writeValueAsString(initSendingTokenRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void logout_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var tokenRequest = createTokenRequest(tokenResponse.getAccessToken());

        //WHEN
        mockMvc.perform(post(AUTH_URL + "/logout")
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken())
                .content(objectMapper.writeValueAsString(tokenRequest))
                .contentType(APPLICATION_JSON)).andExpect(status().isOk());

        //THEN
        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

    @Test
    public void logoutWhenNotSignedIn() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var tokenRequest = createTokenRequest(tokenResponse.getAccessToken());

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/logout")
                .content(objectMapper.writeValueAsString(tokenRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isUnauthorized());
    }

    @Test
    public void logoutWhenWrongToken() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var tokenRequest = createTokenRequest(FAKE.crypto().sha256());

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/logout")
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken())
                .content(objectMapper.writeValueAsString(tokenRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isBadRequest());
    }

    @Test
    public void logoutAll_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);

        //WHEN
        mockMvc.perform(post(AUTH_URL + "/logoutAll")
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()))
                .andExpect(status().isOk());

        //THEN
        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

    @Test
    public void logoutAllWhenNotSignedIn() throws Exception {
        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/logoutAll"));

        //THEN
        resultActions.andExpect(status().isUnauthorized());
    }

}
