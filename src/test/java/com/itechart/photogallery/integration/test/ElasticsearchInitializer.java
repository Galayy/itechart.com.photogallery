package com.itechart.photogallery.integration.test;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;

public class ElasticsearchInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final String ELASTICSEARCH_TEST_CLUSTER_NAME = "photogallery-test-cluster";
    private static final int ELASTICSEARCH_TEST_PORT = 9300;

    private static final GenericContainer ELASTICSEARCH =
            new GenericContainer("elasticsearch:6.8.3")
                    .waitingFor(Wait.forListeningPort())
                    .withExposedPorts(ELASTICSEARCH_TEST_PORT)
                    .withEnv("discovery.type", "single-node")
                    .withEnv("cluster.name", ELASTICSEARCH_TEST_CLUSTER_NAME)
                    .withEnv("ES_JAVA_OPTS", "-Xms512m -Xmx512m")
                    .withEnv("xpack.security.enabled", "false");

    static {
        ELASTICSEARCH.start();
    }

    private void applyProperties(ConfigurableApplicationContext applicationContext) {
        TestPropertyValues.of(
                "spring.elasticsearch.enabled: true",
                "spring.data.elasticsearch.repositories.enabled: true",
                "elasticsearch.host:" + ELASTICSEARCH.getContainerIpAddress(),
                "elasticsearch.port:" + ELASTICSEARCH.getMappedPort(ELASTICSEARCH_TEST_PORT),
                "elasticsearch.cluster-name:" + ELASTICSEARCH_TEST_CLUSTER_NAME
        ).applyTo(applicationContext);
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        applyProperties(applicationContext);
    }

}
