package com.itechart.photogallery.integration.test;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

public class AwsInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final String AMAZON_S3_ACCESS_KEY = "test-photogallery-access-key";
    private static final String AMAZON_S3_SECRET_KEY = "test-photogallery-secret-key";

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        TestPropertyValues.of(
                "amazonProperties.accessKey:" + AMAZON_S3_ACCESS_KEY,
                "amazonProperties.secretKey:" + AMAZON_S3_SECRET_KEY
        ).applyTo(applicationContext);
    }

}
