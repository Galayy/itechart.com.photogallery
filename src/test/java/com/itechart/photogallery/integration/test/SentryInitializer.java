package com.itechart.photogallery.integration.test;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

public class SentryInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final String DEFAULT_DSN = "noop://localhost?async=false";

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        TestPropertyValues.of(
                "dsn:" + DEFAULT_DSN
        ).applyTo(applicationContext);
    }

}
