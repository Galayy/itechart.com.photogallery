package com.itechart.photogallery.integration.test;

import java.util.Properties;

import org.jetbrains.annotations.NotNull;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.PropertiesPropertySource;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;

public class MailInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final String PHOTOGALLERY_MAIL_PASSWORD = "test-photogallery-password";
    private static final int PHOTOGALLERY_MAIL_PORT = 1025;
    private static final String PHOTOGALLERY_MAIL_USER = "test-photogallery-user";

    private static final GenericContainer MAIL =
            new GenericContainer("mailhog/mailhog:latest")
                    .waitingFor(Wait.forListeningPort())
                    .withExposedPorts(PHOTOGALLERY_MAIL_PORT);

    static {
        MAIL.start();
    }

    private static String getHost() {
        return MAIL.getContainerIpAddress();
    }

    private static String getPort() {
        return String.valueOf(MAIL.getMappedPort(PHOTOGALLERY_MAIL_PORT));
    }

    @Override
    public void initialize(@NotNull ConfigurableApplicationContext applicationContext) {
        applyProperties(applicationContext);
    }

    private void applyProperties(ConfigurableApplicationContext applicationContext) {
        var environment = applicationContext.getEnvironment();

        var properties = new Properties();
        properties.setProperty("spring.mail.port", getPort());
        properties.setProperty("spring.mail.host", getHost());
        properties.setProperty("spring.mail.username", PHOTOGALLERY_MAIL_USER);
        properties.setProperty("spring.mail.password", PHOTOGALLERY_MAIL_PASSWORD);

        environment.getPropertySources().addFirst(new PropertiesPropertySource("mailProps", properties));
        applicationContext.setEnvironment(environment);
    }

}
