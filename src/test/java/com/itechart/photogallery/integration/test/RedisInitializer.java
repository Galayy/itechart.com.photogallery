package com.itechart.photogallery.integration.test;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.util.UriComponentsBuilder;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;

public class RedisInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final int REDIS_PORT = 6379;
    private static final int TTL = 1;

    private static final GenericContainer REDIS =
            new GenericContainer("redis:latest")
                    .waitingFor(Wait.forListeningPort())
                    .withExposedPorts(REDIS_PORT);

    static {
        REDIS.start();
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        applyProperties(applicationContext);
    }

    private static String getUrl() {
        return "redis:" + UriComponentsBuilder.newInstance().host(REDIS.getContainerIpAddress())
                .port(REDIS.getMappedPort(REDIS_PORT)).toUriString();
    }

    private void applyProperties(ConfigurableApplicationContext applicationContext) {
        TestPropertyValues.of(
                "spring.redis.url:" + getUrl(),
                "app.redis.image-ttl:" + TTL
        ).applyTo(applicationContext);
    }

}
