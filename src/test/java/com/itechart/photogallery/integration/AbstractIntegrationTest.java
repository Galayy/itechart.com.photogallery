package com.itechart.photogallery.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.itechart.photogallery.comment.repository.CommentRepository;
import com.itechart.photogallery.common.generated.model.*;
import com.itechart.photogallery.image.entity.ImageEntity;
import com.itechart.photogallery.image.entity.ImageHistoryEntity;
import com.itechart.photogallery.image.repository.ImageElasticRepository;
import com.itechart.photogallery.image.repository.ImageHistoryRepository;
import com.itechart.photogallery.image.repository.ImageRepository;
import com.itechart.photogallery.image.service.ImageSearchService;
import com.itechart.photogallery.integration.test.*;
import com.itechart.photogallery.rank.repository.RankRepository;
import com.itechart.photogallery.user.entity.UserEntity;
import com.itechart.photogallery.user.repository.UserElasticRepository;
import com.itechart.photogallery.user.repository.UserRepository;
import com.itechart.photogallery.user.service.UserSearchService;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.util.concurrent.ThreadLocalRandom;

import static com.itechart.photogallery.common.generated.model.UserRole.ADMIN;
import static com.itechart.photogallery.common.utils.TestUtils.*;
import static com.itechart.photogallery.image.entity.enums.ImageHistoryEntityStatus.ACCEPTED;
import static com.itechart.photogallery.image.mapper.ImageMapper.IMAGE_MAPPER;
import static com.itechart.photogallery.user.mapper.UserMapper.USER_MAPPER;
import static java.util.Locale.US;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.security.core.context.SecurityContextHolder.clearContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@ContextConfiguration(initializers = {
        AwsInitializer.class,
        ElasticsearchInitializer.class,
        MailInitializer.class,
        PostgresInitializer.class,
        RedisInitializer.class,
        SentryInitializer.class
})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractIntegrationTest {

    protected static final String AUTH_URL = "/api/v1/auth";
    protected static final String COMMENT_URL = "/api/v1/comments";
    protected static final String IMAGE_URL = "/api/v1/images";
    protected static final String RANK_URL = "/api/v1/ranks";
    protected static final String USER_URL = "/api/v1/users";

    protected static final Faker FAKE = Faker.instance(US, ThreadLocalRandom.current());
    protected static final String TEST_PASSWORD = FAKE.bothify("????####");
    protected static final String TEST_EMAIL = FAKE.bothify("???##@gmail.com");
    protected static final String TOKEN_PREFIX = "Bearer ";

    @Autowired
    protected ImageSearchService imageElasticsearchService;
    @Autowired
    protected UserSearchService userElasticsearchService;

    @Autowired
    protected CommentRepository commentRepository;
    @Autowired
    protected ImageElasticRepository imageElasticRepository;
    @Autowired
    protected ImageRepository imageRepository;
    @Autowired
    protected ImageHistoryRepository imageHistoryRepository;
    @Autowired
    protected RankRepository rankRepository;
    @Autowired
    protected UserElasticRepository userElasticRepository;
    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    protected PasswordEncoder passwordEncoder;

    @After
    public void clearDatabase() {
        commentRepository.deleteAll();
        imageElasticRepository.deleteAll();
        imageHistoryRepository.deleteAll();
        imageRepository.deleteAll();
        rankRepository.deleteAll();
        userElasticRepository.deleteAll();
        userRepository.deleteAll();
    }

    public User signUpAndConfirmTestUser(SignUpRequest user) {
        var encodedPassword = passwordEncoder.encode(user.getPassword());

        var entity = new UserEntity();
        entity.setAddress(user.getAddress());
        entity.setPassword(encodedPassword);
        entity.setLogin(user.getEmail());
        entity.setFirstName(user.getFirstName());
        entity.setNickname(user.getNickname());
        entity.setLastName(user.getLastName());
        entity.setRole(USER_MAPPER.toEntity(user.getRole()));
        entity.setConfirmRegistrationDate(Instant.now());
        entity.setConfirmRegistrationToken(RandomStringUtils.randomAscii(10));
        entity.setEnabled(true);
        var signedUpEntity = userRepository.save(entity);

        userElasticsearchService.create(signedUpEntity);
        return USER_MAPPER.toModel(signedUpEntity);
    }

    public User deleteTestUser(User user, TokenResponse tokenResponse) throws Exception {
        mockMvc.perform(delete(USER_URL + "/{id}", String.valueOf(user.getId())).header(AUTHORIZATION,
                TOKEN_PREFIX + tokenResponse.getAccessToken())).andExpect(status().isNoContent());
        return USER_MAPPER.toModel(userRepository.getOne(user.getId()));
    }

    public TokenResponse signInTestUser(User signedUpUser, SignUpRequest user) throws Exception {
        var signInRequest = createSignInRequest(signedUpUser.getLogin(), user.getPassword());

        var signInResult = mockMvc.perform(post(AUTH_URL + "/signin").content(objectMapper
                .writeValueAsString(signInRequest)).contentType(APPLICATION_JSON)).andExpect(status().isCreated())
                .andReturn();

        return objectMapper.readValue(signInResult.getResponse().getContentAsByteArray(), TokenResponse.class);
    }

    public Image uploadTestImage(User user) {
        var entity = new ImageEntity();
        entity.setImageUrl(FAKE.internet().url());
        entity.setUserId(user.getId());
        var image = imageRepository.save(entity);
        imageElasticsearchService.create(image);

        var imageHistoryEntity = new ImageHistoryEntity();
        imageHistoryEntity.setStatus(ACCEPTED);
        imageHistoryEntity.setImageId(image.getId());
        imageHistoryEntity.setReason(FAKE.gameOfThrones().quote());
        imageHistoryRepository.save(imageHistoryEntity);

        return IMAGE_MAPPER.toModel(image);
    }

    public Comment createTestComment(TokenResponse tokenResponse, Image image) throws Exception {
        var comment = createCommentRequest(image.getId());

        var mvcResult = mockMvc.perform(post(COMMENT_URL).content(objectMapper.writeValueAsString(comment)).contentType
                (APPLICATION_JSON).header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()))
                .andExpect(status().isCreated()).andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Comment.class);
    }

    public TokenResponse createSignedInAdmin() throws Exception {
        clearContext();
        var admin = createSignUpRequest(ADMIN);
        var signedUpAdmin = signUpAndConfirmTestUser(admin);
        return signInTestUser(signedUpAdmin, admin);
    }

    public Rank createTestRank(TokenResponse tokenResponse, Image image) throws Exception {
        var rank = createRankRequest(image.getId());

        var mvcResult = mockMvc.perform(post(RANK_URL).content(objectMapper.writeValueAsString(rank)).contentType
                (APPLICATION_JSON).header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()))
                .andExpect(status().isCreated()).andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Rank.class);
    }

    public void createTestInitPasswordResetRequest(InitSendingTokenRequest request) throws Exception {
        mockMvc.perform(post(AUTH_URL + "/reset-password/init")
                .content(objectMapper.writeValueAsString(request)).contentType(APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
