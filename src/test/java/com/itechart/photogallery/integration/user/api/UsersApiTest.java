package com.itechart.photogallery.integration.user.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.itechart.photogallery.common.generated.model.Comment;
import com.itechart.photogallery.common.generated.model.Image;
import com.itechart.photogallery.common.generated.model.Rank;
import com.itechart.photogallery.common.generated.model.User;
import com.itechart.photogallery.integration.AbstractIntegrationTest;
import org.junit.Test;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Collections;
import java.util.List;

import static com.itechart.photogallery.common.generated.model.UserRole.ADMIN;
import static com.itechart.photogallery.common.generated.model.UserRole.USER;
import static com.itechart.photogallery.common.utils.TestUtils.*;
import static com.itechart.photogallery.user.mapper.UserMapper.USER_MAPPER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UsersApiTest extends AbstractIntegrationTest {

    @Test
    public void getActivityReport_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var token = signInTestUser(signedUpUser, user);

        //WHEN
        var resultActions = mockMvc.perform(get(USER_URL + "/{id}/activity-report", signedUpUser.getId())
                .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void getActivityReportWhenNotFound() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var token = signInTestUser(signedUpUser, user);

        //WHEN
        var resultActions = mockMvc.perform(get(USER_URL + "/" + FAKE.internet().uuid() + "/activity-report")
                .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void getActivityReportWhenForbidden() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var anotherUser = createSignUpRequest(USER);
        var signedUpAnotherUser = signUpAndConfirmTestUser(anotherUser);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var token = signInTestUser(signedUpUser, user);

        //WHEN
        var resultActions = mockMvc.perform(get(USER_URL + "/{id}/activity-report",
                signedUpAnotherUser.getId()).header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isForbidden());
    }


    @Test
    public void getCurrentUser_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var token = signInTestUser(signedUpUser, user);

        //WHEN
        var mvcResult = mockMvc.perform(get(USER_URL + "/current").header(AUTHORIZATION,
                TOKEN_PREFIX + token.getAccessToken())).andExpect(status().isOk()).andReturn();
        var currentUser = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), User.class);

        //THEN
        assertThat(signedUpUser.getId()).isEqualTo(currentUser.getId());
    }

    @Test
    public void getCurrentUserWhenUnauthorized() throws Exception {
        //WHEN
        var resultActions = mockMvc.perform(get(USER_URL + "/current"));

        //THEN
        resultActions.andExpect(status().isUnauthorized());
    }

    @Test
    public void getUser_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var token = signInTestUser(signedUpUser, user);

        //WHEN
        var mvcResult = mockMvc.perform(get(USER_URL + "/{id}", String.valueOf(signedUpUser.getId()))
                .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken())).andExpect(status().isOk())
                .andReturn();
        var resultUser = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), User.class);

        //THEN
        assertThat(signedUpUser.getId()).isEqualTo(resultUser.getId());
    }

    @Test
    public void getUserWhenUnauthorized() throws Exception {
        //WHEN
        var resultActions = mockMvc.perform(get(USER_URL + FAKE.internet().uuid()));

        //THEN
        resultActions.andExpect(status().isUnauthorized());
    }

    @Test
    public void updateUserProfileWhenUnauthorized() throws Exception {
        //GIVEN
        var userToUpdate = createUpdateUserRequest();

        //WHEN
        var resultActions = mockMvc.perform(put(USER_URL + FAKE.internet().uuid() + "/profile")
                .content(objectMapper.writeValueAsString(userToUpdate)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isUnauthorized());
    }

    @Test
    public void updateUserProfile_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var userToUpdate = createUpdateUserRequest();

        //WHEN
        var mvcResult = mockMvc.perform(put(USER_URL + "/{id}/profile", String.valueOf(signedUpUser.getId()))
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken())
                .content(objectMapper.writeValueAsString(userToUpdate)).contentType(APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        var updatedUser = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), User.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(signedUpUser.getId()).isEqualTo(updatedUser.getId());
            softly.assertThat(userToUpdate.getNewFirstName()).isEqualTo(updatedUser.getFirstName());
        });
    }

    @Test
    public void updateUserProfileWithWrongId() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var userToUpdate = createUpdateUserRequest();

        //WHEN
        var resultActions = mockMvc.perform(put(USER_URL + "/" + FAKE.internet().uuid() + "/profile")
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken())
                .content(objectMapper.writeValueAsString(userToUpdate)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void changePassword_happyPath() throws Exception {
        //GIVEN
        var userToUpdate = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(userToUpdate);
        var tokenResponse = signInTestUser(signedUpUser, userToUpdate);
        var changePasswordRequest = createChangePasswordRequest(userToUpdate.getPassword());

        //WHEN
        var resultActions = mockMvc.perform(put(USER_URL + "/password")
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken())
                .content(objectMapper.writeValueAsString(changePasswordRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void changePasswordWhenUnauthorized() throws Exception {
        var changePasswordRequest = createChangePasswordRequest("somString1");

        //WHEN
        var resultActions = mockMvc.perform(put(USER_URL + "/password")
                .content(objectMapper.writeValueAsString(changePasswordRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isUnauthorized());
    }

    @Test
    public void getAllUsers_HappyPath() throws Exception {
        //GIVEN
        var firstUser = createSignUpRequest(USER);
        var secondUser = createSignUpRequest(USER);
        signUpAndConfirmTestUser(secondUser);
        signUpAndConfirmTestUser(firstUser);

        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.addAll("sortParams", Collections.singletonList("+address"));

        //WHEN
        var mvcResult = mockMvc.perform(get(USER_URL + "/all").params(params).param("searchParam",
                firstUser.getLastName())).andExpect(status().isOk()).andReturn();
        final List<User> users = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(),
                new TypeReference<>() {
                });

        //THEN
        assertThat(users.size()).isEqualTo(1);
    }

    @Test
    public void getAllUsersForAdmin() throws Exception {
        //GIVEN
        var admin = createSignUpRequest(ADMIN);
        var firstUser = createSignUpRequest(USER);
        var secondUser = createSignUpRequest(USER);
        var signedUpAdmin = signUpAndConfirmTestUser(admin);
        signUpAndConfirmTestUser(firstUser);
        signUpAndConfirmTestUser(secondUser);

        var tokenResponse = signInTestUser(signedUpAdmin, admin);

        //WHEN
        var mvcResult = mockMvc.perform(get(USER_URL + "/all").header(AUTHORIZATION, TOKEN_PREFIX +
                tokenResponse.getAccessToken())).andExpect(status().isOk()).andReturn();
        final List<User> users = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(),
                new TypeReference<>() {
                });

        //THEN
        assertThat(users.size()).isEqualTo(3);
    }

    @Test
    public void getDeletedUsersForAdmin() throws Exception {
        //GIVEN
        var admin = createSignUpRequest(ADMIN);
        var firstUser = createSignUpRequest(USER);
        var secondUser = createSignUpRequest(USER);
        var signedUpAdmin = signUpAndConfirmTestUser(admin);
        var signedUpUser = signUpAndConfirmTestUser(firstUser);
        signUpAndConfirmTestUser(secondUser);

        var tokenResponse = signInTestUser(signedUpAdmin, admin);
        deleteTestUser(signedUpUser, tokenResponse);

        //WHEN
        var mvcResult = mockMvc.perform(get(USER_URL + "/all").header(AUTHORIZATION, TOKEN_PREFIX +
                tokenResponse.getAccessToken()).param("isDeleted", "true")).andExpect(status().isOk())
                .andReturn();
        final List<User> users = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(),
                new TypeReference<>() {
                });

        //THEN
        assertThat(users.size()).isEqualTo(1);
    }

    @Test
    public void getAllUsersForUser() throws Exception {
        //GIVEN
        var admin = createSignUpRequest(ADMIN);
        var user = createSignUpRequest(USER);
        var anotherUser = createSignUpRequest(USER);
        signUpAndConfirmTestUser(admin);
        signUpAndConfirmTestUser(anotherUser);

        var signedUpUser = signUpAndConfirmTestUser(user);
        signedUpUser.setEnabled(false);
        var modifiedUser = userRepository.save(USER_MAPPER.toEntity(signedUpUser));
        userElasticsearchService.confirm(modifiedUser);

        //WHEN
        var mvcResult = mockMvc.perform(get(USER_URL + "/all")).andExpect(status().isOk()).andReturn();
        final List<User> users = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(),
                new TypeReference<>() {
                });

        //THEN
        assertThat(users.size()).isEqualTo(1);
    }

    @Test
    public void deleteUser_happyPath() throws Exception {
        //GIVEN
        var admin = createSignUpRequest(ADMIN);
        var user = createSignUpRequest(USER);

        var signedUpUser = signUpAndConfirmTestUser(user);
        var signedUpAdmin = signUpAndConfirmTestUser(admin);

        var userTokenResponse = signInTestUser(signedUpUser, user);
        var adminTokenResponse = signInTestUser(signedUpAdmin, admin);

        var image = uploadTestImage(signedUpUser);
        var imageHistory = imageHistoryRepository.findAll().get(0);
        var comment = createTestComment(userTokenResponse, image);
        var rank = createTestRank(userTokenResponse, image);

        //WHEN
        var deletedUser = deleteTestUser(signedUpUser, adminTokenResponse);

        //THEN
        var deletedComment = commentRepository.getOne(comment.getId());
        var deletedImage = imageRepository.getOne(image.getId());
        var deletedImageHistory = imageHistoryRepository.getOne(imageHistory.getId());
        var deletedRank = rankRepository.getOne(rank.getId());

        assertSoftly(softly -> {
            softly.assertThat(deletedUser.getDeletedAt()).isNotNull();
            softly.assertThat(deletedComment.getDeletedAt()).isNotNull();
            softly.assertThat(deletedImage.getDeletedAt()).isNotNull();
            softly.assertThat(deletedImageHistory.getDeletedAt()).isNotNull();
            softly.assertThat(deletedRank.getDeletedAt()).isNotNull();
        });
    }

    @Test
    public void deleteUserWhenForbidden() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var anotherUser = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var signedUpAnotherUser = signUpAndConfirmTestUser(anotherUser);

        var tokenResponse = signInTestUser(signedUpAnotherUser, anotherUser);

        //WHEN
        var resultActions = mockMvc.perform(delete(USER_URL + "/{id}", String.valueOf(signedUpUser.getId()))
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isForbidden());
    }

    @Test
    public void deleteUserWithWrongId() throws Exception {
        //GIVEN
        var admin = createSignUpRequest(USER);
        var signedUpAdmin = signUpAndConfirmTestUser(admin);
        var tokenResponse = signInTestUser(signedUpAdmin, admin);

        //WHEN
        var resultActions = mockMvc.perform(delete(USER_URL + "/" + FAKE.internet().uuid())
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void getUserWithWrongId() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);

        //WHEN
        var resultActions = mockMvc.perform(get(USER_URL + "/" + FAKE.internet().uuid())
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void getUserRanks_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);
        createTestRank(tokenResponse, image);

        //WHEN
        var mvcResult = mockMvc.perform(get(USER_URL + "/{id}/ranks", String.valueOf(signedUpUser.getId()))
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken())).andExpect(status().isOk())
                .andReturn();
        final List<Rank> comments = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(),
                new TypeReference<>() {
                });

        //THEN
        assertThat(comments.size()).isEqualTo(1);
    }

    @Test
    public void getUserRanksWithWrongId() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);

        //WHEN
        var resultActions = mockMvc.perform(get(USER_URL + "/" + FAKE.internet().uuid() + "/ranks")
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void getUserComments_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);
        createTestComment(tokenResponse, image);

        //WHEN
        var mvcResult = mockMvc.perform(get(USER_URL + "/{id}/comments", String.valueOf(signedUpUser.getId()))
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken())).andExpect(status().isOk())
                .andReturn();
        final List<Comment> comments = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(),
                new TypeReference<>() {
                });

        //THEN
        assertThat(comments.size()).isEqualTo(1);
    }

    @Test
    public void getUserCommentsWithWrongId() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);

        //WHEN
        var resultActions = mockMvc.perform(get(USER_URL + "/" + FAKE.internet().uuid() + "/comments")
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void getUserImages_HappyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        uploadTestImage(signedUpUser);

        //WHEN
        var mvcResult = mockMvc.perform(get(USER_URL + "/{userId}/images", String.valueOf(signedUpUser.getId()))
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken())).andExpect(status()
                .isOk()).andReturn();
        final List<Image> images = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(),
                new TypeReference<>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(images.size()).isEqualTo(1);
            softly.assertThat(images.get(0).getUserId()).isEqualTo(signedUpUser.getId());
        });
    }

    @Test
    public void getUserImagesWhenNotFound() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        uploadTestImage(signedUpUser);

        //WHEN
        var resultActions = mockMvc.perform(get(USER_URL + "/" + FAKE.internet().uuid() + "/images")
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

}
