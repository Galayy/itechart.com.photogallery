package com.itechart.photogallery.integration.comment.api;

import static com.itechart.photogallery.common.generated.model.UserRole.USER;
import static com.itechart.photogallery.common.utils.TestUtils.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.photogallery.common.generated.model.Comment;
import com.itechart.photogallery.integration.AbstractIntegrationTest;

import org.junit.Test;

public class CommentsApiTest extends AbstractIntegrationTest {

    @Test
    public void getComments_HappyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);
        createTestComment(tokenResponse, image);

        //WHEN
        var mvcResult = mockMvc.perform(get(COMMENT_URL)).andExpect(status().isOk()).andReturn();
        final List<Comment> comments = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(),
                new TypeReference<>() {
                });

        //THEN
        assertThat(comments.size()).isEqualTo(1);
    }

    @Test
    public void getAllCommentsWhenForbidden() throws Exception {
        //WHEN
        var resultActions = mockMvc.perform(get(COMMENT_URL + "/all").param("isDeleted",
                "true"));

        //THEN
        resultActions.andExpect(status().isForbidden());
    }

    @Test
    public void createComment_HappyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);

        //WHEN
        var comment = createTestComment(tokenResponse, image);

        //THEN
        assertThat(comment.getImageId()).isEqualTo(image.getId());
    }

    @Test
    public void createCommentWhenUnauthorized() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var image = uploadTestImage(signedUpUser);
        var comment = createCommentRequest(image.getId());

        //WHEN
        var resultActions = mockMvc.perform(post(COMMENT_URL).content(objectMapper.writeValueAsString(comment))
                .contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isUnauthorized());
    }

    @Test
    public void createCommentWhenNotFound() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var comment = createCommentRequest(UUID.fromString(FAKE.internet().uuid()));

        //WHEN
        var resultActions = mockMvc.perform(post(COMMENT_URL).content(objectMapper.writeValueAsString(comment))
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken())
                .contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void getCommentWhenNotFound() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);

        //WHEN
        var resultActions = mockMvc.perform(get(COMMENT_URL + "/" + FAKE.internet().uuid())
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void getComment_HappyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);
        var comment = createTestComment(tokenResponse, image);

        //WHEN
        var mvcResult = mockMvc.perform(get(COMMENT_URL + "/{id}", String.valueOf(comment.getId()))
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()))
                .andExpect(status().isOk()).andReturn();
        var requestedComment = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Comment.class);

        //THEN
        assertThat(requestedComment.getContent()).isEqualTo(comment.getContent());
    }

    @Test
    public void updateComment_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);
        var comment = createTestComment(tokenResponse, image);
        var commentToUpdate = createUpdateCommentRequest();

        //WHEN
        var mvcResult = mockMvc.perform(put(COMMENT_URL + "/{id}", String.valueOf(comment.getId()))
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken())
                .content(objectMapper.writeValueAsString(commentToUpdate)).contentType(APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        var updatedComment = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Comment.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(updatedComment.getId()).isEqualTo(comment.getId());
            softly.assertThat(updatedComment.getContent()).isEqualTo(commentToUpdate.getNewContent());
        });
    }

    @Test
    public void updateCommentWithWrongId() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var commentToUpdate = createUpdateCommentRequest();

        //WHEN
        var resultActions = mockMvc.perform(put(COMMENT_URL + "/" + FAKE.internet().uuid())
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken())
                .content(objectMapper.writeValueAsString(commentToUpdate)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void updateCommentWhenForbidden() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var anotherUser = createSignUpRequest(USER);

        var signedUpUser = signUpAndConfirmTestUser(user);
        var signedUpAnotherUser = signUpAndConfirmTestUser(anotherUser);

        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);
        var comment = createTestComment(tokenResponse, image);
        var commentToUpdate = createUpdateCommentRequest();
        var anotherTokenResponse = signInTestUser(signedUpAnotherUser, anotherUser);

        //WHEN
        var resultActions = mockMvc.perform(put(COMMENT_URL + "/{id}", String.valueOf(comment.getId()))
                .header(AUTHORIZATION, TOKEN_PREFIX + anotherTokenResponse.getAccessToken())
                .content(objectMapper.writeValueAsString(commentToUpdate)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isForbidden());
    }

    @Test
    public void deleteComment_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);
        var comment = createTestComment(tokenResponse, image);

        //WHEN
        mockMvc.perform(delete(COMMENT_URL + "/{id}", String.valueOf(comment.getId()))
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()))
                .andExpect(status().isNoContent());

        //THEN
        var deletedComment = commentRepository.getOne(comment.getId());

        assertThat(deletedComment.getDeletedAt()).isNotNull();
    }

    @Test
    public void deleteCommentWithWrongId() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);

        //WHEN
        var resultActions = mockMvc.perform(delete(COMMENT_URL + "/" + FAKE.internet().uuid())
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void deleteCommentWhenUnauthorized() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);
        var comment = createTestComment(tokenResponse, image);

        //WHEN
        var resultActions = mockMvc.perform(delete(COMMENT_URL + "/{id}", String.valueOf(comment.getId())));

        //THEN
        resultActions.andExpect(status().isUnauthorized());
    }

}
