package com.itechart.photogallery.integration.rank.api;

import static com.itechart.photogallery.common.generated.model.UserRole.USER;
import static com.itechart.photogallery.common.utils.TestUtils.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.photogallery.common.generated.model.Rank;
import com.itechart.photogallery.integration.AbstractIntegrationTest;

import org.junit.Test;

public class RanksApiTest extends AbstractIntegrationTest {

    @Test
    public void getRankWhenNotFound() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);

        //WHEN
        var resultActions = mockMvc.perform(get(RANK_URL + "/" + FAKE.internet().uuid())
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void getRank_HappyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);
        var rank = createTestRank(tokenResponse, image);

        //WHEN
        var mvcResult = mockMvc.perform(get(RANK_URL + "/{id}", String.valueOf(rank.getId()))
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()))
                .andExpect(status().isOk()).andReturn();
        var requestedRank = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Rank.class);

        //THEN
        assertThat(requestedRank.getRank()).isEqualTo(rank.getRank());
    }

    @Test
    public void getAllRanks_HappyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);
        createTestRank(tokenResponse, image);

        //WHEN
        var mvcResult = mockMvc.perform(get(RANK_URL + "/all")).andExpect(status().isOk()).andReturn();
        final List<Rank> ranks = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(),
                new TypeReference<>() {
                });

        //THEN
        assertThat(ranks.size()).isEqualTo(1);
    }

    @Test
    public void getAllRanksWhenInvalidMinAndMax() throws Exception {
        //WHEN
        var resultActions = mockMvc.perform(get(RANK_URL + "/all").param("minRank", "8")
                .param("maxRank", "7"));

        //THEN
        resultActions.andExpect(status().isBadRequest());
    }

    @Test
    public void getRanksWhenForbidden() throws Exception {
        //WHEN
        var resultActions = mockMvc.perform(get(RANK_URL).param("isDeleted", "true"));

        //THEN
        resultActions.andExpect(status().isForbidden());
    }

    @Test
    public void createRank_HappyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);

        //WHEN
        var rank = createTestRank(tokenResponse, image);

        //THEN
        assertThat(rank.getImageId()).isEqualTo(image.getId());
    }

    @Test
    public void createRankWhenUnauthorized() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var image = uploadTestImage(signedUpUser);
        var rank = createRankRequest(image.getId());

        //WHEN
        var resultActions = mockMvc.perform(post(RANK_URL).content(objectMapper.writeValueAsString(rank))
                .contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isUnauthorized());
    }

    @Test
    public void createRankWhenNotFound() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var rank = createRankRequest(UUID.fromString(FAKE.internet().uuid()));

        //WHEN
        var resultActions = mockMvc.perform(post(RANK_URL).content(objectMapper.writeValueAsString(rank))
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken())
                .contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void updateRank_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);
        var rank = createTestRank(tokenResponse, image);
        var rankToUpdate = createUpdateRankRequest();

        //WHEN
        var mvcResult = mockMvc.perform(put(RANK_URL + "/{id}", String.valueOf(rank.getId()))
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken())
                .content(objectMapper.writeValueAsString(rankToUpdate)).contentType(APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        var updatedRank = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Rank.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(updatedRank.getId()).isEqualTo(rank.getId());
            softly.assertThat(updatedRank.getRank()).isEqualTo(rankToUpdate.getNewRank());
        });
    }

    @Test
    public void updateRankWithWrongId() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var rankToUpdate = createUpdateRankRequest();

        //WHEN
        var resultActions = mockMvc.perform(put(RANK_URL + "/" + FAKE.internet().uuid())
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken())
                .content(objectMapper.writeValueAsString(rankToUpdate)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void updateRankWhenForbidden() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var anotherUser = createSignUpRequest(USER);

        var signedUpUser = signUpAndConfirmTestUser(user);
        var signedUpAnotherUser = signUpAndConfirmTestUser(anotherUser);

        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);
        var rank = createTestRank(tokenResponse, image);
        var rankToUpdate = createUpdateRankRequest();
        var anotherTokenResponse = signInTestUser(signedUpAnotherUser, anotherUser);

        //WHEN
        var resultActions = mockMvc.perform(put(RANK_URL + "/{id}", String.valueOf(rank.getId()))
                .header(AUTHORIZATION, TOKEN_PREFIX + anotherTokenResponse.getAccessToken())
                .content(objectMapper.writeValueAsString(rankToUpdate)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isForbidden());
    }

    @Test
    public void deleteRank_happyPath() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);
        var rank = createTestRank(tokenResponse, image);

        //WHEN
        mockMvc.perform(delete(RANK_URL + "/{id}", String.valueOf(rank.getId()))
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()))
                .andExpect(status().isNoContent());

        //THEN
        var deletedComment = rankRepository.getOne(rank.getId());

        assertThat(deletedComment.getDeletedAt()).isNotNull();
    }

    @Test
    public void deleteRankWithWrongId() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);

        //WHEN
        var resultActions = mockMvc.perform(delete(RANK_URL + "/" + FAKE.internet().uuid())
                .header(AUTHORIZATION, TOKEN_PREFIX + tokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void deleteCommentWhenUnauthorized() throws Exception {
        //GIVEN
        var user = createSignUpRequest(USER);
        var signedUpUser = signUpAndConfirmTestUser(user);
        var tokenResponse = signInTestUser(signedUpUser, user);
        var image = uploadTestImage(signedUpUser);
        var rank = createTestRank(tokenResponse, image);

        //WHEN
        var resultActions = mockMvc.perform(delete(RANK_URL + "/{id}", String.valueOf(rank.getId())));

        //THEN
        resultActions.andExpect(status().isUnauthorized());
    }

}
