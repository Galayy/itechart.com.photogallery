package com.itechart.photogallery.unit.auth.api;

import static com.itechart.photogallery.common.utils.TestUtils.*;
import static com.itechart.photogallery.unit.ValidationMessages.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.photogallery.auth.service.AuthService;
import com.itechart.photogallery.common.exception.UnexpectedError;
import com.itechart.photogallery.common.generated.model.TokenResponse;
import com.itechart.photogallery.unit.AbstractUnitTest;

import org.junit.Test;

import org.mockito.Mock;

public class AuthApiTest extends AbstractUnitTest {

    private static final String AUTH_INIT_CONFIRM_REGISTRATION_URL = "/api/v1/auth/confirm-registration/init";
    private static final String AUTH_RESET_PASSWORD_URL = "/api/v1/auth/reset-password";
    private static final String AUTH_SIGNIN_URL = "/api/v1/auth/signin";
    private static final String AUTH_SIGNUP_URL = "/api/v1/auth/signup";

    @Mock
    private AuthService authService;

    @Test
    public void finishPasswordReset_happyPath() throws Exception {
        //GIVEN
        var resetPasswordRequest = createFinishPasswordResetRequest(VALID_PASSWORD);
        doNothing().when(authService).finishPasswordReset(resetPasswordRequest);

        //WHEN
        var resultActions = mockMvc.perform(put(AUTH_RESET_PASSWORD_URL + "/finish")
                .content(objectMapper.writeValueAsString(resetPasswordRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void finishPasswordResetWithInvalidNewPassword() throws Exception {
        //GIVEN
        var resetPasswordRequest = createFinishPasswordResetRequest(FAKE.random().hex(10), INVALID_PASSWORD);

        //WHEN
        var mvcResult = mockMvc.perform(put(AUTH_RESET_PASSWORD_URL + "/finish")
                .content(objectMapper.writeValueAsString(resetPasswordRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(INVALID_PASSWORD_MESSAGE);
        });
    }

    @Test
    public void initPasswordReset_happyPath() throws Exception {
        //GIVEN
        var emailRequest = createInitSendingTokenRequest(VALID_EMAIL);
        doNothing().when(authService).initPasswordReset(emailRequest);

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_RESET_PASSWORD_URL + "/init")
                .content(objectMapper.writeValueAsString(emailRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void initPasswordResetWithInvalidEmail() throws Exception {
        //GIVEN
        var emailRequest = createInitSendingTokenRequest(INVALID_EMAIL);

        //WHEN
        var mvcResult = mockMvc.perform(post(AUTH_RESET_PASSWORD_URL + "/init")
                .content(objectMapper.writeValueAsString(emailRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(INVALID_EMAIL_MESSAGE);
        });
    }

    @Test
    public void signUp_happyPath() throws Exception {
        //GIVEN
        var signUpRequest = createSignUpRequest(VALID_NAME, VALID_NAME, VALID_EMAIL, VALID_PASSWORD);
        doNothing().when(authService).signUp(signUpRequest);

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_SIGNUP_URL).content(objectMapper.writeValueAsString(signUpRequest))
                .contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isCreated());
    }

    @Test
    public void signUpWithInvalidLogin() throws Exception {
        //GIVEN
        var signUpRequest = createSignUpRequest(VALID_NAME, VALID_NAME, INVALID_EMAIL, VALID_PASSWORD);

        //WHEN
        var mvcResult = mockMvc.perform(post(AUTH_SIGNUP_URL).content(objectMapper.writeValueAsString(signUpRequest))
                .contentType(APPLICATION_JSON)).andExpect(status().isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(INVALID_EMAIL_MESSAGE);
        });
    }

    @Test
    public void signUpWithInvalidPassword() throws Exception {
        var signUpRequest = createSignUpRequest(VALID_NAME, VALID_NAME, VALID_EMAIL, INVALID_PASSWORD);

        //WHEN
        var mvcResult = mockMvc.perform(post(AUTH_SIGNUP_URL).content(objectMapper.writeValueAsString(signUpRequest))
                .contentType(APPLICATION_JSON)).andExpect(status().isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(INVALID_PASSWORD_MESSAGE);
        });
    }

    @Test
    public void signUpWithInvalidName() throws Exception {
        var signUpRequest = createSignUpRequest(INVALID_NAME, VALID_NAME, VALID_EMAIL, VALID_PASSWORD);

        //WHEN
        var mvcResult = mockMvc.perform(post(AUTH_SIGNUP_URL).content(objectMapper.writeValueAsString(signUpRequest))
                .contentType(APPLICATION_JSON)).andExpect(status().isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(INVALID_NAME_MESSAGE);
        });
    }

    @Test
    public void signUpWithMultipleInvalidFields() throws Exception {
        var signUpRequest = createSignUpRequest(INVALID_NAME, INVALID_NAME, INVALID_EMAIL, INVALID_PASSWORD);

        //WHEN
        var mvcResult = mockMvc.perform(post(AUTH_SIGNUP_URL).content(objectMapper.writeValueAsString(signUpRequest))
                .contentType(APPLICATION_JSON)).andExpect(status().isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertThat(errors.size()).isEqualTo(4);
    }

    @Test
    public void signIn_happyPath() throws Exception {
        //GIVEN
        var signInRequest = createSignInRequest(VALID_EMAIL, VALID_PASSWORD);
        given(authService.signIn(signInRequest)).willReturn(new TokenResponse());

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_SIGNIN_URL).content(objectMapper.writeValueAsString(signInRequest))
                .contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isCreated());
    }

    @Test
    public void signInWithInvalidLogin() throws Exception {
        //GIVEN
        var signInRequest = createSignInRequest(INVALID_EMAIL, VALID_PASSWORD);

        //WHEN
        var mvcResult = mockMvc.perform(post(AUTH_SIGNIN_URL).content(objectMapper.writeValueAsString(signInRequest))
                .contentType(APPLICATION_JSON)).andExpect(status().isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(INVALID_EMAIL_MESSAGE);
        });
    }

    @Test
    public void signInWithInvalidPassword() throws Exception {
        //GIVEN
        var signInRequest = createSignInRequest(VALID_EMAIL, INVALID_PASSWORD);

        //WHEN
        var mvcResult = mockMvc.perform(post(AUTH_SIGNIN_URL).content(objectMapper.writeValueAsString(signInRequest))
                .contentType(APPLICATION_JSON)).andExpect(status().isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(INVALID_PASSWORD_MESSAGE);
        });
    }

    @Test
    public void signInWithInvalidLoginAndPassword() throws Exception {
        //GIVEN
        var signInRequest = createSignInRequest(INVALID_EMAIL, INVALID_PASSWORD);

        //WHEN
        var mvcResult = mockMvc.perform(post(AUTH_SIGNIN_URL).content(objectMapper.writeValueAsString(signInRequest))
                .contentType(APPLICATION_JSON)).andExpect(status().isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertThat(errors.size()).isEqualTo(2);
    }

    @Test
    public void initConfirmRegistration_happyPath() throws Exception {
        //GIVEN
        var emailRequest = createInitSendingTokenRequest(VALID_EMAIL);
        doNothing().when(authService).initConfirmRegistration(emailRequest);

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_INIT_CONFIRM_REGISTRATION_URL)
                .content(objectMapper.writeValueAsString(emailRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void initConfirmRegistrationWithInvalidEmail() throws Exception {
        //GIVEN
        var emailRequest = createInitSendingTokenRequest(INVALID_EMAIL);

        //WHEN
        var mvcResult = mockMvc.perform(post(AUTH_INIT_CONFIRM_REGISTRATION_URL)
                .content(objectMapper.writeValueAsString(emailRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(INVALID_EMAIL_MESSAGE);
        });
    }

}
