package com.itechart.photogallery.unit.user.api;

import static com.itechart.photogallery.common.utils.TestUtils.createChangePasswordRequest;
import static com.itechart.photogallery.common.utils.TestUtils.createUpdateUserRequest;
import static com.itechart.photogallery.unit.ValidationMessages.INVALID_NAME_MESSAGE;
import static com.itechart.photogallery.unit.ValidationMessages.INVALID_PASSWORD_MESSAGE;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.photogallery.common.exception.UnexpectedError;
import com.itechart.photogallery.common.generated.model.User;
import com.itechart.photogallery.unit.AbstractUnitTest;
import com.itechart.photogallery.user.service.UserService;

import org.junit.Test;

import org.mockito.Mock;

public class UsersApiTest extends AbstractUnitTest {

    private static final String USER_URL = "/api/v1/users";

    @Mock
    private UserService userService;

    @Test
    public void changePassword_happyPath() throws Exception {
        //GIVEN
        var changePasswordRequest = createChangePasswordRequest(VALID_PASSWORD, VALID_PASSWORD);
        doNothing().when(userService).updatePassword(changePasswordRequest);

        //WHEN
        var resultActions = mockMvc.perform(put(USER_URL + "/password")
                .content(objectMapper.writeValueAsString(changePasswordRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void changePasswordWithInvalidNewPassword() throws Exception {
        //GIVEN
        var changePasswordRequest = createChangePasswordRequest(VALID_PASSWORD, INVALID_PASSWORD);

        //WHEN
        var mvcResult = mockMvc.perform(put(USER_URL + "/password")
                .content(objectMapper.writeValueAsString(changePasswordRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(INVALID_PASSWORD_MESSAGE);
        });
    }

    @Test
    public void changePasswordWithInvalidOldPassword() throws Exception {
        //GIVEN
        var changePasswordRequest = createChangePasswordRequest(INVALID_PASSWORD, VALID_PASSWORD);

        //WHEN
        var mvcResult = mockMvc.perform(
                put(USER_URL + "/password")
                        .content(objectMapper.writeValueAsString(changePasswordRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(INVALID_PASSWORD_MESSAGE);
        });
    }

    @Test
    public void changePasswordWithInvalidRequest() throws Exception {
        //GIVEN
        var changePasswordRequest = createChangePasswordRequest(INVALID_PASSWORD, INVALID_PASSWORD);

        //WHEN
        var mvcResult = mockMvc.perform(put(USER_URL + "/password")
                .content(objectMapper.writeValueAsString(changePasswordRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertThat(errors.size()).isEqualTo(2);
    }

    @Test
    public void updateUserProfile_happyPath() throws Exception {
        //GIVEN
        var updateUserRequest = createUpdateUserRequest(VALID_NAME, VALID_NAME);
        given(userService.update(UUID.fromString(FAKE.internet().uuid()), updateUserRequest))
                .willReturn(new User());

        //WHEN
        var resultActions = mockMvc.perform(put(USER_URL + "/" + FAKE.internet().uuid() + "/profile")
                .content(objectMapper.writeValueAsString(updateUserRequest)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void updateUserProfileWithInvalidNewFirstName() throws Exception {
        //GIVEN
        var updateUserRequest = createUpdateUserRequest(INVALID_NAME, VALID_NAME);

        //WHEN
        var mvcResult = mockMvc.perform(put(USER_URL + "/" + FAKE.internet().uuid() + "/profile")
                .content(objectMapper.writeValueAsString(updateUserRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(INVALID_NAME_MESSAGE);
        });
    }

    @Test
    public void updateUserProfileWithInvalidNewLastName() throws Exception {
        //GIVEN
        var updateUserRequest = createUpdateUserRequest(VALID_NAME, INVALID_NAME);

        //WHEN
        var mvcResult = mockMvc.perform(put(USER_URL + "/" + FAKE.internet().uuid() + "/profile")
                .content(objectMapper.writeValueAsString(updateUserRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(INVALID_NAME_MESSAGE);
        });
    }

    @Test
    public void updateUserProfileWithInvalidNewFirstNameAndNewLastName() throws Exception {
        //GIVEN
        var updateUserRequest = createUpdateUserRequest(INVALID_NAME, INVALID_NAME);

        //WHEN
        var mvcResult = mockMvc.perform(put(USER_URL + "/" + FAKE.internet().uuid() + "/profile")
                .content(objectMapper.writeValueAsString(updateUserRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertThat(errors.size()).isEqualTo(2);
    }

}
