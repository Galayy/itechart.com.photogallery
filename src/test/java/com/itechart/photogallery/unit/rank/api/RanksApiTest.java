package com.itechart.photogallery.unit.rank.api;

import static com.itechart.photogallery.common.utils.TestUtils.createRankRequest;
import static com.itechart.photogallery.common.utils.TestUtils.createUpdateRankRequest;
import static com.itechart.photogallery.unit.ValidationMessages.INVALID_RANK_MESSAGE;

import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.mockito.BDDMockito.given;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.photogallery.common.exception.UnexpectedError;
import com.itechart.photogallery.common.generated.model.Rank;
import com.itechart.photogallery.rank.service.RankService;
import com.itechart.photogallery.unit.AbstractUnitTest;

import org.junit.Test;

import org.mockito.Mock;

public class RanksApiTest extends AbstractUnitTest {

    private static final String RANK_URL = "/api/v1/ranks";

    @Mock
    private RankService rankService;

    @Test
    public void createRank_happyPath() throws Exception {
        //GIVEN
        var rank = createRankRequest(VALID_RANK);
        given(rankService.create(rank)).willReturn(new Rank());

        //WHEN
        var resultActions = mockMvc.perform(post(RANK_URL)
                .content(objectMapper.writeValueAsString(rank)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isCreated());
    }

    @Test
    public void createRankWithInvalidRank() throws Exception {
        //GIVEN
        var rank = createRankRequest(INVALID_RANK);

        //WHEN
        var mvcResult = mockMvc.perform(post(RANK_URL).content(objectMapper
                .writeValueAsString(rank)).contentType(APPLICATION_JSON)).andExpect(status()
                .isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(INVALID_RANK_MESSAGE);
        });
    }

    @Test
    public void updateRank_happyPath() throws Exception {
        //GIVEN
        var rankToUpdate = createUpdateRankRequest(VALID_RANK);
        given(rankService.update(UUID.fromString(FAKE.internet().uuid()), rankToUpdate))
                .willReturn(new Rank());

        //WHEN
        var resultActions = mockMvc.perform(put(RANK_URL + "/" + FAKE.internet().uuid())
                .content(objectMapper.writeValueAsString(rankToUpdate)).contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void updateRankWithInvalidNewRank() throws Exception {
        //GIVEN
        var rankToUpdate = createUpdateRankRequest(INVALID_RANK);

        //WHEN
        var mvcResult = mockMvc.perform(put(RANK_URL + "/" + FAKE.internet().uuid())
                .content(objectMapper.writeValueAsString(rankToUpdate)).contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
        final List<UnexpectedError> errors = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsByteArray(), new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(INVALID_RANK_MESSAGE);
        });
    }

}
