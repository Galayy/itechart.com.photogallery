package com.itechart.photogallery.unit;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public final class ValidationMessages {

    public static final String INVALID_EMAIL_MESSAGE = "must match \"^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$\"";
    public static final String INVALID_NAME_MESSAGE = "must match \"\\D+\"";
    public static final String INVALID_PASSWORD_MESSAGE = "must match \"(?=.*\\d)[\\w]{6,}\"";
    public static final String INVALID_RANK_MESSAGE = "must be less than or equal to 10";

}
