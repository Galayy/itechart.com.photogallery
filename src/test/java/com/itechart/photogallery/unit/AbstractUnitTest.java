package com.itechart.photogallery.unit;

import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.github.javafaker.Faker;

import com.itechart.photogallery.PhotogalleryApplication;
import com.itechart.photogallery.auth.api.AuthApiImpl;
import com.itechart.photogallery.common.exception.ExceptionHandling;
import com.itechart.photogallery.rank.api.RanksApiImpl;
import com.itechart.photogallery.user.api.UsersApiImpl;

import org.junit.Before;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@AutoConfigureMockMvc
@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest(classes = PhotogalleryApplication.class)
public abstract class AbstractUnitTest {

    protected static final Faker FAKE = Faker.instance(Locale.US, ThreadLocalRandom.current());

    protected static final String INVALID_EMAIL = FAKE.random().hex(25);
    protected static final String INVALID_NAME = FAKE.bothify("???##");
    protected static final String INVALID_PASSWORD = FAKE.internet().password(false);
    protected static final Integer INVALID_RANK = FAKE.number().numberBetween(15, 100);

    protected static final String VALID_EMAIL = FAKE.bothify("???##@gmail.com");
    protected static final String VALID_NAME = FAKE.name().name();
    protected static final String VALID_PASSWORD = FAKE.bothify("????####");
    protected static final Integer VALID_RANK = FAKE.number().numberBetween(1, 9);

    @InjectMocks
    protected RanksApiImpl ranksApi;
    @InjectMocks
    protected UsersApiImpl usersApi;
    @InjectMocks
    protected AuthApiImpl authApi;

    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    protected MockMvc mockMvc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.objectMapper = new ObjectMapper();
        this.mockMvc = MockMvcBuilders.standaloneSetup(ranksApi, usersApi, authApi)
                .setControllerAdvice(new ExceptionHandling()).build();
    }

}
