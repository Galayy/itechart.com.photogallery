PROJECT_NAME := photogallery

clean:
	gradlew clean

build: clean
ifndef skip-tests
	gradlew build
else
	gradlew build -x test
endif

docker-build: build
	docker image build -t $(PROJECT_NAME) .

heroku: build
ifndef login
	heroku container:login
endif
	heroku container:push web --app=$(PROJECT_NAME)-service
	heroku container:release web --app=$(PROJECT_NAME)-service
	heroku stack:set heroku-18 --app=$(PROJECT_NAME)-service

new-relic-local:
	java -javaagent:newrelic/newrelic.jar -jar build/libs/$(PROJECT_NAME)-0.0.1-SNAPSHOT.jar
